import sys, os
import random
sys.path.append(os.path.abspath(os.getcwd()))
from absl import app
from pysc2.lib import actions, features
from pysc2.env import sc2_env, run_loop
from TerranBot.LearningAgent2 import LearningTerranAgent
from ZergBot.ZergAgent import LearningZergAgent
from ProtossBot.ProtossAgent import LearningProtossAgent
from GeneralMethods import connect_actions
from GeneralMethods import connect_actions_deeper


class RandomTerranAgent(LearningTerranAgent):
    '''
    Klasa agenta wykonującego losowe akcje terran.
    W ostatnim kroku wynik gry jest zapisywany do pliku score.txt
    '''
    def step(self, obs):
        if obs.last():
            f = open("score.txt", "a")
            f.write(str(obs.reward) + "\n")
            f.close()
        super(RandomTerranAgent, self).step(obs)
        action = random.choice(self.actions)
        raw_action = getattr(self, action)(obs)

        return raw_action


class SemiRandomTerranAgent(LearningTerranAgent):
    '''
    Klasa agenta wykonującego częściowo losowe akcje terran.
    Co 4 krok gry wykonywana jest losowa akcja, akcja optymalizacji robotników i akcja naprawy budynków.
    Co 32 krok wykonywana jest akcja ataku.
    W ostatnim kroku wynik gry jest zapisywany do pliku score.txt
    '''
    def __init__(self):
        super(SemiRandomTerranAgent, self).__init__()
        self.actions_counter = 0

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, wywołująca częściowo losowe akcje

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        super(SemiRandomTerranAgent, self).step(obs)
        if obs.last():
            f = open("score.txt", "a")
            f.write(str(obs.reward) + "\n")
            f.close()
        all_actions = []
        action = random.choice(self.actions)
        all_actions = connect_actions_deeper(all_actions, getattr(self, action)(obs))
        attack_action = getattr(self, "attack")(obs)
        harvest_action = getattr(self, "harvest_all_minerals")(obs)
        repair_action = getattr(self, "repair_buildings")(obs)
        self.actions_counter += 1
        if self.actions_counter % 4 == 0:
            all_actions = connect_actions_deeper(all_actions, harvest_action)
            all_actions = connect_actions_deeper(all_actions, repair_action)
        if self.actions_counter > 32:
            self.actions_counter = 0
            all_actions = connect_actions_deeper(all_actions, attack_action)
            
        all_actions = [act for act in all_actions if act != None]
        if len(all_actions) == 0:
            all_actions.append(actions.RAW_FUNCTIONS.no_op())
        return all_actions


class RandomZergAgent(LearningZergAgent):
    def step(self, obs):
        super(RandomZergAgent, self).step(obs)
        action = random.choice(self.actions)
        raw_action = getattr(self, action)(obs)
        return raw_action


class RandomProtossAgent(LearningProtossAgent):
    def step(self, obs):
        super(RandomProtossAgent, self).step(obs)
        action = random.choice(self.actions)
        raw_action = getattr(self, action)(obs)
        return raw_action

class SemiRandomZergAgent(RandomZergAgent):
    '''
    Klasa agenta wykonującego częściowo losowe akcje terran.
    Co 4 krok gry wykonywana jest losowa akcja, akcja optymalizacji robotników i akcja naprawy budynków.
    Co 32 krok wykonywana jest akcja ataku.
    W ostatnim kroku wynik gry jest zapisywany do pliku score.txt
    '''

    def __init__(self):
        super(SemiRandomZergAgent, self).__init__()
        self.actions_counter = 0

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, wywołująca częściowo losowe akcje

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        super(SemiRandomZergAgent, self).step(obs)
        if obs.last():
            f = open("score.txt", "a")
            f.write(str(obs.reward) + "\n")
            f.close()
        all_actions = []
        action = random.choice(self.actions)
        all_actions = connect_actions_deeper(all_actions, getattr(self, action)(obs))
        attack_action = getattr(self, "attack")(obs)
        harvest_action = getattr(self, "harvest_all_minerals")(obs)
        # repair_action = getattr(self, "repair_buildings")(obs)
        self.actions_counter += 1
        if self.actions_counter % 4 == 0:
            all_actions = connect_actions_deeper(all_actions, harvest_action)
            # all_actions = connect_actions_deeper(all_actions, repair_action)
        if self.actions_counter > 32:
            self.actions_counter = 0
            all_actions = connect_actions_deeper(all_actions, attack_action)

        all_actions = [act for act in all_actions if act != None]
        if len(all_actions) == 0:
            all_actions.append(actions.RAW_FUNCTIONS.no_op())
        return all_actions


def main(unused_argv):
    # agent1 = SemiRandomTerranAgent()
    agent1 = RandomProtossAgent()
    # agent2 = RandomTerranAgent()
    try:
        with sc2_env.SC2Env(
                map_name="WintersGateLE",
                players=[sc2_env.Agent(sc2_env.Race.protoss),
                         sc2_env.Bot(sc2_env.Race.zerg, sc2_env.Difficulty.medium)
                         # sc2_env.Agent(sc2_env.Race.terran)
                         ],
                agent_interface_format=features.AgentInterfaceFormat(
                    action_space=actions.ActionSpace.RAW,
                    use_raw_units=True,
                    raw_resolution=256,
                ),
                step_mul=5,  # zmiana ilości pomijanych kroków
                disable_fog=False,
        ) as env:

            run_loop.run_loop([agent1], env, max_episodes=1000)

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    app.run(main)
