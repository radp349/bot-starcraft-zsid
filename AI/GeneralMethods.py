import numpy as np
import random
from sklearn.cluster import DBSCAN
from pysc2.lib import units, actions, features


def get_reward(prev_obs, obs, prev_state, state, time,x):
    '''

    Funkcja przyznająca nagrodę w trakcie przebiegu gry, a na koniec dodatkową nagrodę w zależności
    od tego czy bot wygrał.
    Każda z rzeczy, za którą bot może dostać nagrodę posiada własną wagę, ustalane były one "na ślepo" z powodu
    nieznajomości odpowiednich dla każdego elementu.
    Dodano również karę za czas, im dłużej bot gra tym większą karę dostaje.

    
    :returns: (float): Przyznana nagroda za osiągnięcia.

    '''
    reward = 0
    if obs.last():
        reward += obs.reward * 1000
    else:
        w_command_center = 10
        w_supply_depot = 2
        w_supply = 0.1
        w_barracks = 8
        w_marines = 16
        w_killed_unit = 0.4
        w_refinery=3
        w_killed_structure = 0.6
        reward += (state[x-1][0] - prev_state[x-1][0])*w_command_center if (state[x-1][0] - prev_state[x-1][0]) > 0 else 0
        reward -= state[x-1][9] * w_supply #za dużo supply to kara
        reward += (state[x-1][9] - prev_state[x-1][9])*(w_supply+0.01) if (state[x-1][9] - prev_state[x-1][9]) > 0 else 0 # za zbieranie supply nagroda
        reward += (state[x-1][3] - prev_state[x-1][3])*w_supply_depot if (state[x-1][3] - prev_state[x-1][3]) >=0 else (state[x-1][3] - prev_state[x-1][3])*w_supply_depot + 1  # supply depot
        reward += (state[x-1][4] - prev_state[x-1][4])*w_barracks if (state[x-1][4] - prev_state[x-1][4]) >=0 else (state[x-1][4] - prev_state[x-1][4])*w_barracks + 3  # barracks
        reward += (state[x-1][7] - prev_state[x-1][7])*w_marines if state[x-1][7] - prev_state[x-1][7] >=0 else 0  # marines
        reward += (state[x - 1][5] - prev_state[x - 1][5]) * w_refinery if (state[x - 1][5] - prev_state[x - 1][
            5]) >= 0 else (state[x - 1][5] - prev_state[x - 1][5]) * w_refinery + 1

        reward += (obs.observation['score_cumulative'][5] - prev_obs.observation['score_cumulative'][5]) * w_killed_unit
        reward += (obs.observation['score_cumulative'][6] - prev_obs.observation['score_cumulative'][
            6]) * w_killed_structure
        reward -= time / 100.

    return reward


def get_units_by_type(obs, unit_type):
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type]


def get_distances(obs, units, xy):
    units_xy = [(unit.x, unit.y) for unit in units]
    return np.linalg.norm(np.array(units_xy) - np.array(xy), axis=1)


def get_distances_from_points(obs, points, xy):
    return np.linalg.norm(np.array(points) - np.array(xy), axis=1)


def get_my_units_by_type(obs, unit_type):
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.alliance == features.PlayerRelative.SELF]


def get_enemy_buildings(obs):
    enemy_buildings = [building for building in obs.observation.raw_units if
                       building.alliance == features.PlayerRelative.ENEMY and (
                               (building.unit_type == terran_buildings).any() or (
                               building.unit_type == zerg_buildings).any() or (
                                       building.unit_type == protoss_buildings).any())]
    return enemy_buildings


def get_my_buildings(obs):
    ally_buildings = [building for building in obs.observation.raw_units if
                      building.alliance == features.PlayerRelative.SELF and (
                              (building.unit_type == terran_buildings).any() or (
                              building.unit_type == zerg_buildings).any() or (
                                      building.unit_type == protoss_buildings).any())]
    return ally_buildings


def get_my_bases(obs):
    ally_buildings = [building for building in obs.observation.raw_units if
                      building.alliance == features.PlayerRelative.SELF and (
                          (building.unit_type == bases).any()).any()]
    return ally_buildings


def get_enemy_workers(obs):
    return [worker for worker in obs.observation.raw_units if
            worker.alliance == features.PlayerRelative.ENEMY and (worker.unit_type == workers).any()]


def get_enemy_combat_units(obs):
    enemy_combat_units = [combat_unit for combat_unit in obs.observation.raw_units if
                          combat_unit.alliance == features.PlayerRelative.ENEMY and (
                                  (combat_unit.unit_type == terran_combat_units).any() or (
                                  combat_unit.unit_type == zerg_combat_units).any() or (
                                          combat_unit.unit_type == protoss_combat_units).any())]
    return enemy_combat_units


def get_enemy_units_by_type(obs, unit_type):  # zwraca ilosc wybudowanych budynkow gotowych do dzialania
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.alliance == features.PlayerRelative.ENEMY]


def get_my_completed_units_by_type(obs, unit_type):
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.build_progress == 100
            and unit.alliance == features.PlayerRelative.SELF]


def get_enemy_completed_units_by_type(obs, unit_type):  # zwraca ilosc wszystkich unit_type, nawet w budowie
    # lub wszystkie jednostki bojowe
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.build_progress == 100
            and unit.alliance == features.PlayerRelative.ENEMY]


def get_minerals(obs):
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type in [
                units.Neutral.BattleStationMineralField,
                units.Neutral.BattleStationMineralField750,
                units.Neutral.LabMineralField,
                units.Neutral.LabMineralField750,
                units.Neutral.MineralField,
                units.Neutral.MineralField750,
                units.Neutral.PurifierMineralField,
                units.Neutral.PurifierMineralField750,
                units.Neutral.PurifierRichMineralField,
                units.Neutral.PurifierRichMineralField750,
                units.Neutral.RichMineralField,
                units.Neutral.RichMineralField750
            ]]


def cluster(units, ep=10, samples=2):  # podajesz jednostki i oczekiwany promien clustra
    units_xy = np.array([(unit.x, unit.y) for unit in units])
    sorted_units = []
    if units_xy.any():
        model = DBSCAN(eps=ep, min_samples=samples)
        cl = np.array(model.fit_predict(units_xy))
        cl_unique = np.unique(cl)

        for i in cl_unique:
            un = []
            for j in range(len(units)):
                if cl[j] == i:
                    un.append(units[j])
            sorted_units.append(un)
    return sorted_units


def mean_of_units(units):
    units_xy = np.array([(unit.x, unit.y) for unit in units])
    return np.mean(units_xy, axis=0)


def get_my_upgrade(obs, upgrade_type):
    return [upg for upg in obs.observation.upgrades
            if upg == upgrade_type]


def free_supply(obs):
    return obs.observation.player.food_cap - obs.observation.player.food_used


def sort_by_dist(obs, first_base_cords, units_to_sort):
    units_sorted = []
    tmp = get_distances(obs, units_to_sort, first_base_cords)
    if len(tmp) > 0:
        dist = list(tmp)
        for i in range(len(units_to_sort)):
            tmp = dist.index(min(dist))
            units_sorted.append(units_to_sort[tmp])
            units_to_sort.pop(tmp)
            dist.pop(tmp)
    return units_sorted


workers = [units.Terran.SCV, units.Zerg.Drone, units.Protoss.Probe]
terran_buildings = [units.Terran.Armory, units.Terran.Barracks, units.Terran.BarracksFlying,
                    units.Terran.BarracksReactor, units.Terran.BarracksTechLab,
                    units.Terran.CommandCenter, units.Terran.Refinery, units.Terran.Bunker,
                    units.Terran.CommandCenterFlying, units.Terran.EngineeringBay,
                    units.Terran.Factory, units.Terran.FactoryFlying, units.Terran.FactoryReactor,
                    units.Terran.FactoryTechLab, units.Terran.FusionCore,
                    units.Terran.GhostAcademy, units.Terran.MissileTurret, units.Terran.OrbitalCommand,
                    units.Terran.OrbitalCommandFlying,
                    units.Terran.PlanetaryFortress, units.Terran.Reactor, units.Terran.Refinery,
                    units.Terran.RefineryRich, units.Terran.SensorTower, units.Terran.Starport,
                    units.Terran.StarportFlying, units.Terran.StarportReactor, units.Terran.StarportTechLab,
                    units.Terran.SupplyDepot, units.Terran.SupplyDepotLowered, units.Terran.TechLab]

zerg_buildings = [units.Zerg.Hive, units.Zerg.Hatchery, units.Zerg.HydraliskDen, units.Zerg.Lair, units.Zerg.Spire,
                  units.Zerg.SpawningPool, units.Zerg.SpineCrawler,
                  units.Zerg.SporeCrawler, units.Zerg.SpineCrawlerUprooted, units.Zerg.SporeCrawlerUprooted,
                  units.Zerg.GreaterSpire, units.Zerg.RoachWarren, units.Zerg.EvolutionChamber,
                  units.Zerg.Extractor,
                  units.Zerg.ExtractorRich, units.Zerg.BanelingNest, units.Zerg.LurkerDen,
                  units.Zerg.UltraliskCavern]

protoss_buildings = [units.Protoss.Nexus, units.Protoss.Pylon, units.Protoss.Assimilator,
                     units.Protoss.AssimilatorRich, units.Protoss.Gateway, units.Protoss.WarpGate,
                     units.Protoss.Stargate, units.Protoss.CyberneticsCore, units.Protoss.Forge,
                     units.Protoss.PhotonCannon, units.Protoss.ShieldBattery, units.Protoss.TwilightCouncil,
                     units.Protoss.RoboticsBay, units.Protoss.RoboticsFacility, units.Protoss.TemplarArchive,
                     units.Protoss.DarkShrine, units.Protoss.FleetBeacon]

terran_combat_units = [units.Terran.Marine, units.Terran.WidowMine, units.Terran.Marauder, units.Terran.Reaper,
                       units.Terran.Hellion,
                       units.Terran.Hellbat, units.Terran.SiegeTank, units.Terran.SiegeTankSieged,
                       units.Terran.Cyclone, units.Terran.Ghost, units.Terran.Thor,
                       units.Terran.AutoTurret, units.Terran.VikingAssault, units.Terran.VikingFighter,
                       units.Terran.Medivac, units.Terran.Raven, units.Terran.Liberator,
                       units.Terran.Banshee, units.Terran.Battlecruiser, units.Terran.LiberatorAG]

zerg_combat_units = [units.Zerg.Queen, units.Zerg.Zergling, units.Zerg.Baneling, units.Zerg.BanelingCocoon,
                     units.Zerg.Mutalisk, units.Zerg.Roach,
                     units.Zerg.Ravager, units.Zerg.Infestor, units.Zerg.BroodLord, units.Zerg.SwarmHost,
                     units.Zerg.Locust,
                     units.Zerg.LocustFlying, units.Zerg.InfestedTerran, units.Zerg.Viper, units.Zerg.Ultralisk]

protoss_combat_units = [units.Protoss.Zealot, units.Protoss.Stalker, units.Protoss.Sentry, units.Protoss.Adept,
                        units.Protoss.HighTemplar, units.Protoss.DarkTemplar, units.Protoss.Immortal,
                        units.Protoss.Colossus, units.Protoss.Disruptor, units.Protoss.DisruptorPhased,
                        units.Protoss.Archon, units.Protoss.Observer, units.Protoss.WarpPrism,
                        units.Protoss.Phoenix, units.Protoss.VoidRay, units.Protoss.Oracle, units.Protoss.Carrier,
                        units.Protoss.Tempest, units.Protoss.Mothership, units.Protoss.MothershipCore]

bases = [units.Protoss.Nexus, units.Terran.CommandCenter, units.Terran.PlanetaryFortress, units.Terran.OrbitalCommand,
         units.Zerg.Lair, units.Zerg.Hive]
