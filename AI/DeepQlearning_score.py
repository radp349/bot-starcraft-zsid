# -*- coding: utf-8 -*-
import sys

sys.path.append(os.path.abspath(os.getcwd()))
import random
import math
from time import time
import MineralGatheringMethods as br
import GeneralMethods as GenM
import TrainArmy as ta
import score as scr
import ResearchUpgrades as ru
import BuildingConstructs as bc
import UnitsSkills as us
import EnemySearchAgentMethods as esam
import AttackMethods as am
import BaseBuildingMethods as bbm
import numpy as np

import copy
from absl import app
from pysc2.agents import base_agent
from pysc2.lib import actions, features, units, upgrades
from pysc2.env import sc2_env, run_loop

from tensorflow.keras import Model, Sequential
from tensorflow.keras.layers import Dense, TimeDistributed, Reshape, LSTM, Dropout, MaxPooling2D, Conv2D, Flatten, BatchNormalization,Input, ConvLSTM2D, MaxPooling3D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model

from collections import deque
import os

_SCORE = []
learning = True


class DQN_Agent:
    '''
    
    Klasa odpowiedzialna za naukę naszego agenta,
    znajdują się w niej wszystkie elementy potrzebne do nauki agenta posiadającego
    dowolne akcje.
    Używamy DeepQLearningu do zoptymalizowania nauki

        
    :param state_size: rozmiar zwracany prze metodę get_state, zwraca ona wszystkie istotne informacje liczbowe o stanie gry
    :type state_size: int
    :param actions: możliwe akcje do wykonania przez bota
    :type actions: tuple
    :param optimizer: funkcja optymalizująca wykorzystywana w sieciach, Adam
    :type optimizer: optimizer

    .. note::

        Główna klasa ucząca programu.

    '''
    def __init__(self, state_size, actions, optimizer):
        
        self.actions = actions
        self._state_size = state_size
        self._action_size = len(actions)
        self._optimizer = optimizer
        self.expirience_replay = deque(maxlen=2000)

        # Initialize discount and exploration rate
        self.gamma = 0.76
        self.epsilon = 0.24

        if os.path.isfile('model.h5'):
            self.q_network = load_model('model.h5')
            self.target_network = load_model('model.h5')
            self.q_network.compile(loss='mse', optimizer=self._optimizer)
            self.target_network.compile(loss='mse', optimizer=self._optimizer)
        else:
            # Build networks
            self.q_network = self._build_compile_model()
            self.target_network = self._build_compile_model()
        self.alighn_target_model()

    def store(self, state, action, reward, next_state, terminated):
        self.expirience_replay.append((state, action, reward, next_state, terminated))

    def _build_compile_model(self):
        '''

        Strukturta używanego modelu, zastosowanie ConvLSTM w celu poprawy działania,
        przyjmuje argumentu rozmiaru batch_size, standardowo 32 poprzednie state'y gry

        :param model: warstwy modelu
        :type model: class
        
        :returns: (network): struktura sieci


        '''
        model = Sequential()
        model.add(ConvLSTM2D(70,kernel_size=(3,3),padding='same', input_shape=(1,32,29,1)))
        model.add(BatchNormalization())
        model.add(MaxPooling2D())
        model.add(Flatten())
        model.add(Dense(40, activation='relu'))
        model.add(Dense(30, activation="relu"))
        model.add(Dense(20, activation="relu"))
        model.add(Dense(self._action_size, activation="linear"))

        model.compile(loss='mse', optimizer=self._optimizer)
        model.summary()
        return model

    def alighn_target_model(self):
        '''

        Przepisanie wag z q_network do target_network po jednym przejściu, by nie odbiegały od siebie,
        implementacja DeepQNetwork

        '''
        self.target_network.set_weights(self.q_network.get_weights())

    def save_model(self):
        '''

        Zapisywanie modelu do pliku

        '''
        self.q_network.save("model.h5")

    def act(self, state):
        '''

        Metoda ustalająca następne posunięcie bota, możliwe są losowe ruchy, jeżeli
        nie wybrano losowego ruchu to wykonuje się ruch wybrany przez sieć

        '''
        if np.random.rand() <= self.epsilon:
            action = np.random.choice(self.actions)
            #print("epsilon: 1")
        else:
            state = np.expand_dims(state, axis=(0, 3))
            state = np.expand_dims(state, axis=0)
            r = self.q_network.predict(state)
            action = self.actions[np.argmax(r[0])]
            #print("epsilon: 0")
        return action

    def retrain(self, batch_size):
        '''

        Główna metoda ucząca w klasie, zbiera minibatch zebranych stanów z self.expirience_replay,
        rozpakowuje je na poszczególne części i predykuje wybór sieci
        Na koniec fit'uje q_network

        
        :param minibatch: lista zawierająca ostatnie 32 stany gry
        :type minibatch: list
        :param state: tablica stanów 32 x state_size
        :type state: array
        :param target: akcja przewidziana przez sieć
        :type target: String
        :param t: akcja przewidziana prze drugą sieć
        :type t: String

        :returns: None  

        '''
        minibatch = random.sample(self.expirience_replay, batch_size) #hmm

        for state, action, reward, next_state, terminated in minibatch:
            state = np.expand_dims(state, axis=(0, 3))
            next_state = np.expand_dims(next_state, axis=(0, 3))
            state = np.expand_dims(state, axis=0)
            next_state = np.expand_dims(next_state, axis=0)
            target = self.q_network.predict(next_state)

            if terminated:
                target[0][action] = reward
            else:
                t = self.target_network.predict(next_state)
                target[0][action] = reward + self.gamma * np.amax(t)

            self.q_network.fit(state, np.array(target), epochs=1, verbose=0)


class Agent(base_agent.BaseAgent):

    '''

    Podstawowy agent dziedziczący po BaseAgent moduły pysc2,
    zawiera listę dostępnych akcji i ich definicję.

    '''
    # 10 akcji
    actions = ("harvest_all_minerals",
               "train_scv",
               "attack",
               "build_supply_depot", #3
               "build_barrack", #4
               "build_refinery", #5
               #"scout",
               "do_research",
               "train_marine", #7
               "build_command_center",
               "build_reactor_barrack",
               # "disengage",
               # "disengage_lowest_HP",
               # "research_infantry_weapons_level_1",
               # "research_infantry_armor_level_1",
               
               #"build_factory",
               #"build_turret"
               #"use_scanner_sweep"
               # "test_canceling"
               )



    def step(self, obs):
        super(Agent, self).step(obs)
        if obs.first():
            command_center = GenM.get_my_units_by_type(
                obs, units.Terran.CommandCenter)[0]
            self.base_top_left = (command_center.x < 32)
            self.first_base_cords = (command_center.x, command_center.y)

    def scout(self, obs):
        '''

        Akcja niezależna wysyłająca SCV'ke na zwiady,
        niezwykle pomocna

        :returns: Scouting action

        '''
        return esam.scout(self, obs)


    def harvest_all_minerals(self, obs):
        return br.harvest_all_minerals(obs)

    def build_refinery(self, obs):
        return br.build_refinery(obs, self.first_base_cords)

    def build_reactor_barrack(self, obs):
        if random.random() > 2:
            return bc.build_techlab_barrack(obs)
        return bc.build_reactor_barrack(obs)


    def build_command_center(self, obs):
        return bbm.build_command_center(self.first_base_cords, obs)

    def train_scv(self, obs):
        '''

        **Trenowanie SCV'ek.**

        :returns: (List): Lista akcji.

        '''
        acts = []
        if len(GenM.get_my_units_by_type(obs, units.Terran.OrbitalCommand) ) > 0:
            acts.append(us.calldown_MULE(obs))
        if len(GenM.get_my_bases(obs))*22 > len(GenM.get_my_units_by_type(obs, units.Terran.SCV)):
            if GenM.free_supply(obs) < 1:
                acts.append(self.build_supply_depot(obs))
                return acts
            acts.append(br.train_scv(obs))
            return acts
        acts.append(self.build_supply_depot(obs))
        return acts

    def train_marine(self, obs):
        '''

        **Trenowanie marinów.**

        :returns: (List): Lista akcji.

        '''
        barracks = GenM.get_my_units_by_type(obs, units.Terran.Barracks)
        if len(barracks) < 1:
            return self.build_barrack(obs)
        if GenM.free_supply(obs) < 1:
            return self.build_supply_depot(obs)
        trainer = []
        for barrack in barracks:
            trainer.append(ta.train_marine(obs))
        return trainer

    # atak i micro
    def attack(self, obs):
        marines = GenM.get_my_units_by_type(obs, units.Terran.Marine)
        if len(marines) > 14:
            return am.attack(obs)
        return am.disengage(obs, self.first_base_cords)

    def disengage(self, obs):
        return am.disengage(obs)

    def disengage_lowest_HP(self, obs):
        return am.disengage_lowest_HP(obs)

    # upgrade
    def do_research(self, obs):
        '''

        **Ulepszenia.**

        :returns: (method): Zbudowanie bay'a/ulepszenie/produkcja marina.

        '''
        if len(GenM.get_my_units_by_type(obs, units.Terran.EngineeringBay)) < 1:
            return self.build_engineering_bay(obs)
        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel1)) < 1:
            return self.research_infantry_weapons_level_1(obs)
        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel1)) < 1:
            return self.research_infantry_armor_level_1(obs)
        return self.train_marine(obs)

    def research_infantry_weapons_level_1(self, obs):
        return ru.research_infantry_weapons_level_1(obs)

    def research_infantry_armor_level_1(self, obs):
        return ru.research_infantry_armor_level_1(obs)

    # to trzeba zamienieć
    def build_barrack(self, obs):
        return bc.build_barrack(self.first_base_cords, self.base_top_left, obs)

    def build_engineering_bay(self, obs):
        return bc.build_engineering_bay(self.first_base_cords, self.base_top_left, obs)

    def build_factory(self, obs):
        return bc.build_factory(self.first_base_cords, self.base_top_left, obs)

    def build_turret(self, obs):
        return bc.build_missile_turret(self.first_base_cords, self.base_top_left, obs)

    def build_supply_depot(self, obs):
        if GenM.free_supply(obs)>=7:
            return actions.RAW_FUNCTIONS.no_op()
        return bc.build_supply_depot(self.first_base_cords, obs)




class RandomAgent(Agent):
    def step(self, obs):
        super(RandomAgent, self).step(obs)
        action = random.choice(self.actions)
        return getattr(self, action)(obs)


class SmartAgent(Agent):
    '''

    Klasa zawierająca pobieranie stanów i wykonywanie kolejnych kroków.

    '''
    def __init__(self):
        super(SmartAgent, self).__init__()
        self.state_size = 29  # ilość rzeczy zwracanych przez get_state
        self.batch_size = 32
        self.t = 32
        self.state_seq = deque(maxlen=self.t)
        self.optimizer = Adam(learning_rate=0.09)
        self.DQN = DQN_Agent(self.state_size, self.actions, self.optimizer)
        self.time = 0
        self.new_game()

    def reset(self):
        super(SmartAgent, self).reset()
        self.new_game()

    def new_game(self):
        self.base_top_left = None
        self.previous_state = None
        self.previous_obs = None
        self.previous_action = None
        self.score = 0
        self.counter = 0
        self.f = 15

    def get_state(self, obs):
        '''

        Pobieranie wszystkich informacji z programu, ilości jednostek, surowców, jednostek przeciwnika 
        oraz budynków.

        
        :returns: (tuple): krotka zawierająca wszystkie potrzebne informację o stanie gry 

        '''
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        idle_scvs = [scv for scv in scvs if scv.order_length == 0]
        command_centers = GenM.get_my_units_by_type(obs, units.Terran.CommandCenter)
        supply_depots = GenM.get_my_units_by_type(obs, units.Terran.SupplyDepot)
        refinery = GenM.get_my_units_by_type(obs, units.Terran.Refinery)
        completed_barrackses = GenM.get_my_units_by_type(obs, units.Terran.Barracks)
        barrackses = GenM.get_my_completed_units_by_type(obs, units.Terran.Barracks)
        engineering_bay = GenM.get_my_units_by_type(obs, units.Terran.EngineeringBay)
        marines = GenM.get_my_completed_units_by_type(obs, units.Terran.Marine)
        queued_marines = (completed_barrackses[0].order_length
                          if len(completed_barrackses) > 0 else 0)
        free_supply = (obs.observation.player.food_cap -
                       obs.observation.player.food_used)
        free_supply = free_supply if free_supply > 0 else 0

        enemy_scvs = GenM.get_enemy_units_by_type(obs, units.Terran.SCV)
        enemy_idle_scvs = [scv for scv in enemy_scvs if scv.order_length == 0]
        enemy_completed_command_centers = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.CommandCenter)
        enemy_completed_supply_depots = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.SupplyDepot)
        enemy_completed_barrackses_techlab = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.BarracksTechLab)
        enemy_completed_barrackses = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Barracks)
        enemy_completed_bunkers = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Bunker)
        enemy_completed_starports = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Starport)
        enemy_completed_starports_techlab = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.StarportTechLab)
        enemy_completed_factories = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Factory)
        enemy_completed_refineries = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Refinery)
        enemy_completed_armories = GenM.get_enemy_completed_units_by_type(
            obs, units.Terran.Armory)
        enemy_marines = GenM.get_enemy_units_by_type(obs, units.Terran.Marine)
        enemy_marauders = GenM.get_enemy_units_by_type(obs, units.Terran.Marauder)
        enemy_thors = GenM.get_enemy_units_by_type(obs, units.Terran.Thor)
        enemy_ravens = GenM.get_enemy_units_by_type(obs, units.Terran.Raven)
        enemy_siegetanks = GenM.get_enemy_units_by_type(obs, units.Terran.SiegeTank)
        enemy_medivacs = GenM.get_enemy_units_by_type(obs, units.Terran.Medivac)

        return (len(command_centers),
                len(scvs),
                len(idle_scvs),
                len(supply_depots),
                len(barrackses),
                len(refinery),
                len(engineering_bay),
                len(marines),
                queued_marines,
                free_supply,
                len(enemy_completed_command_centers),
                len(enemy_scvs),
                len(enemy_idle_scvs),
                len(enemy_completed_supply_depots),
                len(enemy_completed_bunkers),
                len(enemy_completed_barrackses),
                len(enemy_completed_barrackses_techlab),
                len(enemy_completed_refineries),
                len(enemy_completed_armories),
                len(enemy_completed_factories),
                len(enemy_completed_starports),
                len(enemy_completed_starports_techlab),
                len(enemy_completed_armories),
                len(enemy_marines),
                len(enemy_marauders),
                len(enemy_thors),
                len(enemy_medivacs),
                len(enemy_ravens),
                len(enemy_siegetanks),
                )

    def step(self, obs):
        '''

        Metoda wykonująca następne etapy gry.

        :param state: Lista stanów.
        :type state: List
        :param action: Wybrana akcja.
        :type action: String
        :param reward: Nagroda przyznana w obecnym przejściu.
        :type reward: float

        :returns: String: Wybrana akcja.

        '''
        super(SmartAgent, self).step(obs)
        self.time += 1
        self.counter += 1
        if obs.first():
            
            command_center = GenM.get_my_units_by_type(
                obs, units.Terran.CommandCenter)[0]
            self.base_top_left = (command_center.x < 32)

            self.first_base_cords = (command_center.x, command_center.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        if self.counter == self.f:
            self.counter = 0
            self.f += 8
            return esam.scout(self, obs)

        state = list(self.get_state(obs))
        self.state_seq.append((state))
        if len(self.state_seq) == self.t:
            sta = []
            for st in self.state_seq:
                sta.append(np.array(st))
            state = np.array(sta)
            #print("-------------------------------")
            action = self.DQN.act(state)
            if self.previous_action is not None and learning:
                reward = GenM.get_reward(self.previous_obs, obs, copy.copy(self.previous_state), state, self.time,
                                         self.t)
                #print("reward: " + str(reward))
                self.DQN.store(self.previous_state, self.actions.index(self.previous_action), reward, state,
                               True if obs.last() else False)
                self.score += reward
                if not obs.last():
                    self.DQN.alighn_target_model()
                if len(self.DQN.expirience_replay) > self.batch_size:
                    self.DQN.retrain(self.batch_size)
                if obs.last():
                    self.DQN.save_model()
                    self.DQN.expirience_replay.clear()
                    scr.save(self.score, obs.reward)
                    self.f = 15
                    self.score = 0
                    self.time = 0

            self.previous_state = state
            self.previous_obs = obs
            self.previous_action = action
            #print("akcja: " + str(action))
            return getattr(self, action)(obs)
        else:
            action = random.choice(self.actions)
            return getattr(self, action)(obs)


def main(unused_argv):
    '''

    **Główna funkcja wywołująca główną pętlę programu.**

    :param agent1: Agent używany w programie.
    :type agent1: class

    '''
    agent1 = SmartAgent()
    try:
        with sc2_env.SC2Env(
                map_name="WintersGateLE",
                players=[sc2_env.Agent(sc2_env.Race.terran, name="Player"),
                         sc2_env.Bot(sc2_env.Race.terran, sc2_env.Difficulty.medium)],
                agent_interface_format=features.AgentInterfaceFormat(
                    action_space=actions.ActionSpace.RAW,
                    use_raw_units=True,
                    raw_resolution=256,
                ),
                score_index=-1,
                step_mul=40,
                disable_fog=False,
        ) as env:
            try:
                run_loop.run_loop([agent1], env, max_episodes=1000, max_frames=34080)
            except ValueError:
                agent1.DQN.save_model()
                agent1.DQN.expirience_replay.clear()
                agent1.f = 15
                agent1.score = 0
                agent1.time = 0
                app.run(main)
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    app.run(main)
