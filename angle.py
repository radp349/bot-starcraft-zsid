import math
import random
import numpy as np

def angle_generator(begin_degree, end_degree, step_degree):
    '''Funkcja generująca kąty w stopniach z przedziału(begin_degree,begin_degree+step_degree, ..., end_degree>.

    :param begin_degree: Wartość pierwszego kąta w tablicy.
    :type begin_degree: int.
    :param end_degree: Wartość ostatniego kąta w tablicy.
    :type end_degree: int.
    :param step_degree: Krok o ile ma być zwiększony kolejny element tablicy.
    :type step_degree: type.
    :returns: Losowo wybrany kąt z przetasowanej tablicy kątów zamieniony na radiany.

    '''
    angle = []
    for i in range(begin_degree, end_degree, step_degree):
        angle.append(i)
    random.shuffle(angle)
    return math.radians(random.choice(angle))


def sin_processing(radius, angle):
    '''Funkcja kalkulująca współrzędną(x) punktu na okręgu, przy użycie funkcji trygonometrycznej sinus.

    :param radius: Promień okręgu.
    :type radius: float.
    :param angle: Kąt.
    :type angle: int.
    :returns: Pierwsza składowa punktu na okręgu(x).

    '''
    return radius*math.sin(angle)


def cos_processing(radius, angle):
    '''Funkcja kalkulująca współrzędną(y) punktu na okręgu, przy użycie funkcji trygonometrycznej cosinus.

    :param radius: Promień okręgu.
    :type radius: float.
    :param angle: Kąt.
    :type angle: int.
    :returns: Druga składowa punktu na okręgu(x).

    '''
    return radius*math.cos(angle)

def array_angle_generator(begin_degree, end_degree, step_degree):
    '''Funkcja generująca tablicę kątów w stopniach z przedziału(begin_degree,begin_degree+step_degree, ..., end_degree>.

    :param begin_degree: Wartość pierwszego kąta w tablicy.
    :type begin_degree: int.
    :param end_degree: Wartość ostatniego kąta w tablicy.
    :type end_degree: int.
    :param step_degree: Krok o ile ma być zwiększony kolejny element tablicy.
    :type step_degree: type.
    :returns: Tablica przetasowanych kątów zamienionych na radiany.

    '''
    angles = []
    for i in range(begin_degree, end_degree, step_degree):
        angles.append(i)
    random.shuffle(angles)
    return np.radians(np.array(angles))