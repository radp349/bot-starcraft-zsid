import os
import sys

sys.path.append(os.path.abspath(os.getcwd()))
from absl import app
import copy
from pysc2.lib import actions, features
from pysc2.env import sc2_env, run_loop
from Strategies.TerranBot import TerranBot
from Strategies.ZergBot import ZergBot
from Strategies.ProtossBot import ProtossBot


class TerranBotRunner(TerranBot):
    def __init__(self):
        super(TerranBotRunner, self).__init__()


class ZergBotRunner(ZergBot):
    def __init__(self):
        super(ZergBotRunner, self).__init__()


class ProtossBotRunner(ProtossBot):
    def __init__(self):
        super(ProtossBotRunner, self).__init__()


def main(unused_argv):
    step_mul = 5

    setup_terran = {"agent": TerranBot(), "race": sc2_env.Race.terran,
                    "enemy_race": sc2_env.Race.zerg, "enemy_difficulty": sc2_env.Difficulty.hard}
    setup_protoss = {"agent": ProtossBot(), "race": sc2_env.Race.protoss,
                     "enemy_race": sc2_env.Race.protoss, "enemy_difficulty": sc2_env.Difficulty.medium}
    setup_zerg = {"agent": ZergBot(), "race": sc2_env.Race.zerg,
                  "enemy_race": sc2_env.Race.terran, "enemy_difficulty": sc2_env.Difficulty.cheat_insane}
    setup = setup_zerg
    setup_enemy = setup_protoss
    try:
        with sc2_env.SC2Env(
                map_name="WintersGateLE",
                players=[sc2_env.Agent(setup["race"]),
                         sc2_env.Agent(setup_enemy["race"])
                         ],
                agent_interface_format=features.AgentInterfaceFormat(
                    action_space=actions.ActionSpace.RAW,
                    use_raw_units=True,
                    raw_resolution=256,
                ),
                step_mul=step_mul,  # zmiana ilości pomijanych kroków
                disable_fog=False,
        ) as env:
            setup['agent'].create_predicator(env, step_mul)
            setup['agent'].enemy = setup_enemy['agent']
            run_loop.run_loop([setup["agent"], setup_enemy["agent"]], env, max_episodes=1000)

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    app.run(main)
