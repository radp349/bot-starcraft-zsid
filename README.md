# Bot Starcraft ZSID

Projekt w postaci bota do wszystkich ras w grze Starcraft II,  realizowany w ramach ZSID 2021

# Uruchomienie gry bot kontra człowiek
Wymagany jest zainstalowany Python 3.6+ oraz system opercyjny Windows 7 i nowszy.

Należy sklonować repozytorium
`git clone https://gitlab.com/radp349/bot-starcraft-zsid.git`

Następnie należy uruchomić plik setup.exe.

Gra powinna otworzyć w otwarym oknie gry.

Do odinstalowania zainstalowanych wcześnij biblioteki potrzebnych do uruchomienia gry wystarczy uruchomic plik uninstall.exe.


W przypadku problemów z plikem setup.exe należy:
1. Uruchomic dwa okna cmd.exe z folderu z repozytorium.
2. W pierwszym wpisać:
    `python -m human_vs_bot --human --map WintersGateLE --user_race <terran/protoss/zerg>`
3. W drugim wpisać:
    `python -m human_vs_bot --agent Test_random_bot.SemiRandomTerranAgent --agent_race terran --action_space RAW --use_raw_units`
4. Gra uruchomi się w otwratym oknie gry SC2.

