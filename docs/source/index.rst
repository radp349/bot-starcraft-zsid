Dokumentacja projektu grupy 2 z ZSID!
**********************************************

bot-starcraft-zsid mainFile
===============================

Główny plik
-------------------------------
.. automodule:: main
   :members:

GraNaBota
-------------------------------
.. automodule:: human_vs_bot  
   :members:

Liczenie kąta
-------------------------------
.. automodule:: angle
   :members:

GeneralMethods
---------------------------------------
.. automodule:: GeneralMethods
   :members:   

Predykcja
---------------------------------------
.. automodule:: main_predict_config
   :members:  

Setup instalacji
---------------------------------------
.. automodule:: setup
   :members:       

bot-starcraft-zsid AI
==================================
**Sekcja zajmująca się nauką bota.**


GeneralMethods
-----------------------------------
.. automodule:: AI.GeneralMethods
   :members: get_reward

Score
---------------------------------------
.. automodule:: TerranBot.score
   :members:
   :undoc-members:
   :special-members:

DeepQlearning
----------------------------------------
.. automodule:: AI.DeepQlearning_score
   :members:
   :private-members:

TerranBot
=====================================
**Część zawierająca bota rasy Terran.**


TerranAttack
--------------------------------------
.. automodule:: TerranBot.AttackMethods
   :members: 

TerranBuilding
---------------------------------------
.. automodule:: TerranBot.BuildingConstructs
   :members:          

TerranSearch
---------------------------------------
.. automodule:: TerranBot.EnemySearchAgentMethods
   :members:

TerranTrain
---------------------------------------
.. automodule:: TerranBot.TrainArmy
   :members:

TerranSkills
----------------------------------------
.. automodule:: TerranBot.UnitsSkills
   :members:

TerranUpgrades
-----------------------------------------
.. automodule:: TerranBot.ResearchUpgrades
   :members:

TerranBaseBuilding
-----------------------------------------
.. automodule:: TerranBot.BaseBuildingMethods
   :members:

GatheringMethods
--------------------------------------
.. automodule:: TerranBot.MineralGatheringMethods
   :members:   

ZergBot
============================================
**Część zawierająca bota rasy Zergów.**


ZergAttack
--------------------------------------

.. automodule:: ZergBot.AttackMethods
   :members:

ZergBuilding
--------------------------------------
.. automodule:: ZergBot.BaseBuildingMethods
   :members:

ZergConstructs
--------------------------------------

.. automodule:: ZergBot.BuildingConstructs
   :members:

ZergSearchMethods
---------------------------------------

.. automodule:: ZergBot.EnemySearchAgentMethods
   :members:

ZergGatheringMethods
-------------------------------------------

.. automodule:: ZergBot.MineralGatheringMethods
   :members:

ZergUpgrades
--------------------------------------------

.. automodule:: ZergBot.ResearchUpgrades
   :members:

ZergArmy
--------------------------------------------

.. automodule:: ZergBot.TrainArmy
   :members:

ZergSkills
------------------------------------------

.. automodule:: ZergBot.UnitsSkills
   :members:

ZergAgent
-------------------------------------------

.. automodule:: ZergBot.ZergAgent
   :members:

ProtossBot
==================================================
**Część zawierająca bota rasy Protossów.**


ProtossAttack
--------------------------------------

.. automodule:: ProtossBot.AttackMethods
   :members:

ProtossBuilding
--------------------------------------
.. automodule:: ProtossBot.BaseBuildingMethods
   :members:

ProtossConstructs
--------------------------------------

.. automodule:: ProtossBot.BuildingConstructs
   :members:

ProtossSearchMethods
---------------------------------------

.. automodule:: ProtossBot.EnemySearchAgentMethods
   :members:

ProtossGatheringMethods
-------------------------------------------

.. automodule:: ProtossBot.MineralGatheringMethods
   :members:

ProtossUpgrades
--------------------------------------------

.. automodule:: ProtossBot.ResearchUpgrades
   :members:

ProtossArmy
--------------------------------------------

.. automodule:: ProtossBot.TrainArmy
   :members:

ProtossSkills
------------------------------------------

.. automodule:: ProtossBot.UnitsSkills
   :members:

ProtossAgent
-------------------------------------------

.. automodule:: ProtossBot.ProtossAgent
   :members:


Strategies
=================================================
**Strategie poszczególnych botów w różnych momentach gry.**


TerranBot
--------------------------------------

.. automodule:: Strategies.TerranBot
   :members:

EarlyTerranStrategy
--------------------------------------

.. automodule:: Strategies.TerranStrategies.EarlyTerranStrategy
   :members:

MidTerranStrategy
--------------------------------------

.. automodule:: Strategies.TerranStrategies.MidTerranStrategy
   :members:

LateTerranStrategy
--------------------------------------

.. automodule:: Strategies.TerranStrategies.LateTerranStrategy
   :members:

SkyTerranTactics
--------------------------------------

.. automodule:: Strategies.TerranStrategies.SkyTerranTactics
   :members:     

TerranBaseStrategy
--------------------------------------

.. automodule:: Strategies.TerranStrategies.TerranBaseStrategy
   :members:            

TerranEarlyPush
--------------------------------------

.. automodule:: Strategies.TerranStrategies.TerranEarlyPush
   :members:  


ZergBot
--------------------------------------

.. automodule:: Strategies.ZergBot
   :members:

EarlyZergStrategy
--------------------------------------

.. automodule:: Strategies.ZergStrategies.EarlyZergStrategy
   :members:

MidZergStrategy
--------------------------------------

.. automodule:: Strategies.ZergStrategies.MidZergStrategy
   :members:

MidLateZergStrategy
--------------------------------------

.. automodule:: Strategies.ZergStrategies.MidLateZergStrategy
   :members:

ZergBaseStrategy
--------------------------------------

.. automodule:: Strategies.ZergStrategies.ZergBaseStrategy
   :members:

ZergRush
--------------------------------------

.. automodule:: Strategies.ZergStrategies.ZergRush
   :members:

ProtossBot
--------------------------------------

.. automodule:: Strategies.ProtossBot
   :members:

EarlyProtossStrategy
--------------------------------------

.. automodule:: Strategies.ProtossStrategies.EarlyProtossStrategy
   :members:

MidProtossStrategy
--------------------------------------

.. automodule:: Strategies.ProtossStrategies.MidProtossStrategy
   :members:

ProtossBaseStrategy
--------------------------------------

.. automodule:: Strategies.ProtossStrategies.ProtossBaseStrategy
   :members:


Predicator
=================================================
**Trenowanie i implementacja predykcji zwycięstwa.**


Predicator
--------------------------------------

.. automodule:: Predicator.Predicator
   :members:

ReplayParser
--------------------------------------

.. automodule:: Predicator.ReplayParser
   :members:

TrainPredicator
--------------------------------------

.. automodule:: Predicator.TrainPredicator
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. toctree::
   :maxdepth: 9
   :numbered:
   :hidden:
   :caption: Zawartość:

   index
