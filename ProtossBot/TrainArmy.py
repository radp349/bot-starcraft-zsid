from pysc2.lib import units, actions, features
import GeneralMethods as gm
import numpy as np

def train_probe( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Probe) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Probe), lub pusta akcja

    '''
    nexuses = gm.get_my_units_by_type(obs, units.Protoss.Nexus)
    if  len(nexuses)>0 and obs.observation.player.minerals >= 50 and gm.free_supply(obs) >= 1:
        nexus = nexuses[0]
        if nexus.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Probe_quick("now", nexus.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_zealot( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Zealot) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Zealot), lub pusta akcja

    '''
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    if (len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 100):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Zealot_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_sentry( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Sentry) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Sentry), lub pusta akcja

    '''
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    cybernetics_cores= gm.get_my_units_by_type(obs, units.Protoss.CyberneticsCore)
    if (len(cybernetics_cores)>0 and len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 100):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Sentry_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_stalker( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Stalker) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Stalker), lub pusta akcja

    '''
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    cybernetics_cores= gm.get_my_units_by_type(obs, units.Protoss.CyberneticsCore)
    if (len(cybernetics_cores)>0 and len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 125 and obs.observation.player.vespene >= 50):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Stalker_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()



def train_adept( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Adept) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Adept), lub pusta akcja

    '''
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    cybernetics_cores= gm.get_my_units_by_type(obs, units.Protoss.CyberneticsCore)
    if (len(cybernetics_cores)>0 and len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 25):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Adept_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_phoenix( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Phoenix) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Phoenix), lub pusta akcja

    '''
    stargates= gm.get_my_units_by_type(obs, units.Protoss.Stargate)
    if (len(stargates)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 100):
        stargate = stargates[0]
        if stargate.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Phoenix_quick("now", stargate.tag)
    return actions.RAW_FUNCTIONS.no_op()



def train_oracle( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Oracle) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Oracle), lub pusta akcja

    '''
    stargates= gm.get_my_units_by_type(obs, units.Protoss.Stargate)
    if (len(stargates)>0 and gm.free_supply(obs) >= 3 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150):
        stargate = stargates[0]
        if stargate.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Oracle_quick("now", stargate.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_void_ray( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Void Ray) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Void Ray), lub pusta akcja

    '''
    stargates= gm.get_my_units_by_type(obs, units.Protoss.Stargate)
    if (len(stargates)>0 and gm.free_supply(obs) >= 4 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >= 150):
        stargate = stargates[0]
        if stargate.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_VoidRay_quick("now", stargate.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_observer( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Observer) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Observer), lub pusta akcja

    '''
    robotics_facilities= gm.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
    if (len(robotics_facilities)>0 and gm.free_supply(obs) >= 1 and obs.observation.player.minerals >= 25 and obs.observation.player.vespene >= 75):
        robotics_facility = robotics_facilities[0]
        if robotics_facility.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Observer_quick("now", robotics_facility.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_warp_prism( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Warp Prism) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Warp Prism)), lub pusta akcja

    '''
    robotics_facilities= gm.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
    if (len(robotics_facilities)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 250):
        robotics_facility = robotics_facilities[0]
        if robotics_facility.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_WarpPrism_quick("now", robotics_facility.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_immortal( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Immortal) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Immortal), lub pusta akcja

    '''
    robotics_facilities= gm.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
    if (len(robotics_facilities)>0 and gm.free_supply(obs) >= 4 and obs.observation.player.minerals >= 275 and obs.observation.player.vespene >= 100):
        robotics_facility = robotics_facilities[0]
        if robotics_facility.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Immortal_quick("now", robotics_facility.tag)
    return actions.RAW_FUNCTIONS.no_op()



def train_high_templar( obs):
    '''Funkcja sprawdzająca czy dana jednostka (High Templar) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (High Templar), lub pusta akcja

    '''
    templar_archives= gm.get_my_units_by_type(obs, units.Protoss.TemplarArchive)
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    if (len(templar_archives)>0 and len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 150):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_HighTemplar_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_dark_templar( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Dark Templar) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Dark Templar), lub pusta akcja

    '''
    dark_shrines= gm.get_my_units_by_type(obs, units.Protoss.DarkShrine)
    gateways= gm.get_my_units_by_type(obs, units.Protoss.Gateway)
    if (len(dark_shrines)>0 and len(gateways)>0 and gm.free_supply(obs) >= 2 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 150):
        gateway = gateways[0]
        if gateway.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_DarkTemplar_quick("now", gateway.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_tempest( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Tempest) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Tempest), lub pusta akcja

    '''
    stargates= gm.get_my_units_by_type(obs, units.Protoss.Stargate)
    fleet_beacons= gm.get_my_units_by_type(obs, units.Protoss.FleetBeacon)
    if (len(fleet_beacons) >0 and len(stargates)>0 and gm.free_supply(obs) >= 5 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 175):
        stargate = stargates[0]
        if stargate.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Tempest_quick("now", stargate.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_carrier( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Carrier) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Carrier), lub pusta akcja

    '''
    stargates= gm.get_my_units_by_type(obs, units.Protoss.Stargate)
    fleet_beacons= gm.get_my_units_by_type(obs, units.Protoss.FleetBeacon)
    if (len(fleet_beacons) >0 and len(stargates)>0 and gm.free_supply(obs) >= 6 and obs.observation.player.minerals >= 350 and obs.observation.player.vespene >= 250):
        stargate = stargates[0]
        if stargate.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Carrier_quick("now", stargate.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_mothership( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Mothership) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Mothership), lub pusta akcja

    '''
    motherships= gm.get_my_units_by_type(obs, units.Protoss.Mothership)
    fleet_beacons= gm.get_my_units_by_type(obs, units.Protoss.FleetBeacon)
    nexuses= gm.get_my_units_by_type(obs, units.Protoss.Nexus)
    if (len(motherships) ==0 and len(fleet_beacons) >0 and len(nexuses)>0 and gm.free_supply(obs) >= 8 and obs.observation.player.minerals >= 400 and obs.observation.player.vespene >= 400):
        nexus = nexuses[0]
        if nexus.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Mothership_quick("now", nexus.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_colossus( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Colossus) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Colossus), lub pusta akcja

    '''
    robotics_facilities= gm.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
    robotics_bays= gm.get_my_units_by_type(obs, units.Protoss.RoboticsBay)
    if (len(robotics_facilities) >0 and len(robotics_bays)>0 and gm.free_supply(obs) >= 6 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >= 200):
        robotics_facility = robotics_facilities[0]
        if robotics_facility.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Colossus_quick("now", robotics_facility.tag)
    return actions.RAW_FUNCTIONS.no_op()

def train_disruptor( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Disruptor) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Disruptor), lub pusta akcja

    '''
    robotics_facilities= gm.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
    robotics_bays= gm.get_my_units_by_type(obs, units.Protoss.RoboticsBay)
    if (len(robotics_facilities) >0 and len(robotics_bays)>0 and gm.free_supply(obs) >= 3 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150):
        robotics_facility = robotics_facilities[0]
        if robotics_facility.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Disruptor_quick("now", robotics_facility.tag)
    return actions.RAW_FUNCTIONS.no_op()



def morph_archon(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Archon) może zostać wytrenowana z dwóch jednostek (High/Dark Templar), a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Archon), lub pusta akcja

    '''
    templars= gm.get_my_units_by_type(obs, units.Protoss.DarkTemplar)
    templars.extend( gm.get_my_units_by_type(obs, units.Protoss.HighTemplar))
    if len(templars)>0:
        templars = templars[:2]
        templars_tags = [unit.tag for unit in templars]
        return actions.RAW_FUNCTIONS.Morph_Archon_quick("now", templars_tags)
    return actions.RAW_FUNCTIONS.no_op()