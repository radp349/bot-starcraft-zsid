from pysc2.lib import units, actions, features
import GeneralMethods as gm
import numpy as np
import angle as ang


def build_concrete_pylon(obs,first_base_cords,angle,r):
    '''Funkcja zlecająca budowę pylonu o konkretnym kącie i promieniu od bazy głównej najbliższemu próbnikowi.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za pylonu.

    '''
    probes = gm.get_my_completed_units_by_type(obs, units.Protoss.Probe)                   # tablica wszystkich robotników
    x = ang.sin_processing(r, angle) + 0.5   # wyznaczenie pierwszej składowej(x) punktu na okręgu, plus wyrównanie
    y = ang.cos_processing(r, angle)         # wyznaczenie drugiej składowej(y) punktu na okręgu
    exps = gm.get_my_bases(obs)              # uzyskanie listy posiadanych baz
    if obs.observation.player.minerals >= 100 and len(probes) > 0 and len(exps)>0:   # warunki dla budynku

        pylon_xy = (first_base_cords[0] + x, first_base_cords[1] + y)  # pozycja budowy magazynu na okręgu
        distances = gm.get_distances(obs, probes, pylon_xy)  # wykrycie i zangażowanie do budowy
        probe = probes[np.argmin(distances)]                          # najbliższego robotnika
        return actions.RAW_FUNCTIONS.Build_Pylon_pt("now", probe.tag, pylon_xy)
    return actions.RAW_FUNCTIONS.no_op()                          # przywołanie akcji budowania magazynu


def build_pylon(obs,first_base_cords ):
    '''Funkcja zlecająca budowę pylonu najbliższemu próbnikowi.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za pylonu.

    '''

    probes = gm.get_my_completed_units_by_type(obs, units.Protoss.Probe)
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    
    exps = gm.get_my_bases(obs)              # uzyskanie listy posiadanych baz
    if obs.observation.player.minerals >= 100 and len(probes) > 0 and len(exps)>0:   # warunki dla budynku

        r = 6
        angle = 0
        angles = ang.array_angle_generator(0, 360, 45) 

        for a in angles:
            free_place = True
            angle = a
            x = ang.sin_processing(r, a)
            y = ang.cos_processing(r, a)
            pylon_xy = (exps[0].x + x, exps[0].y + y)  # pozycja budowy magazynu na okręgu
            
            for pylon in pylons:
                if pylon.x == pylon_xy[0] and pylon.y ==pylon_xy[1]:
                    free_place = False
                    break

            if free_place == True:
                break

        distances = gm.get_distances(obs, probes, pylon_xy)  # wykrycie i zangażowanie do budowy
        probe = probes[np.argmin(distances)]                          # najbliższego robotnika
        return actions.RAW_FUNCTIONS.Build_Pylon_pt("now", probe.tag, pylon_xy)
    return actions.RAW_FUNCTIONS.no_op()                          # przywołanie akcji budowania magazynu


def build_gateway(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę bramy najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę bramy.

    '''
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    gateways = gm.get_my_completed_units_by_type(obs, units.Protoss.Gateway)
    exps = gm.get_my_bases(obs)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)

    if  len(pylons) > 0 and len(probes) > 0 and obs.observation.player.minerals >= 200:

        r = 9.5
        angle = 0

        if base_top_left:
            angles = ang.array_angle_generator(270, 550, 35) 
        else:
            angles = ang.array_angle_generator(90, 370, 35)   

        for a in angles:
            free_place = True
            angle = a
            x = ang.sin_processing(r, a)
            y = ang.cos_processing(r, a)
            gateway_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
            
            for gateway in gateways:
                if gateway.x == gateway_xy[0] and gateway.y ==gateway_xy[1]:
                    free_place = False
                    break

            if free_place == True:
                break


        distances = gm.get_distances(obs, pylons, gateway_xy)
        if np.min(distances)>8:
            return build_concrete_pylon(obs,first_base_cords,angle,r-3.5)


        distances = gm.get_distances(obs, probes, gateway_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Gateway_pt(
            "now", probe.tag, gateway_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_forge(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę kuźni najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę kuźni.

    '''
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    exps = gm.get_my_bases(obs)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 17.25
    if base_top_left:
                          
        angle = ang.angle_generator(125, 145, 20)  
    else:
        angle = ang.angle_generator(305, 325, 20)  
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(pylons) >0 and len(forges) == 0 and len(exps) > 0 and obs.observation.player.minerals >= 150 and len(
            probes) > 0:
        forge_xy = (first_base_cords[0] + x, first_base_cords[1] + y)

        distances = gm.get_distances(obs, pylons, forge_xy)
        
        if np.min(distances)>6.5:
            
            return build_concrete_pylon(obs,first_base_cords,angle, r-3.75)
        
        
        distances = gm.get_distances(obs, probes, forge_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Forge_pt(
            "now", probe.tag, forge_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_cybernetics_core(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę rdzenia cybernetycznego najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę rdzenia cybernetycznego.

    '''
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    gateways = gm.get_my_completed_units_by_type(obs, units.Protoss.Gateway)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 18
    if base_top_left:
                          
        angle = ang.angle_generator(102, 122, 20)  
    else:
        angle = ang.angle_generator(282, 302, 20)  
    x = ang.sin_processing(r, angle)               
    y = ang.cos_processing(r, angle) 
    if len(cybernetics_cores) == 0 and len(gateways) > 0 and obs.observation.player.minerals >= 150 and \
            len(probes) > 0:
        cybernetics_cores_xy = (first_base_cords[0] + x, first_base_cords[1] + y)

        distances = gm.get_distances(obs, pylons, cybernetics_cores_xy)
        if np.min(distances)>6.5:
            
            return build_concrete_pylon(obs,first_base_cords,angle,r-3.5)

        distances = gm.get_distances(obs, probes, cybernetics_cores_xy)
        probe = probes[np.argmin(distances)]

        return actions.RAW_FUNCTIONS.Build_CyberneticsCore_pt(
            "now", probe.tag, cybernetics_cores_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_shield_battery(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę generatora osłon najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę generatora osłon.

    '''
    shield_batteries = gm.get_my_completed_units_by_type(obs, units.Protoss.ShieldBattery)
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(shield_batteries) == 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 100 and \
            len(probes) > 0:
        shield_battery_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, probes, shield_battery_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_ShieldBattery_pt(
            "now", probe.tag,shield_battery_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_twilight_council(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę rady zmroku najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę rady zmroku.

    '''
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 18
    if base_top_left:
                          
        angle = ang.angle_generator(87, 117, 20)  
    else:
        angle = ang.angle_generator(267, 287, 20)   
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(pylons) and len(twilight_councils) == 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100 and len(probes) > 0:
        twilight_council_xy = (first_base_cords[0] + x, first_base_cords[1] + y)

        distances = gm.get_distances(obs, pylons, twilight_council_xy)
        if np.min(distances)>6.5:
            return build_concrete_pylon(obs,first_base_cords,angle, r-3.5)
        
        distances = gm.get_distances(obs, probes,twilight_council_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_TwilightCouncil_pt(
            "now", probe.tag,twilight_council_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_stargate(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę gwiezdnych wrót najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę gwiezdnych wrót.

    '''
    stargates = gm.get_my_completed_units_by_type(obs, units.Protoss.Stargate)
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(stargates) == 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 150 and len(probes) > 0:
        stargate_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, probes,stargate_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Stargate_pt(
            "now", probe.tag,stargate_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_robotics_facility(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę zakładu robotyki najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę zakładu robotyki.

    '''
    robotics_facilities = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsFacility)
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    if len(pylons) > 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100 and len(probes) > 0:

        

        r = 19

        angle = 0

        if base_top_left:
                            
            angles = ang.array_angle_generator(40, 60, 20)  
        else:
            angles = ang.array_angle_generator(240, 260, 20) 

        for a in angles:
            free_place = True
            angle = a
            x = ang.sin_processing(r, angle)
            y = ang.cos_processing(r, angle)
            robotics_facility_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
            
            for robotics_facility in robotics_facilities:
                if robotics_facility.x == robotics_facility_xy[0] and robotics_facility.y ==robotics_facility_xy[1]:
                    free_place = False
                    break

            if free_place == True:
                break



        distances = gm.get_distances(obs, pylons, robotics_facility_xy)
        if np.min(distances)>8:
            return build_concrete_pylon(obs,first_base_cords,angle, r-5)

        distances = gm.get_distances(obs, probes,robotics_facility_xy)
        probe = probes[np.argmin(distances)]

        return actions.RAW_FUNCTIONS.Build_RoboticsFacility_pt(
            "now", probe.tag,robotics_facility_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_templar_archive(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę archiwów templariuszy najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę archiwów templariuszy.

    '''
    templar_archives = gm.get_my_completed_units_by_type(obs, units.Protoss.TemplarArchive)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(templar_archives) == 0 and len(twilight_councils) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 200 and len(probes) > 0:
        templar_archive_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, probes,templar_archive_xy)
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_TemplarArchive_pt(
            "now", probe.tag,templar_archive_xy)
    return actions.RAW_FUNCTIONS.no_op()

def build_dark_shrine(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę mrocznej świątyni najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę mrocznej świątyni.

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    pylons = gm.get_my_completed_units_by_type(obs, units.Protoss.Pylon)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)

    if len(pylons)>0 and len(twilight_councils) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 150 and len(probes) > 0:

        r = 18
        if base_top_left:
                            
            angle = ang.angle_generator(80, 110, 20)  
        else:
            angle = ang.angle_generator(260, 280, 20)   
        x = ang.sin_processing(r, angle)
        y = ang.cos_processing(r, angle)
        dark_shrine_xy = (first_base_cords[0] + x, first_base_cords[1] + y)

        distances = gm.get_distances(obs, pylons, dark_shrine_xy)
        if np.min(distances)>6.5:
            return build_concrete_pylon(obs,first_base_cords,angle, r-3.5)

        distances = gm.get_distances(obs, probes,dark_shrine_xy )
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_DarkShrine_pt(
            "now", probe.tag,dark_shrine_xy )
    return actions.RAW_FUNCTIONS.no_op()

def build_fleet_beacon(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę stacji nawigacyjnej floty najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę stacji nawigacyjnej floty.

    '''
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    stargates = gm.get_my_completed_units_by_type(obs, units.Protoss.Stargate)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(fleet_beacons) == 0 and len(stargates) > 0 and obs.observation.player.minerals >= 300 and \
            obs.observation.player.vespene >= 200 and len(probes) > 0:
        fleet_beacon_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, probes,fleet_beacon_xy )
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_FleetBeacon_pt(
            "now", probe.tag,fleet_beacon_xy )
    return actions.RAW_FUNCTIONS.no_op()

def build_robotics_bay(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę stacji robotyki najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę stacji robotyki.

    '''
    robotics_bays = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsBay)
    robotic_facilities = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsFacility)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    pylons = gm.get_my_units_by_type(obs, units.Protoss.Pylon)
    r = 18

    angle = 0

    if base_top_left:
                        
        angles = ang.array_angle_generator(60, 80, 20)  
    else:
        angles = ang.array_angle_generator(220, 240, 20) 

    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(robotics_bays) == 0 and len(robotic_facilities) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 150 and len(probes) > 0:
        robotics_bay_xy = (first_base_cords[0] + x, first_base_cords[1] + y)

        
        for a in angles:
            free_place = True
            angle = a
            x = ang.sin_processing(r, angle)
            y = ang.cos_processing(r, angle)
            robotics_bay_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
            
            for robotics_bay in robotics_bays:
                if robotics_bay.x == robotics_bay_xy[0] and robotics_bay.y ==robotics_bay_xy[1]:
                    free_place = False
                    break

            if free_place == True:
                break



        distances = gm.get_distances(obs, pylons, robotics_bay_xy)
        if np.min(distances)>8:
            return build_concrete_pylon(obs,first_base_cords,angle, r-4)

        distances = gm.get_distances(obs, probes,robotics_bay_xy )
        probe = probes[np.argmin(distances)]

        return actions.RAW_FUNCTIONS.Build_RoboticsBay_pt(
            "now", probe.tag,robotics_bay_xy )
    return actions.RAW_FUNCTIONS.no_op()

def build_photon_cannon(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę działa fotonowego najbliższemu próbnikowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę działa fotonowego.

    '''
    photon_cannons = gm.get_my_completed_units_by_type(obs, units.Protoss.PhotonCannon)
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(photon_cannons) == 0 and len(forges) > 0 and obs.observation.player.minerals >= 150 and \
            len(probes) > 0:
        photon_cannon_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, probes,photon_cannon_xy )
        probe = probes[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_PhotonCannon_pt(
            "now", probe.tag,photon_cannon_xy )
    return actions.RAW_FUNCTIONS.no_op()


