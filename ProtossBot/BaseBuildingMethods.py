import numpy as np
from pysc2.lib import units, actions, features
import GeneralMethods
import GeneralMethods as gm
import random


def potential_location_for_new_base(obs, first_base_cords):
    '''
    Funkcja szukająca miejsc na nowe potencjalne bazy do zbudowania.
    
    
    :param obs: Obecny stan gry.
    :type obs: timestep.
    
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.

    
    :returns: Lista tupli z koordynatami (x, y).
    '''
    neutral_vespene_geysers = []
    neutral_vespene_geysers_names = [units.Neutral.VespeneGeyser, units.Neutral.ProtossVespeneGeyser,
                                     units.Neutral.PurifierVespeneGeyser, units.Neutral.RichVespeneGeyser,
                                     units.Neutral.ShakurasVespeneGeyser]
    for name in neutral_vespene_geysers_names:
        neutral_vespene_geysers.extend(gm.get_units_by_type(obs, name))
    command_centers = GeneralMethods.get_my_bases(obs)
    if len(command_centers) == 0:
        return [first_base_cords]
    command_centers = list(GeneralMethods.sort_by_dist(obs, first_base_cords, command_centers))

    # wysrodkowanie pozycji pomiedzy 2 gejzerami:
    coordinates_fixed = []
    coordinates_fixed_close = []
    for coordinate in neutral_vespene_geysers:
        tmp_dist = gm.get_distances(obs, neutral_vespene_geysers, (coordinate.x, coordinate.y))
        min = np.argmin(tmp_dist)
        if tmp_dist[min] == 0:
            tmp_dist[min] = 400
            min = np.argmin(tmp_dist)
        if 30 > tmp_dist[min] > 14:
            x1, y1 = (coordinate.x, coordinate.y)
            x2, y2 = (neutral_vespene_geysers[min].x, neutral_vespene_geysers[min].y)
            avgx = abs(int((x1 + x2) / 2))
            avgy = abs(int((y1 + y2) / 2))
            coordinates_fixed.append((avgx, avgy))
        elif tmp_dist[min] <= 14:
            x1, y1 = (coordinate.x, coordinate.y)
            x2, y2 = (neutral_vespene_geysers[min].x, neutral_vespene_geysers[min].y)
            avgx = abs(int((x1 + x2) / 2))
            avgy = abs(int((y1 + y2) / 2))
            coordinates_fixed_close.append((avgx, avgy))

    coordinates_fixed = list(dict.fromkeys(coordinates_fixed))  # usuniecie powtorek
    coordinates_fixed_close = list(dict.fromkeys(coordinates_fixed_close))

    potenial_new_base_coordinates = []
    potenial_new_base_coordinates_close = []

    if len(command_centers) > 0:
        for potenial_base in coordinates_fixed:
            tmp_dist = gm.get_distances(obs, command_centers, (potenial_base[0], potenial_base[1]))
            if tmp_dist[np.argmin(tmp_dist)] > 25:
                potenial_new_base_coordinates.append(potenial_base)

        for potenial_base in coordinates_fixed_close:
            tmp_dist = gm.get_distances(obs, command_centers, (potenial_base[0], potenial_base[1]))
            if tmp_dist[np.argmin(tmp_dist)] > 25:
                potenial_new_base_coordinates_close.append(potenial_base)

    else:
        potenial_new_base_coordinates = coordinates_fixed


    minerals = GeneralMethods.cluster(GeneralMethods.get_minerals(obs))
    cluster = []
    for m in minerals:
        cluster.append(GeneralMethods.mean_of_units(m))
    base_coordinates = []
    base_coordinates_close = []
    for coordinate in potenial_new_base_coordinates:
        distances = np.linalg.norm(np.array(cluster) - np.array((coordinate[0], coordinate[1])), axis=1)
        meanOfMinerals = cluster[np.argmin(distances)].tolist()
        coordinate = list(coordinate)
        if coordinate[0] < meanOfMinerals[0]:
            coordinate[0] -= 3
        else:
            coordinate[0] += 3
        if coordinate[1] < meanOfMinerals[1]:
            coordinate[1] -= 3
        else:
            coordinate[1] += 3
        base_coordinates.append(coordinate)

    for coordinate in potenial_new_base_coordinates_close:  # jak u gory tylko dla gejzerow blisko siebie
        distances = np.linalg.norm(np.array(cluster) - np.array((coordinate[0], coordinate[1])), axis=1)
        meanOfMinerals = cluster[np.argmin(distances)].tolist()
        coordinate = list(coordinate)

        if coordinate[0] < 126:  # wyrownanie czy w lewo czy w prawo w zaleznosci od polowy mapy
            sign = -1
        else:
            sign = 1

        if coordinate[0] < meanOfMinerals[0]:
            coordinate[0] -= sign * 11
        else:
            coordinate[0] += sign * 11

        if coordinate[1] < meanOfMinerals[1]:
            coordinate[1] += 5
        else:
            coordinate[1] -= 5
        base_coordinates_close.append(coordinate)

    base_coordinates.extend(base_coordinates_close)

    return base_coordinates


def build_nexus(first_base_cords, obs):
    '''
    Funkcja budująca nowe bazy.
    
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    
    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns: Akcja budowy nowej bazy (hatchers).
    '''
    nexuses = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Nexus)
    probes = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Probe)
    
    if (len(nexuses) < 4 and obs.observation.player.minerals >= 400 and
            len(probes) > 0):
        potential_locations = potential_location_for_new_base(obs, first_base_cords)
        if len(potential_locations) > 0:
            distances = GeneralMethods.get_distances_from_points(obs, potential_locations, first_base_cords)
            nexus_xy = potential_locations[np.argmin(distances)]
            distances = GeneralMethods.get_distances(obs, probes, nexus_xy)
            probe = probes[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Build_Nexus_pt(
                "now", probe.tag, nexus_xy)
    return actions.RAW_FUNCTIONS.no_op()
