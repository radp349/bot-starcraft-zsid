from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm
import numpy as np

def research_ground_weapons_level1( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Weapons Level1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Weapons Level1), lub pusta akcja

    '''
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    if len(forges) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundWeaponsLevel1_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_ground_armor_level1( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level 1), lub pusta akcja

    '''
    forges =gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    if len(forges) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundArmorLevel1_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_ground_shields_level1( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Shields Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Shields Level 1), lub pusta akcja

    '''
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    if len(forges) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossShieldsLevel1_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_air_weapons_level1( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Weapons Level 1), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    if len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirWeaponsLevel1_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_air_armor_level1( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Armor Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Armor Level 1), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    if len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirArmorLevel1_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_charge( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Charge), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Charge), lub pusta akcja

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(twilight_councils) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        twilight_councils = [unit.tag for unit in twilight_councils]
        return actions.RAW_FUNCTIONS.Research_Charge_quick(
            "now",twilight_councils[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_blink( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Blink), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Blink), lub pusta akcja

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(twilight_councils) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        twilight_councils = [unit.tag for unit in twilight_councils]
        return actions.RAW_FUNCTIONS.Research_Blink_quick(
            "now",twilight_councils[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_resonating_glaives( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Resonating Glaives), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Resonating Glaives), lub pusta akcja

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(twilight_councils) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        twilight_councils = [unit.tag for unit in twilight_councils]
        return actions.RAW_FUNCTIONS.Research_AdeptResonatingGlaives_quick(
            "now",twilight_councils[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_weapons_level2( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Weapons Level 2), lub pusta akcja

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)

    if len(twilight_councils) > 0 and len(forges)>0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundWeaponsLevel2_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_armor_level2( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level 2), lub pusta akcja

    '''
    forges =gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(twilight_councils)>0 and len(forges) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundArmorLevel2_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_ground_shields_level2( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Shields Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Shields Level 2), lub pusta akcja

    '''
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(forges) > 0 and len(twilight_councils)>0 and obs.observation.player.minerals >= 225 and obs.observation.player.vespene >=225:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossShieldsLevel2_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_weapons_level3( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Weapons Level 3), lub pusta akcja

    '''
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)

    if len(twilight_councils) > 0 and len(forges)>0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >=200:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundWeaponsLevel3_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_armor_level3( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level3), lub pusta akcja

    '''
    forges =gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(twilight_councils)>0 and len(forges) > 0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >=200:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossGroundArmorLevel3_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_ground_shields_level3( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Shields Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Shields Level 3), lub pusta akcja

    '''
    forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
    twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
    if len(forges) > 0 and len(twilight_councils)>0 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >=300:
        forges = [unit.tag for unit in forges]
        return actions.RAW_FUNCTIONS.Research_ProtossShieldsLevel3_quick(
            "now",forges[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_psionic_storm( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Psionic Storm), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Psionic Storm), lub pusta akcja

    '''
    templar_archives = gm.get_my_completed_units_by_type(obs, units.Protoss.TemplarArchive)
    if len(templar_archives) > 0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >=200:
        templar_archives= [unit.tag for unit in templar_archives]
        return actions.RAW_FUNCTIONS.Research_PsiStorm_quick(
            "now",templar_archives[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_anion_pulse_cristal( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Anion Pulse Cristal), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Anion Pulse Cristal), lub pusta akcja

    '''
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    if len(fleet_beacons) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        fleet_beacons= [unit.tag for unit in fleet_beacons]
        return actions.RAW_FUNCTIONS.Research_PhoenixAnionPulseCrystals_quick(
            "now",fleet_beacons[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_air_weapons_level2( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Weapons Level 2), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    if len(fleet_beacons) > 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >=175:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirWeaponsLevel2_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_air_armor_level2( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Armor Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Armor Level 2), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    if len(fleet_beacons) > 0 and  len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 225 and obs.observation.player.vespene >=225:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirArmorLevel2_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_air_weapons_level3( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Weapons Level 3), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    if len(fleet_beacons) > 0 and len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >=250:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirWeaponsLevel3_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_air_armor_level3( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Air Armor Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Air Armor Level 3), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    fleet_beacons = gm.get_my_completed_units_by_type(obs, units.Protoss.FleetBeacon)
    if len(fleet_beacons) > 0 and  len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >=300:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_ProtossAirArmorLevel3_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_gravitic_boosters( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Gravitic Boosters), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Gravitic Boosters), lub pusta akcja

    '''
    robotics_bays = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsBay)
    if  len(robotics_bays) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        robotics_bays = [unit.tag for unit in robotics_bays]
        return actions.RAW_FUNCTIONS.Research_GraviticBooster_quick(
            "now",robotics_bays[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_gravitic_drive( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Gravitic Drive), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Gravitic Drive), lub pusta akcja

    '''
    robotics_bays = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsBay)
    if  len(robotics_bays) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        robotics_bays = [unit.tag for unit in robotics_bays]
        return actions.RAW_FUNCTIONS.Research_GraviticDrive_quick(
            "now",robotics_bays[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_extended_thermal_lance( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Extended Thermal Lance), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Extended Thermal Lance), lub pusta akcja

    '''
    robotics_bays = gm.get_my_completed_units_by_type(obs, units.Protoss.RoboticsBay)
    if  len(robotics_bays) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        robotics_bays = [unit.tag for unit in robotics_bays]
        return actions.RAW_FUNCTIONS.Research_ExtendedThermalLance_quick(
            "now",robotics_bays[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_warp_gate( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Warp Gate), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Warp Gate), lub pusta akcja

    '''
    cybernetics_cores = gm.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)
    if  len(cybernetics_cores) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >=50:
        cybernetics_cores = [unit.tag for unit in cybernetics_cores]
        return actions.RAW_FUNCTIONS.Research_WarpGate_quick(
            "now",cybernetics_cores[0])
    return actions.RAW_FUNCTIONS.no_op()