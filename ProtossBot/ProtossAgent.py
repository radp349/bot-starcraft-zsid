import random
import numpy as np
from pysc2.agents import base_agent
from pysc2.lib import actions, features, units
from ProtossBot import TrainArmy as ta
from ProtossBot import ResearchUpgrades as ru
from ProtossBot import BuildingConstructs as bc
# import UnitsSkills as us
import GeneralMethods as gm
import ProtossBot.BaseBuildingMethods as bbm
import ProtossBot.EnemySearchAgentMethods as essam
import ProtossBot.AttackMethods as am
import ProtossBot.MineralGatheringMethods as mgm


class LearningProtossAgent(base_agent.BaseAgent):
    def step(self, obs):
        super(LearningProtossAgent, self).step(obs)
        if obs.first():
            nexus = gm.get_my_units_by_type(
                obs, units.Protoss.Nexus)[0]
            self.base_top_left = (nexus.x < 128)
            self.first_base_cords = (nexus.x, nexus.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               "build_assimilator",
               "build_pylon",
               "build_gateway",
               "build_cybernetics_core",
               "scout",
               "train_zealot",
               "train_stalker",
               "attack",
               "train_probe",
               "harvest_all_minerals"
              )

    # metoda "nic nie rób"
    def do_nothing(self, obs):
        return actions.RAW_FUNCTIONS.no_op()

    def harvest_all_minerals(self, obs):
        return mgm.harvest_all_minerals(obs)

    def build_hatchery(self, obs):
        return bbm.build_hatchery(self.first_base_cords, obs)

    def scout(self, obs):
        return essam.scout(self, obs)

    def attack(self, obs):
        zealots = gm.get_my_units_by_type(obs, units.Protoss.Zealot)
        stalkers = gm.get_my_units_by_type(obs, units.Protoss.Stalker)
        if len(zealots) +len(stalkers)> 5:
            return am.attack(obs)
        return actions.RAW_FUNCTIONS.no_op()

    def build_assimilator(self, obs):
        return mgm.build_assimilator(obs, self.first_base_cords)

    def build_pylon(self, obs):
        #if gm.free_supply(obs) >= 7:
        #    return actions.RAW_FUNCTIONS.no_op()
        return bc.build_pylon(obs,self.first_base_cords)

    def build_cybernetics_core(self, obs):
        #if gm.free_supply(obs) >= 7:
        #    return actions.RAW_FUNCTIONS.no_op()
        return bc.build_cybernetics_core(obs,self.first_base_cords,  self.base_top_left)

    def build_gateway(self, obs):
        return bc.build_gateway(obs,self.first_base_cords,self.base_top_left)

    def scout(self, obs):
        return essam.scout(self, obs)

    def train_zealot(self, obs):
        return ta.train_zealot(obs)

    def train_stalker(self, obs):
        return ta.train_stalker(obs)

    def train_probe(self, obs):
        return ta.train_probe(obs)
