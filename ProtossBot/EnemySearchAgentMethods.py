import numpy as np
from pysc2.lib import units, actions, features
import GeneralMethods as gm
import random
import math


def ally_scouting_units(scout_unit_chosen, obs):
    '''
    Funkcja sprawdzająca, czy jest przydzielona jednostka do scoutowania lub przydzielająca nową.
    Przydzielany do zwiadu jest dron.
    
    :param scout_unit_chosen: Lista danych wybranej jednostki w agenice lub lista pusta.
    :type scout_unit_chosen: lista jednostek.
    
    :param obs: Obecny stan gry.
    :type obs: timestep.
    '''

    probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
    observers = gm.get_my_units_by_type(obs, units.Protoss.Observer)
    scout_unit_chosen.clear()
    if len(observers) >0:
        scout_unit_chosen.append(observers[0])
    elif len(probes) > 9:
        scout_unit_chosen.append(probes[0])
    else:
        scout_unit_chosen.clear()


def scout(self, obs):
    '''
    Funkcja wykonująca scout. W wypadku nie posiadania wybranej jednostki do scoutu lub jej śmierci wyznacza ją za pomocą funkcji ally_scouting_units.
    
    :param self: Self agenta zergów.
    :type self: agent.
    
    :param obs: Obecny stan gry.
    :type obs: timestep.
    
    :returns: Lista akcji przemieszczenia się lub lista z akcją nic nie robienia.
    '''
    acts = []
    potentail_attack_coordinates = potential_location_by_gaysers(self, obs)
    if len(potentail_attack_coordinates) > 0:
        scvs = gm.get_my_units_by_type(obs, units.Protoss.Probe)
        observers = gm.get_my_units_by_type(obs, units.Protoss.Observer)

        if len(self.scout_unit_chosen) == 0:
            ally_scouting_units(self.scout_unit_chosen, obs)

        unit = []
        if len(self.scout_unit_chosen) != 0:
            unit = [scv for scv in scvs if scv.tag == self.scout_unit_chosen[0].tag]

            if len(unit) == 0:
                unit = [observer for observer in observers if observer.tag == self.scout_unit_chosen[0].tag]
                if len(unit) == 0:
                    ally_scouting_units(self.scout_unit_chosen, obs)
            else:
                if len(observers)>0:
                    ally_scouting_units(self.scout_unit_chosen, obs)
                    unit = [observer for observer in observers if observer.tag == self.scout_unit_chosen[0].tag]


        if len(self.scout_unit_chosen) != 0 and len(potentail_attack_coordinates) > 0 and len(unit) > 0:
            if self.scouting_target >= len(potentail_attack_coordinates):
                self.scouting_target = 0

            place = (potentail_attack_coordinates[self.scouting_target][0],
                     potentail_attack_coordinates[self.scouting_target][1])
            x = place[0] - unit[0].x
            y = place[1] - unit[0].y
            dist = math.sqrt(x ** 2 + y ** 2)
            if dist < 4:
                self.scouting_target += 1
                if self.scouting_target >= len(potentail_attack_coordinates):
                    self.scouting_target = 0
                place = (potentail_attack_coordinates[self.scouting_target][0],
                         potentail_attack_coordinates[self.scouting_target][1])
            acts.append(actions.RAW_FUNCTIONS.Move_pt("now", unit[0].tag, place))
            return acts
    acts.append(actions.RAW_FUNCTIONS.no_op())
    return acts


def potential_location_by_gaysers(self, obs):
    '''
    Funkcja wyznaczająca potencjalne miejsca, w których może znajdować się baza przeciwnika.
    
    :param self: Self agenta zergów.
    :type self: agent.
    
    :param obs: Obecny stan gry.
    :type obs: timestep.
    
    :returns: Lista miejsc potencjalnego ataku posortowane odległoscią od naszej pierwszej bazy.
    '''
    neutral_vespene_geysers = []
    neutral_vespene_geysers_names = [units.Neutral.VespeneGeyser, units.Neutral.ProtossVespeneGeyser,
                                     units.Neutral.PurifierVespeneGeyser, units.Neutral.RichVespeneGeyser,
                                     units.Neutral.ShakurasVespeneGeyser]
    for name in neutral_vespene_geysers_names:
        neutral_vespene_geysers.extend(gm.get_units_by_type(obs, name))
    refineries = gm.get_my_units_by_type(obs, units.Protoss.Assimilator)

    indexs = []
    index = 0
    for geyser in neutral_vespene_geysers:
        for refinery in refineries:
            if (geyser.x, geyser.y) == (refinery.x, refinery.y):
                indexs.append(index)
        index += 1

    substract_index = 0
    for index in indexs:
        neutral_vespene_geysers.pop(index - substract_index)
        substract_index += 1
    neutral_geysers = neutral_vespene_geysers

    if len(neutral_geysers) > 0:
        coordinates = []
        hatcheries = gm.get_my_bases(obs)
        if len(hatcheries) > 0:
            hatcheries = list(gm.sort_by_dist(obs, self.first_base_cords, hatcheries))
            distancesgeysers = gm.get_distances(obs, neutral_geysers, (hatcheries[0].x, hatcheries[0].y))
            distancesgeysers = distancesgeysers.tolist()
            for geyser in range(len(neutral_geysers)):
                coordinates.append(neutral_geysers[np.argmax(distancesgeysers)])
                neutral_geysers.pop(np.argmax(distancesgeysers))
                distancesgeysers.pop(np.argmax(distancesgeysers))

            # wysrodkowanie pozycji pomiedzy 2 gejzerami:
            coordinates_fixed = []
            for coordinate in coordinates:
                tmp_dist = gm.get_distances(obs, coordinates, (coordinate.x, coordinate.y))
                min = np.argmin(tmp_dist)
                if tmp_dist[min] == 0:
                    tmp_dist[min] = 200
                    min = np.argmin(tmp_dist)
                if 22 > tmp_dist[min] > 0:
                    x1, y1 = (coordinate.x, coordinate.y)
                    x2, y2 = (coordinates[min].x, coordinates[min].y)
                    avgx = abs(int((x1 + x2) / 2))
                    avgy = abs(int((y1 + y2) / 2))
                    coordinates_fixed.append((avgx, avgy))

            coordinates_fixed = list(dict.fromkeys(coordinates_fixed))
            coordinates_fixed_sorted = []
            tmp = gm.get_distances_from_points(obs, coordinates_fixed, self.first_base_cords)
            if len(tmp) > 0:
                dist = list(tmp)
                for i in range(len(coordinates_fixed)):
                    tmp = dist.index(max(dist))
                    coordinates_fixed_sorted.append(coordinates_fixed[tmp])
                    coordinates_fixed.pop(tmp)
                    dist.pop(tmp)
            return coordinates_fixed_sorted
    return []
