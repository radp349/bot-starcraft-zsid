from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm
import numpy as np

def use_guardian_shield(obs):
    '''Funkcja zwracająca akcję użycia Guardian Shield

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Guardian Shield lub pusta akcja

    '''
    sentries = gm.get_my_units_by_type(obs, units.Protoss.Sentry)
    enemy_units = gm.get_enemy_combat_units(obs)
    acts = []
    if len(sentries) > 0 and len(enemy_units)>0:
        for sentry in sentries:
            distances = gm.get_distances(obs, enemy_units,(sentry.x, sentry.y))
            if np.min(distances)<=10:
                acts.append(actions.RAW_FUNCTIONS.Effect_GuardianShield_quick("now", sentry.tag))

        if len(acts)>0: 
            return acts
    
    return actions.RAW_FUNCTIONS.no_op()

def use_blink_stalker(obs, first_base_cords):
    '''Funkcja zwracająca akcję użycia Blink

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Blink lub pusta akcja

    '''
    stalkers = gm.get_my_units_by_type(obs, units.Protoss.Stalker)
    enemy_units = gm.get_enemy_combat_units(obs)
    acts = []
    if len(stalkers) > 0 and len(enemy_units)>0:
        for stalker in stalkers:
            distances = gm.get_distances(obs, enemy_units,(stalker.x, stalker.y))

            if np.min(distances)<=7 and stalker.shield<=40:

                acts.append(actions.RAW_FUNCTIONS.Effect_Blink_Stalker_pt("now", stalker.tag,first_base_cords))

        if len(acts)>0:

            return acts
    
    return actions.RAW_FUNCTIONS.no_op()


