import numpy as np
from pysc2.lib import units, actions, features
from sklearn.cluster import DBSCAN
import random
import GeneralMethods as GeneralMethods


def do_nothing():
    return actions.RAW_FUNCTIONS.no_op()


def harvest_minerals(obs, commandCenter, scv_tags):
    '''
    Funkcja przypisująca danym scv akcje zbierania najbliższych minerałów

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param commandCenter: Instancja bazy, do której scvka ma zanosić minerały.
    :type commandCenter: NamedNumpyArray.
    :param scv_tags: Id poszczególnych scv, która ma mieć przypisaną akcję.
    :type scv_tags: list.
    :returns: akcja, która przypisze scv zbieranie minerałów do dananej bazy

    '''
    minerals = GeneralMethods.get_minerals(obs)
    distances = GeneralMethods.get_distances(obs, minerals, (commandCenter.x, commandCenter.y))
    mineral_patch = minerals[np.argmin(distances)]
    # return actions.RAW_FUNCTIONS.Move_pt("now", scvs, (30, 30))
    return actions.RAW_FUNCTIONS.Harvest_Gather_SCV_unit(
        "now", scv_tags, mineral_patch.tag)


def harvest_vespene(obs, scv_tags):
    '''
    Funkcja sprawdza w których rafineriach brakuje robotników, a następnie zleca
    zbieranie vespenu scv uzupełniając liczbę robotników do trzech

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param scv_tags: Id poszczególnych scv, która ma mieć przypisaną akcję.
    :type scv_tags: list.
    :returns: akcja, która przypisze scv zbieranie vespenu w niepełnej rafinerii
    '''
    refineries = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Assimilator)
    refineries_without_harvests = [x for x in refineries if x.assigned_harvesters < 3]

    if len(refineries_without_harvests) > 0:
        refinery = random.choice(refineries_without_harvests)
        missing_harvests = 3 - refinery.assigned_harvesters
        scvs = []
        index = 0
        for i in range(missing_harvests):
            if index < len(scv_tags):
                scv = scv_tags[index]
                scvs.append(scv)
                index += 1
        return actions.RAW_FUNCTIONS.Harvest_Gather_unit(
            "now", scvs, refinery.tag)
    return False


def build_assimilator(obs, first_base_cords):
    '''
    Funkcja sprawdzająca dostępne możliwe miejsca do budowy rafinerii wokół naszych baz,
    następnie zlecająca budowę rafinerii najbliższej scv

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :returns: pusta akcja lub akcja odpowiedzialna za budowę rafinerii
    '''
    neutral_vespene_geysers = []
    neutral_vespene_geysers_names = [units.Neutral.VespeneGeyser, units.Neutral.ProtossVespeneGeyser, units.Neutral.PurifierVespeneGeyser, units.Neutral.RichVespeneGeyser, units.Neutral.ShakurasVespeneGeyser]
    for name in neutral_vespene_geysers_names:
        neutral_vespene_geysers.extend(GeneralMethods.get_units_by_type(obs, name))
    refineries = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Assimilator)
    indexs = []
    index = 0
    if len(refineries) > 0:
        for geyser in neutral_vespene_geysers:
            for refinery in refineries:
                if (geyser.x, geyser.y) == (refinery.x, refinery.y):
                    indexs.append(index)
            index += 1
        substract_index = 0
        for index in indexs:
            neutral_vespene_geysers.pop(index - substract_index)
            substract_index += 1
        neutral_geysers = neutral_vespene_geysers
    else:
        neutral_geysers = neutral_vespene_geysers
    exps = GeneralMethods.get_my_bases(obs)

    scvs = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Probe)
    if len(exps) > 0:
        exps = list(GeneralMethods.sort_by_dist(obs, first_base_cords, exps))
        if len(refineries) < (2 * len(exps)) and len(neutral_geysers) > 0 and obs.observation.player.minerals >= 75 and len(
                scvs) > 0:
            index = int(len(refineries)/2)
            distancesGeysers = GeneralMethods.get_distances(obs, neutral_geysers,
                                                                (exps[index].x, exps[index].y))
            geyser = neutral_geysers[np.argmin(distancesGeysers)]
            distances = GeneralMethods.get_distances(obs, scvs, (geyser.x, geyser.y))
            scv = scvs[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Build_Assimilator_unit(
                "now", scv.tag, geyser.tag)
    return actions.RAW_FUNCTIONS.no_op()


def harvest_all_minerals(obs):
    '''
    Główna funkcja odpowiedzialna za zbieranie minerałów. Jako listę bezużytecznych scv przyjmuje scv z order_length == 0,
    następnie do tej listy przypisuje scv będące "nadwyżką" zbierając minerały dla bazych. Mając tę liste, rozdysponuje scv
    przypisując je do wolnych rafinerii, a następnie "uzupełniając" pozostałe bazy.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: pusta akcja lub lista akcji zbierania minerałów i vespenu
    '''
    all_scvs = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Probe)
    idle_scvs = [scv for scv in all_scvs if scv.order_length == 0]
    minerals = GeneralMethods.cluster(GeneralMethods.get_minerals(obs))

    cluster = []
    surplus = []
    exp_index = 0
    for m in minerals:
        cluster.append(GeneralMethods.mean_of_units(m))
    exps = GeneralMethods.get_my_bases(obs)
    if len(exps) > 0:

        for exp in exps:
            distances = np.linalg.norm(np.array(cluster) - np.array((exp.x, exp.y)), axis=1)
            nearestMinerals = minerals[np.argmin(distances)]
            surplus.append(exp.assigned_harvesters - 2 * len(nearestMinerals))

        for valueWorkers in surplus:
            if valueWorkers > 0:
                all_scvs = GeneralMethods.get_my_units_by_type(obs, units.Protoss.Probe)
                distances = np.linalg.norm(np.array(cluster) - np.array(
                    (exps[exp_index].x, exps[exp_index].y)), axis=1)
                nearestMinerals = minerals[np.argmin(distances)]
                mean = GeneralMethods.mean_of_units(nearestMinerals)
                for i in range(valueWorkers):
                    distances = GeneralMethods.get_distances(obs, all_scvs, (mean[0], mean[1]))
                    index = np.argmin(distances) + i
                    if index < len(all_scvs):
                        scv = all_scvs[index]
                        idle_scvs.append(scv)

    if len(idle_scvs) > 0:
        scv_tags = [unit.tag for unit in idle_scvs]
        result = harvest_vespene(obs, scv_tags)
        if result and obs.observation.player.vespene - 250 <= obs.observation.player.minerals:
            return result
        if len(surplus) > exp_index + 1:
            if surplus[exp_index + 1] < 0:
                return harvest_minerals(obs, exps[exp_index + 1], scv_tags)
    return actions.RAW_FUNCTIONS.no_op()
