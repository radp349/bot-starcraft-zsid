import random
import numpy as np
from pysc2.agents import base_agent
from pysc2.lib import actions, features, units
from ZergBot import TrainArmy as ta
from ZergBot import ResearchUpgrades as ru
from ZergBot import BuildingConstructs as bc
import ZergBot.UnitsSkills as us
import GeneralMethods as gm
import ZergBot.BaseBuildingMethods as bbm
import ZergBot.EnemySearchAgentMethods as essam
import ZergBot.AttackMethods as am
import ZergBot.MineralGatheringMethods as mgm


class LearningZergAgent(base_agent.BaseAgent):
    def step(self, obs):
        super(LearningZergAgent, self).step(obs)
        if obs.first():
            hatchery = gm.get_my_units_by_type(
                obs, units.Zerg.Hatchery)[0]
            self.base_top_left = (hatchery.x < 128)
            self.first_base_cords = (hatchery.x, hatchery.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               "build_extractor",
               "build_overlord",
               "build_spawning_pool",
               "build_baneling_nest",
               "build_roach_warren",
               "build_hatchery",
               "build_lair",
               "build_hive",
               "build_hydralisk_den",
               "train_drone",
               "train_drone",
               "train_drone",
               "train_zergling",
               "train_queen",
               "train_queen",
               "plant_larva",
               "train_roach",
               "train_hydralisk",
               "train_baneling",
               "scout",
               "attack",
               # "do_research", # uncomment when method is ready
               "harvest_all_minerals")

    # metoda "nic nie rób"
    def do_nothing(self, obs):
        return actions.RAW_FUNCTIONS.no_op()

    def do_research(self, obs):
        #print("placeholder")
        pass

    def harvest_all_minerals(self, obs):
        return mgm.harvest_all_minerals(obs)

    def build_hatchery(self, obs):
        return bbm.build_hatchery(self.first_base_cords, obs)

    def scout(self, obs):
        return essam.scout(self, obs)

    def plant_larva(self, obs):
        return us.plant_larva(obs, self.first_base_cords)

    def attack(self, obs):
        army = gm.get_my_combat_units(obs)
        if len(army) > 15:
            return am.attack(obs)
        return actions.RAW_FUNCTIONS.no_op()

    # metoda służąca do wydobywania minerałów
    def harvest_minerals(self, obs):
        drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
        idle_drones = [drone for drone in drones if drone.order_length == 0]
        if len(idle_drones) > 0:
            mineral_patches = [unit for unit in obs.observation.raw_units
                               if unit.unit_type in [
                                   units.Neutral.BattleStationMineralField,
                                   units.Neutral.BattleStationMineralField750,
                                   units.Neutral.LabMineralField,
                                   units.Neutral.LabMineralField750,
                                   units.Neutral.MineralField,
                                   units.Neutral.MineralField750,
                                   units.Neutral.PurifierMineralField,
                                   units.Neutral.PurifierMineralField750,
                                   units.Neutral.PurifierRichMineralField,
                                   units.Neutral.PurifierRichMineralField750,
                                   units.Neutral.RichMineralField,
                                   units.Neutral.RichMineralField750
                               ]]
            drone = random.choice(idle_drones)
            distances = gm.get_distances(obs, mineral_patches, (drone.x, drone.y))
            mineral_patch = mineral_patches[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Harvest_Gather_unit(
                "now", drone.tag, mineral_patch.tag)
        return actions.RAW_FUNCTIONS.no_op()

    def build_extractor(self, obs):
        return mgm.build_refinery(obs, self.first_base_cords)

    def build_overlord(self, obs):
        if gm.free_supply(obs) >= 7:
            return actions.RAW_FUNCTIONS.no_op()
        return bc.build_overlord(obs)

    def build_spawning_pool(self, obs):
        if len(gm.get_my_bases(obs)) < 2:
            return actions.RAW_FUNCTIONS.no_op()
        return bc.build_spawning_pool(obs, self.first_base_cords,self.base_top_left)

    def build_evolution_chamber(self, obs):
        return bc.build_evolution_chamber(obs, self.first_base_cords,self.base_top_left)

    def build_roach_warren(self, obs):
        return bc.build_roach_warren(obs, self.first_base_cords,self.base_top_left)

    def build_baneling_nest(self, obs):
        return bc.build_baneling_nest(obs, self.first_base_cords,self.base_top_left)

    def build_spine_crawler(self, obs):
        return bc.build_spine_crawler(obs, self.first_base_cords,self.base_top_left)

    def build_spore_crawler(self, obs):
        return bc.build_spore_crawler(obs, self.first_base_cords,self.base_top_left)

    def build_lair(self, obs):
        return bc.build_lair(obs)

    def build_hydralisk_den(self, obs):
        return bc.build_hydralisk_den(obs, self.first_base_cords,self.base_top_left)

    def build_infestation_pit(self, obs):
        return bc.build_infestation_pit(obs, self.first_base_cords,self.base_top_left)

    def build_spire(self, obs):
        return bc.build_spire(obs, self.first_base_cords,self.base_top_left)

    def build_nydus_network(self, obs):
        return bc.build_nydus_network(obs, self.first_base_cords,self.base_top_left)

    def build_lurker_den(self, obs):
        return bc.build_lurker_den(obs, self.first_base_cords,self.base_top_left)

    def build_hive(self, obs):
        return bc.build_hive(obs)

    def build_ultralisk_cavern(self, obs):
        return bc.build_ultralisk_cavern(obs, self.first_base_cords,self.base_top_left)

    def build_greater_spire(self, obs):
        return bc.build_greater_spire(obs)

    def train_drone(self, obs):
        return ta.train_drone(obs)

    def train_zergling(self, obs):
        return ta.train_zergling(obs)

    def train_queen(self, obs):
        return ta.train_queen(obs)

    def train_roach(self, obs):
        return ta.train_roach(obs)

    def train_baneling(self, obs):
        return ta.train_baneling(obs)

    def train_infestor(self, obs):
        return ta.train_infestor(obs)

    def train_swarm_host(self, obs):
        return ta.train_swarm_host(obs)

    def train_mutalisk(self, obs):
        return ta.train_mutalisk(obs)

    def train_corruptor(self, obs):
        return ta.train_corruptor(obs)

    def train_hydralisk(self, obs):
        return ta.train_hydralisk(obs)

    def train_viper(self, obs):
        return ta.train_viper(obs)

    def train_ultralisk(self, obs):
        return ta.train_ultralisk(obs)

    def train_broodlord(self, obs):
        return ta.train_broodlord(obs)
