from pysc2.lib import units, actions, features
import GeneralMethods as gm


def train_drone( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Drone) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Drone), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    bases = gm.get_my_bases(obs)
    if len(drones) >= len(bases) * 18 or len(drones) > 100:
        return actions.RAW_FUNCTIONS.no_op()
    if len(larvas) > 0 and obs.observation.player.minerals >= 50 and gm.free_supply(obs) > 0:
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Drone_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_zergling( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Zergling) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Zergling), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    spawning_pools = gm.get_my_units_by_type(obs, units.Zerg.SpawningPool)
    if (len(larvas) > 0 and len(spawning_pools)>0 and gm.free_supply(obs) > 0 and obs.observation.player.minerals >= 25):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Zergling_quick("now", larvas)
    return actions.RAW_FUNCTIONS.no_op()


def train_queen(obs, first_base_cords):
    '''
    Funkcja sprawdzająca czy dana jednostka (Queen) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Queen), lub pusta akcja

    '''
    bases1 = gm.get_my_completed_units_by_type(obs, units.Zerg.Hatchery)
    bases2 = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    bases = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    bases.extend(bases1)
    bases.extend(bases2)
    if len(bases) < 1:
        return actions.RAW_FUNCTIONS.no_op()
    quens = gm.get_my_units_by_type(obs, units.Zerg.Queen)
    queens_under_construction = gm.check_number_of_upgrade_in_queue(516, bases)
    if len(quens) + queens_under_construction >= len(bases):
        return actions.RAW_FUNCTIONS.no_op()

    bases = gm.sort_by_dist(obs, first_base_cords, bases)
    bases_without_queen = []
    for base in bases:
        tmp = gm.check_number_of_upgrade_in_queue(516, [base])
        if len(quens) > 0:
            dist = gm.get_distances(obs, quens, (base.x, base.y))
            min_dist = min(dist)
        else:
            min_dist = 1000
        if min_dist > 12 and tmp == 0:
            bases_without_queen.append(base)

    spawning_pools = gm.get_my_units_by_type(obs, units.Zerg.SpawningPool)
    if len(spawning_pools) > 0 and gm.free_supply(obs) > 2 and\
            obs.observation.player.minerals >= 150:
        return actions.RAW_FUNCTIONS.Train_Queen_quick("now", bases_without_queen[0])

    return actions.RAW_FUNCTIONS.no_op()


def train_roach(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Roach) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Roach), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    roach_warrens = gm.get_my_units_by_type(obs, units.Zerg.RoachWarren)
    if (len(larvas) > 0  and len(roach_warrens) > 0 and obs.observation.player.minerals >= 75 and obs.observation.player.vespene >= 25):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Roach_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_baneling(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Baneling) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Baneling), lub pusta akcja

    '''
    zerglings = gm.get_my_units_by_type(obs, units.Zerg.Zergling)
    banelings = gm.get_my_units_by_type(obs, units.Zerg.Baneling)
    baneling_nests = gm.get_my_units_by_type(obs, units.Zerg.BanelingNest)
    if (len(banelings) < 8 and len(zerglings) > 12  and len(baneling_nests) > 0 and obs.observation.player.minerals >= 25 and obs.observation.player.vespene >= 25):
        zerglings = [unit.tag for unit in zerglings[:8]]
        return actions.RAW_FUNCTIONS.Train_Baneling_quick("now", zerglings)
    return actions.RAW_FUNCTIONS.no_op()


def train_ravager(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Ravager) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Ravager), lub pusta akcja

    '''
    roach = gm.get_my_units_by_type(obs, units.Zerg.Roach)
    roach_warren = gm.get_my_units_by_type(obs, units.Zerg.RoachWarren)
    if (len(roach) > 6  and len(roach_warren) > 0 and obs.observation.player.minerals >= 25 and obs.observation.player.vespene >= 75):
        return actions.RAW_FUNCTIONS.Morph_Ravager_quick("now", roach[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_overseer(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Ravager) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Ravager), lub pusta akcja

    '''
    overlords = gm.get_my_units_by_type(obs, units.Zerg.Overlord)
    lair = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    lair.extend(gm.get_my_units_by_type(obs, units.Zerg.Hive))
    if (len(overlords) > 1 and len(lair) > 0 and
            obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 50):
        return actions.RAW_FUNCTIONS.Morph_Overseer_quick("now", overlords[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_infestor(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Infestor) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Infestor), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    infestation_pits = gm.get_my_units_by_type(obs, units.Zerg.InfestationPit)
    if (len(larvas) > 0  and len(infestation_pits) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 150):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Infestor_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_swarm_host(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Swarm Host) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Swarm Host), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    infestation_pits = gm.get_my_units_by_type(obs, units.Zerg.InfestationPit)
    if (len(larvas) > 0  and len(infestation_pits) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 75):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_SwarmHost_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_mutalisk(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Mutalisk) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Mutalisk), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    greater_spires = gm.get_my_units_by_type(obs, units.Zerg.GreaterSpire)    
    if (len(larvas) > 0 and len(spires) + len(greater_spires) > 0
            and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Mutalisk_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_corruptor(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Corruptor) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Corruptor), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    greater_spires = gm.get_my_units_by_type(obs, units.Zerg.GreaterSpire)   
    if (len(larvas) > 0  and len(spires)  +len(greater_spires)> 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 100):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Corruptor_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_hydralisk(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Hydralisk) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Hydralisk), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    hydralisk_dens = gm.get_my_units_by_type(obs, units.Zerg.HydraliskDen)
    if (len(larvas) > 0  and len(hydralisk_dens) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 50):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Hydralisk_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_viper(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Viper) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Viper), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if (len(larvas) > 0  and len(hives) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 200):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Viper_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_ultralisk( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Ultralisk) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Ultralisk), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    ultralisk_caverns = gm.get_my_units_by_type(obs, units.Zerg.UltraliskCavern)
    if (len(larvas) > 0  and len(ultralisk_caverns) > 0 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >= 200):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Ultralisk_quick("now", larvas[0])
    return actions.RAW_FUNCTIONS.no_op()


def train_broodlord( obs):
    '''Funkcja sprawdzająca czy dana jednostka (Broodlord) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Broodlord), lub pusta akcja

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    greater_spires = gm.get_my_units_by_type(obs, units.Zerg.GreaterSpire)
    if (len(larvas) > 0  and len(greater_spires) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Broodlord_quick("now", larvas)
    return actions.RAW_FUNCTIONS.no_op()
