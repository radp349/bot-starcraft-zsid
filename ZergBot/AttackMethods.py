from pysc2.lib import actions, units
import GeneralMethods as GenM
import numpy as np


def attack(obs, first_base_cords, group_action=True):
    """
    Funkcja zwracająca kolejkę rozkazów ataku. Cele ataku w kolejności:

    1. najbliższa i najdalsza jednostka bojowa,
    2. najbliższy i najdalszy robotnik,
    3. najbliższy i najdalszy budynek,
    4. jeżeli nie ma innych celów zaatakowane zostanie najdalsze pole minerałów.



    Jeżeli jednostki jednostki gracza są daleko od siebie to zostanie wydany rozkaz zgrupowania.


    :param group_action:
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Lista akcji ataków punktowych
    """
    my_units = GenM.get_my_combat_units(obs)
    if len(my_units) < 1:
        return actions.RAW_FUNCTIONS.no_op()
    my_units_tags = np.array([unit.tag for unit in my_units])
    enemy_buildings = GenM.get_enemy_buildings(obs)
    enemy_combat_units = GenM.get_enemy_combat_units(obs)
    enemy_workers = GenM.get_enemy_workers(obs)
    completed_hatcheries = GenM.get_my_bases(obs)
    attack_order = []
    # zergling optimization
    if group_action:
        # sorting units by distance
        units_sorted = GenM.sort_by_dist(obs, first_base_cords, my_units)

        # getting 80% of army that's furthest to not block it from getting backup from base
        units_sorted = units_sorted[int(0.2 * len(units_sorted)):]

        # checking, how far are main force units from the furthest one
        distances_from_furthest = GenM.get_distances(obs, units_sorted, (units_sorted[-1].x, units_sorted[-1].y))

        # if the distance is greater than 16 (about size of natural) group at his place
        if max(distances_from_furthest) > 16:
            # print(max(distances_from_furthest))
            army = GenM.get_my_combat_units(obs)
            if len(army) < 5:
                return attack_order
            zergling_tags_local = np.array([unit.tag for unit in army])
            middle_of_zerglings = GenM.mean_of_units(units_sorted)
            attack_order.append(actions.RAW_FUNCTIONS.Attack_pt("now", zergling_tags_local,
                                                                (units_sorted[-4].x, units_sorted[-4].y)))

            return attack_order

    if len(completed_hatcheries) > 0:
        base_pos = (completed_hatcheries[0].x, completed_hatcheries[0].y)
        if len(enemy_combat_units) > 0:
            distances = GenM.get_distances(obs, enemy_combat_units, base_pos)
            attack_target = enemy_combat_units[np.argmin(distances)]
            attack_target_prio = None
            for enemy in enemy_buildings:
                if enemy.unit_type == units.Protoss.ShieldBattery:
                    attack_target_prio = enemy
                    break

            for enemy in enemy_combat_units:
                if enemy.unit_type == units.Terran.Medivac or \
                        enemy.unit_type == units.Terran.SiegeTank or \
                        enemy.unit_type == units.Terran.Liberator or \
                        enemy.unit_type == units.Protoss.Sentry or \
                        enemy.unit_type == units.Protoss.DarkTemplar or \
                        enemy.unit_type == units.Zerg.Baneling or \
                        enemy.unit_type == units.Zerg.Lurker:
                    attack_target_prio = enemy
                    break

            if attack_target_prio is not None:
                attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("now", my_units_tags,
                                                                             attack_target_prio.tag))

            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))

            attack_target = enemy_combat_units[np.argmax(distances)]
            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))

        if len(enemy_workers) > 0:
            distances = GenM.get_distances(obs, enemy_workers, base_pos)
            attack_target = enemy_workers[np.argmin(distances)]
            attack_position = (attack_target.x, attack_target.y)
            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))
            attack_target = enemy_workers[np.argmax(distances)]
            attack_position = (attack_target.x, attack_target.y)
            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))
        if len(enemy_buildings) > 0:
            distances = GenM.get_distances(obs, enemy_buildings, base_pos)
            attack_target = enemy_buildings[np.argmin(distances)]
            attack_position = (attack_target.x, attack_target.y)
            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))
            attack_target = enemy_buildings[np.argmax(distances)]
            attack_position = (attack_target.x, attack_target.y)
            attack_order.append(actions.RAW_FUNCTIONS.Attack_Attack_unit("queued", my_units_tags,
                                                                         attack_target.tag))
        else:
            minerals = GenM.cluster(GenM.get_minerals(obs))
            cluster = []
            for m in minerals:
                cluster.append(GenM.mean_of_units(m))

            distances = np.linalg.norm(np.array(cluster)
                                       - np.array(base_pos),
                                       axis=1)
            attack_position = cluster[np.argmax(distances)]
            attack_order.append(actions.RAW_FUNCTIONS.Attack_pt("queued", my_units_tags,
                                                                (attack_position[0], attack_position[1])))
        return attack_order
    return actions.RAW_FUNCTIONS.no_op()


def disengage(obs, first_base_cords):
    '''
    Funkcja zwracająca pojednynczy rozkaz powrotu do najbardziej wysuniętej bazy.
    Rozkaz jest akcją ataku punktowego wię jednostki w trakcie powrotu będą atakowały napotakane wrogie jednostki.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :returns: Akcja powrotu do bazy lub pusta akcja

    '''
    my_combat_units = GenM.get_my_combat_units(obs)
    my_units_tags = np.array([unit.tag for unit in my_combat_units])
    if len(my_combat_units) > 0:
        command_centers = GenM.get_my_bases(obs)
        if len(command_centers) == 0:
            base_pos = first_base_cords
        else:
            command_centers = list(GenM.sort_by_dist(obs, first_base_cords, command_centers))
            base_pos = (command_centers[-1].x, command_centers[-1].y)
        return actions.RAW_FUNCTIONS.Attack_pt("now", my_units_tags, base_pos)
    return actions.RAW_FUNCTIONS.no_op()


def disengage_lowest_hp(obs):
    '''
    Funkcja zwracająca rozkaz powrotu do bazy jednostki z najmniejszym poziomem zdrowia.
    Celem tej funkcji jest wycofywanie rannych jednostek.

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns: Akcja powrotu do bazy lub pusta akcja

    '''
    my_combat_units = GenM.get_my_combat_units(obs)
    combat_units_healths = [unit.health for unit in my_combat_units]
    combat_units_tags = [unit.tag for unit in my_combat_units]
    completed_command_centers = GenM.get_my_bases(obs)
    if len(my_combat_units) > 0 & len(completed_command_centers) > 0:
        base_pos = (completed_command_centers[0].x, completed_command_centers[0].y)
        disengaged_unit = combat_units_tags[np.argmin(combat_units_healths)]
        return actions.RAW_FUNCTIONS.Move_pt("now", disengaged_unit, base_pos)
    return actions.RAW_FUNCTIONS.no_op()
