from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm
import numpy as np


def research_pneumatized_carapace(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Pneumatized Carapace), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Pneumatized Carapace), lub pusta akcja

    '''
    hatcheries = gm.get_my_units_by_type(obs, units.Zerg.Hatchery)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()
    if len(bases) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        exps = [unit.tag for unit in bases]
        return actions.RAW_FUNCTIONS.Research_PneumatizedCarapace_quick(
            "now", exps[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_metabolic_boost(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Metabolic Boost), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Metabolic Boost), lub pusta akcja

    '''
    spawning_pools = gm.get_my_units_by_type(obs, units.Zerg.SpawningPool)
    if len(spawning_pools) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        spawning_pools = [unit.tag for unit in spawning_pools]
        return actions.RAW_FUNCTIONS.Research_ZerglingMetabolicBoost_quick(
            "now", spawning_pools[0])
    return actions.RAW_FUNCTIONS.no_op()

def research_adrenal_glands(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Adrenal Glands), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Adrenal Glands), lub pusta akcja

    '''
    spawning_pools = gm.get_my_units_by_type(obs, units.Zerg.SpawningPool)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()
    if len(spawning_pools) > 0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >= 200:
        spawning_pools = [unit.tag for unit in spawning_pools]
        return actions.RAW_FUNCTIONS.Research_ZerglingAdrenalGlands_quick(
            "now",spawning_pools[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_glial_regeneration( obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Glial Regeneration), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Glial Regeneration), lub pusta akcja

    '''
    roach_warrens = gm.get_my_units_by_type(obs, units.Zerg.RoachWarren)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()
    if len(roach_warrens) > 0 and len(bases) and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        roach_warrens = [unit.tag for unit in roach_warrens]
        return actions.RAW_FUNCTIONS.Research_GlialRegeneration_quick(
            "now",roach_warrens[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_tunneling_claws(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Tunneling Claws), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Tunneling Claws), lub pusta akcja

    '''
    roach_warrens = gm.get_my_units_by_type(obs, units.Zerg.RoachWarren)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(roach_warrens) > 0 and len(bases) and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        roach_warrens = [unit.tag for unit in roach_warrens]
        return actions.RAW_FUNCTIONS.Research_TunnelingClaws_quick(
            "now",roach_warrens[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_centrifugal_hooks(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Centrifugal Hooks), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Centrifugal Hooks), lub pusta akcja

    '''
    baneling_nests = gm.get_my_units_by_type(obs, units.Zerg.BanelingNest)
    if len(baneling_nests) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        baneling_nests = [unit.tag for unit in baneling_nests]
        return actions.RAW_FUNCTIONS.Research_CentrifugalHooks_quick(
            "now",baneling_nests[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_melee_weapons_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Melee Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Melee Weapons Level 1), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    if len(evolution_chambers) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMeleeWeaponsLevel1_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_melee_weapons_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Melee Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Melee Weapons Level 2), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)

    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(evolution_chambers) > 0 and len(bases) > 0 and obs.observation.player.minerals >= 150\
            and obs.observation.player.vespene >= 150:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMeleeWeaponsLevel2_quick(
            "now", evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_melee_weapons_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Melee Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Melee Weapons Level 3), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(evolution_chambers) > 0 and len(hives) > 0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >=200:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMeleeWeaponsLevel3_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_missile_weapons_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Missile Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Missile Weapons Level 1), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    if len(evolution_chambers) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMissileWeaponsLevel1_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_missile_weapons_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Missile Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Missile Weapons Level 2), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)

    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(evolution_chambers) > 0 and len(bases) > 0 and obs.observation.player.minerals >= 150\
            and obs.observation.player.vespene >= 150:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMissileWeaponsLevel2_quick(
            "now", evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_missile_weapons_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Missile Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Missile Weapons Level 3), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(evolution_chambers) > 0 and len(hives)>0 and obs.observation.player.minerals >= 200 and obs.observation.player.vespene >=200:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergMissileWeaponsLevel3_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_armor_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level 1), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    if len(evolution_chambers) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergGroundArmorLevel1_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_armor_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level 2), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)

    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(evolution_chambers) > 0 and len(bases) > 0 and obs.observation.player.minerals >= 225 and obs.observation.player.vespene >=225:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergGroundArmorLevel2_quick(
            "now", evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_ground_armor_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ground Armor Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ground Armor Level 3), lub pusta akcja

    '''
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(evolution_chambers) > 0 and len(hives)>0 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >=300:
        evolution_chambers = [unit.tag for unit in evolution_chambers]
        return actions.RAW_FUNCTIONS.Research_ZergGroundArmorLevel3_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_attack_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Attack Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Attack Level 1), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    if len(spires) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerAttackLevel1_quick(
            "now",spires[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_attack_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Attack Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Attack Level 2), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)

    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(spires) > 0 and len(bases)>0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >=175:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerAttackLevel2_quick(
            "now",spires[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_attack_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Attack Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Attack Level 3), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    evolution_chambers = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(spires) > 0 and len(hives)>0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >=250:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerAttackLevel3_quick(
            "now",evolution_chambers[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_carapace_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Carapace Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Carapace Level 1), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    if len(spires) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerArmorLevel1_quick(
            "now",spires[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_carapace_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Carapace Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Carapace Level 2), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)

    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()

    if len(spires) > 0 and len(bases)>0 and obs.observation.player.minerals >= 225 and obs.observation.player.vespene >=225:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerArmorLevel2_quick(
            "now",spires[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_flyer_carapace_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Flyer Carapace Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Flyer Carapace Level 3), lub pusta akcja

    '''
    spires = gm.get_my_units_by_type(obs, units.Zerg.Spire)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(spires) > 0 and len(hives)>0 and obs.observation.player.minerals >= 300 and obs.observation.player.vespene >=300:
        spires = [unit.tag for unit in spires]
        return actions.RAW_FUNCTIONS.Research_ZergFlyerArmorLevel3_quick(
            "now",spires[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_neural_parasite(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Neural Parasite), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Neural Parasite), lub pusta akcja

    '''
    infestation_pits = gm.get_my_units_by_type(obs, units.Zerg.InfestationPit)
    if len(infestation_pits) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        infestation_pits = [unit.tag for unit in infestation_pits]
        return actions.RAW_FUNCTIONS.Research_NeuralParasite_quick(
            "now",infestation_pits[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_pathogen_glands(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Pathogen Glands), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Pathogen Glands), lub pusta akcja

    '''
    infestation_pits = gm.get_my_units_by_type(obs, units.Zerg.InfestationPit)
    if len(infestation_pits) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        infestation_pits = [unit.tag for unit in infestation_pits]
        return actions.RAW_FUNCTIONS.Research_PathogenGlands_quick(
            "now",infestation_pits[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_grooved_spines(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Grooved Spines), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Grooved Spines), lub pusta akcja

    '''
    hydralisk_dens = gm.get_my_units_by_type(obs, units.Zerg.HydraliskDen)
    if len(hydralisk_dens) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        hydralisk_dens = [unit.tag for unit in hydralisk_dens]
        return actions.RAW_FUNCTIONS.Research_GroovedSpines_quick(
            "now",hydralisk_dens[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_muscular_augments(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Muscular Augments), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Muscular Augments), lub pusta akcja

    '''
    hydralisk_dens = gm.get_my_units_by_type(obs, units.Zerg.HydraliskDen)
    lairs = gm.get_my_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lairs) > 0:
        bases = lairs
    elif len(hives) > 0:
        bases = hives
    else:
        return actions.RAW_FUNCTIONS.no_op()
    if len(hydralisk_dens) > 0 and len(bases)>0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >=100:
        hydralisk_dens = [unit.tag for unit in hydralisk_dens]
        return actions.Research_MuscularAugments_quick(
            "now",hydralisk_dens[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_adaptive_talons(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Adaptive Talons), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Adaptive Talons), lub pusta akcja

    '''
    lurker_dens = gm.get_my_units_by_type(obs, units.Zerg.LurkerDen)
    hives = gm.get_my_units_by_type(obs, units.Zerg.Hive)
    if len(lurker_dens) > 0 and len(hives)>0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        lurker_dens = [unit.tag for unit in lurker_dens]
        return actions.Research_AdaptiveTalons_quick(
            "now",lurker_dens[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_chitinous_plating(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Chitinous Plating), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Chitinous Plating), lub pusta akcja

    '''
    ultralisk_caverns = gm.get_my_units_by_type(obs, units.Zerg.UltraliskCavern)
    if len(ultralisk_caverns) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        ultralisk_caverns= [unit.tag for unit in ultralisk_caverns]
        return actions.Research_ChitinousPlating_quick(
            "now",ultralisk_caverns[0])
    return actions.RAW_FUNCTIONS.no_op()


def research_anabolic_synthesis(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Anabolic Synthesis), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Anabolic Synthesis), lub pusta akcja

    '''
    ultralisk_caverns = gm.get_my_units_by_type(obs, units.Zerg.UltraliskCavern)
    if len(ultralisk_caverns) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >=150:
        ultralisk_caverns = [unit.tag for unit in ultralisk_caverns]
        return actions.Research_AnabolicSynthesis_quick(
            "now",ultralisk_caverns[0])
    return actions.RAW_FUNCTIONS.no_op()

