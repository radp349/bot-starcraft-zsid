from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm
import numpy as np


def plant_larva(obs, first_base_cords):
    '''Funkcja zwracająca plantowania larwy przez Queen

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Tupla 2 intów z kordynatami pierwszej bazy.
    :type first_base_cords: tuple.
    :returns: Akcja odpowiadająca za plantowania larwy przez Queen lub pusta akcja

    '''
    quens = gm.get_my_units_by_type(obs, units.Zerg.Queen)
    bases = gm.get_my_bases(obs)

    if len(quens) < 1:
        return actions.RAW_FUNCTIONS.no_op()
    if len(bases) < 1:
        return actions.RAW_FUNCTIONS.no_op()

    quens = gm.sort_by_dist(obs, first_base_cords, quens)
    bases = gm.sort_by_dist(obs, first_base_cords, bases)
    actions_list = []

    for index, quuen in enumerate(quens):
        nr = index
        if index >= len(bases):
            nr = len(bases) - 1
        tag = bases[nr].tag
        if not isinstance(tag, np.int64):
            continue

        action = actions.RAW_FUNCTIONS.Effect_InjectLarva_unit("now", quuen.tag, tag)
        actions_list.append(action)

    return actions_list


def use_corrosive_bile(obs):
    ravagers = gm.get_my_units_by_type(obs, units.Zerg.Ravager)
    if len(ravagers) < 1:
        return actions.RAW_FUNCTIONS.no_op()

    enemy_units = gm.get_enemy_combat_units(obs)
    if len(enemy_units) < 1:
        return actions.RAW_FUNCTIONS.no_op()
    action_list = []
    for ravager in ravagers:
        distances = gm.get_distances(obs, enemy_units, (ravager.x, ravager.y))
        if min(distances) < 7:
            target = gm.mean_of_units(enemy_units)
            target[0] = int(target[0])
            target[1] = int(target[1])
            action_list.append(actions.RAW_FUNCTIONS.Effect_CorrosiveBile_pt("now", ravager.tag, target))

    return action_list

