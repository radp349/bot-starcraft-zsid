from pysc2.lib import units, actions, features
import GeneralMethods as gm
import numpy as np
import angle as ang


def build_extractor(obs):
    '''Funkcja zlecająca budowę ekstraktora najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę ekstraktora.

    '''
    neutral_vespene_geysers = []
    neutral_vespene_geysers_names = [units.Neutral.VespeneGeyser, units.Neutral.ProtossVespeneGeyser,
                                     units.Neutral.PurifierVespeneGeyser, units.Neutral.RichVespeneGeyser,
                                     units.Neutral.ShakurasVespeneGeyser]
    for name in neutral_vespene_geysers_names:
        neutral_vespene_geysers.extend(gm.get_units_by_type(obs, name))
    extractors = gm.get_my_units_by_type(obs, units.Zerg.Extractor)
    indexs = []
    index = 0
    if len(extractors) > 0:
        for geyser in neutral_vespene_geysers:
            for extractor in extractors:
                if (geyser.x, geyser.y) == (extractor.x, extractor.y):
                    indexs.append(index)
            index += 1

        substract_index = 0
        for index in indexs:
            neutral_vespene_geysers.pop(index - substract_index)
            substract_index += 1
        neutral_geysers = neutral_vespene_geysers
    else:
        neutral_geysers = neutral_vespene_geysers

    hatcheries = gm.get_my_units_by_type(obs, units.Zerg.Hatchery)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    if len(extractors) < 3 and len(neutral_geysers) > 0 and obs.observation.player.minerals >= 75 and len(
            drones) > 0 and len(hatcheries) > 0:
        distancesGeysers = gm.get_distances(obs, neutral_geysers, (hatcheries[0].x, hatcheries[0].y))
        geyser = neutral_geysers[np.argmin(distancesGeysers)]
        distances = gm.get_distances(obs, drones, (geyser.x, geyser.y))
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Extractor_unit(
            "now", drone.tag, geyser.tag)
    return actions.RAW_FUNCTIONS.no_op()


def build_overlord(obs):
    '''Funkcja transformująca larwę w nadrządcę.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za wytrenowanie nadrządcy.

    '''
    larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
    if (obs.observation.player.minerals >= 100 and
            len(larvas) > 0):
        larvas = [unit.tag for unit in larvas]
        return actions.RAW_FUNCTIONS.Train_Overlord_quick(
            "now", larvas)
    return actions.RAW_FUNCTIONS.no_op()


def build_spawning_pool(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę niecki rozrodczej najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę niecki rozrodczej.

    '''
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    hatcheries = gm.get_my_completed_units_by_type(obs, units.Zerg.Hatchery)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 8
    if base_top_left:
        angle = ang.angle_generator(330, 360, 30)
    else:
        angle = ang.angle_generator(310, 340, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(spawning_pools) == 0 and len(hatcheries) > 0 and len(drones) > 0 and obs.observation.player.minerals >= 200:
        spawning_pool_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, spawning_pool_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_SpawningPool_pt(
            "now", drone.tag, spawning_pool_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_evolution_chamber(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę komory ewolucyjnej najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę komory ewolucyjnej.

    '''
    evolution_chambers = gm.get_my_completed_units_by_type(obs, units.Zerg.EvolutionChamber)
    spawning_pool = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(10, 40, 30)
    else:
        angle = ang.angle_generator(230, 260, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(evolution_chambers) == 0 and len(spawning_pool) > 0 and obs.observation.player.minerals >= 75 and len(
            drones) > 0:
        evolution_chamber_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, evolution_chamber_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_EvolutionChamber_pt(
            "now", drone.tag, evolution_chamber_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_roach_warren(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę kolonii karakanów najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę kolonii karakanów.

    '''
    roach_warrens = gm.get_my_completed_units_by_type(obs, units.Zerg.RoachWarren)
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 6
    if base_top_left:
        angle = ang.angle_generator(60, 90, 30)
    else:
        angle = ang.angle_generator(280, 310, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(roach_warrens) == 0 and len(spawning_pools) > 0 and obs.observation.player.minerals >= 150 and \
            len(drones) > 0:
        roach_warren_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, roach_warren_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_RoachWarren_pt(
            "now", drone.tag, roach_warren_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_baneling_nest(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę gniazda tępicieli najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę gniazda tępicieli.

    '''
    baneling_nests = gm.get_my_completed_units_by_type(obs, units.Zerg.BanelingNest)
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(0, 30, 30)
    else:
        angle = ang.angle_generator(250, 280, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(baneling_nests) == 0 and len(spawning_pools) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 50 and len(drones) > 0:
        baneling_nest_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, baneling_nest_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_BanelingNest_pt(
            "now", drone.tag, baneling_nest_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_spine_crawler(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę kręgowija najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę kręgowija.

    '''
    spine_crawlers = gm.get_my_completed_units_by_type(obs, units.Zerg.SpineCrawler)
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 6
    if base_top_left:
        angle = ang.angle_generator(100, 130, 30)
    else:
        angle = ang.angle_generator(170, 200, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(spine_crawlers) == 0 and len(spawning_pools) > 0 and obs.observation.player.minerals >= 100 and \
            len(drones) > 0:
        spine_crawler_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, spine_crawler_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_SpineCrawler_pt(
            "now", drone.tag, spine_crawler_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_spore_crawler(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę pełzacza zarodnikowego najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę pełzacza zarodnikowego.

    '''
    spore_crawlers = gm.get_my_completed_units_by_type(obs, units.Zerg.SporeCrawler)
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 6
    if base_top_left:
        angle = ang.angle_generator(90, 120, 30)
    else:
        angle = ang.angle_generator(270, 300, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(spore_crawlers) == 0 and len(spawning_pools) > 0 and obs.observation.player.minerals >= 75 and \
            len(drones) > 0:
        spore_crawler_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, spore_crawler_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_SporeCrawler_pt(
            "now", drone.tag, spore_crawler_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_lair(obs, first_base_cords):
    '''Funkcja transformująca wylęgarnię w leże.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za ewolucję wylęgarnii w leże.

    '''
    hatcheries = gm.get_my_completed_units_by_type(obs, units.Zerg.Hatchery)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    spawning_pools = gm.get_my_completed_units_by_type(obs, units.Zerg.SpawningPool)
    if not gm.check_upgrade_in_queue(388, hatcheries):
        return actions.RAW_FUNCTIONS.no_op()
    if len(hives) == 0 and len(lairs) == 0 and len(hatcheries) > 0 and len(spawning_pools) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100:
        hatcheries = gm.sort_by_dist(obs, first_base_cords, hatcheries)
        return actions.RAW_FUNCTIONS.Morph_Lair_quick(
            "now", hatcheries[0].tag, )
    return actions.RAW_FUNCTIONS.no_op()


def build_hydralisk_den(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę nory hydralisków najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę nory hydralisków.

    '''
    hydralisk_dens = gm.get_my_completed_units_by_type(obs, units.Zerg.HydraliskDen)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(320, 350, 30)
    else:
        angle = ang.angle_generator(280, 310, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(lairs) + len(hives) > 0 and len(
            hydralisk_dens) == 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100\
            and len(drones) > 0:
        hydralisk_den_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, hydralisk_den_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_HydraliskDen_pt(
            "now", drone.tag, hydralisk_den_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_infestation_pit(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę jamy infekcji najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę jamy infekcji.

    '''
    infestation_pit = gm.get_my_completed_units_by_type(obs, units.Zerg.InfestationPit)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(30, 60, 30)
    else:
        angle = ang.angle_generator(210, 240, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(lairs) + len(hives) > 0 and len(
            infestation_pit) == 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100 and len(
            drones) > 0:
        infestation_pit_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, infestation_pit_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_InfestationPit_pt(
            "now", drone.tag, infestation_pit_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_spire(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę iglicy najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę iglicy.

    '''
    spires = gm.get_my_completed_units_by_type(obs, units.Zerg.Spire)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 13
    if base_top_left:
        angle = ang.angle_generator(180, 210, 30)
    else:
        angle = ang.angle_generator(30, 60, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(spires) == 0 and len(lairs) + len(hives) > 0 and obs.observation.player.minerals >= 200 and \
            obs.observation.player.vespene >= 200 and len(drones) > 0:
        spire_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, spire_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Spire_pt(
            "now", drone.tag, spire_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_nydus_network(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę sieci nydusowej najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę sieci nydusowej.

    '''
    nydus_networks = gm.get_my_completed_units_by_type(obs, units.Zerg.NydusNetwork)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(90, 120, 30)
    else:
        angle = ang.angle_generator(150, 180, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(lairs) + len(hives) > 0 and len(
            nydus_networks) == 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150 and len(
            drones) > 0:
        nydus_network_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, nydus_network_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_NydusNetwork_pt(
            "now", drone.tag, nydus_network_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_lurker_den(obs, first_base_cords,  base_top_left):
    '''Funkcja zlecająca budowę nory czyhacza najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę nory czyhacza.

    '''
    lurker_dens = gm.get_my_completed_units_by_type(obs, units.Zerg.LurkerDen)
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    r = 11
    if base_top_left:
        angle = ang.angle_generator(120, 150, 30)
    else:
        angle = ang.angle_generator(180, 210, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(lairs) + len(hives) > 0 and len(
            lurker_dens) == 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 150 and len(
            drones) > 0:
        lurker_den_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, drones, lurker_den_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_LurkerDen_pt(
            "now", drone.tag, lurker_den_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_hive(obs):
    '''Funkcja transformująca leże w ul.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za ewolucję leże w ul.

    '''
    lairs = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    infestation_pits = gm.get_my_completed_units_by_type(obs, units.Zerg.InfestationPit)
    if len(hives) == 0 and len(lairs) > 0 and len(infestation_pits) > 0 and obs.observation.player.minerals >= 200 and \
            obs.observation.player.vespene >= 150:
        return actions.RAW_FUNCTIONS.Morph_Hive_quick(
            "now", lairs[0].tag, )
    return actions.RAW_FUNCTIONS.no_op()


def build_ultralisk_cavern(obs, first_base_cords, base_top_left):
    '''Funkcja zlecająca budowę pieczary ultralisków najbliższemu trutniowi.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę pieczary ultralisków.

    '''
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    ultralisk_caverns = gm.get_my_completed_units_by_type(obs, units.Zerg.UltraliskCavern)
    drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
    bases = gm.get_my_bases(obs)
    if len(bases) < 2:
        return actions.RAW_FUNCTIONS.no_op()
    bases = gm.sort_by_dist(obs, first_base_cords, bases)
    r = 13
    if base_top_left:
        angle = ang.angle_generator(90, 135, 30)
    else:
        angle = ang.angle_generator(240, 270, 30)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(ultralisk_caverns) == 0 and len(
            hives) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 200 and len(
            drones) > 0:
        ultralisk_cavern_xy = (bases[1].x + x, bases[1].x + y)
        distances = gm.get_distances(obs, drones, ultralisk_cavern_xy)
        drone = drones[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_UltraliskCavern_pt(
            "now", drone.tag, ultralisk_cavern_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_greater_spire(obs):
    '''Funkcja transformująca iglicę w większą iglicę.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za ulepszenie iglicy.

    '''
    spires = gm.get_my_completed_units_by_type(obs, units.Zerg.Spire)
    hives = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
    if len(spires) > 0 and len(hives) > 0 and obs.observation.player.minerals >= 100 and \
            obs.observation.player.vespene >= 150:
        return actions.RAW_FUNCTIONS.Morph_GreaterSpire_quick(
            "now", spires[0].tag, )
    return actions.RAW_FUNCTIONS.no_op()
