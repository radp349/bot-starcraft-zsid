import numpy as np
from sklearn.cluster import DBSCAN
from pysc2.lib import units, features


def get_enemy_bases(obs):
    '''
    Funkcja zwracająca wszystkie istniejące budynki baz przeciwnika.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Lista istniejących budynków baz przeciwnika
    '''
    enemy_bases = [building for building in obs.observation.raw_units if
                   building.alliance == features.PlayerRelative.ENEMY and (
                       (building.unit_type == bases).any()).any()]
    return enemy_bases


def get_units_by_type(obs, unit_type):
    '''
    Funkcja zwracająca wszystkie jednostki danego typu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param unit_type: typ jednostki
    :type unit_type: Class attribute
    :returns: List: Lista wszystkich jednostek danego typu
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type]


def get_distances(obs, units, xy):
    '''
    Funkcja zwracająca odległości jednostek od punktu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param units: Lista jednostek
    :type units: list
    :param xy: punkt od którego sprawdzana jest odległość
    :type xy: tuple
    :returns: (numpy_array): tablica odległości od punktu
    '''
    units_xy = [(unit.x, unit.y) for unit in units]
    return np.linalg.norm(np.array(units_xy) - np.array(xy), axis=1)


def get_distances_from_points(obs, points, xy):
    '''
    Funkcja zwracająca odległości punktów od punktu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param points: Lista punktów
    :type points: list
    :param xy: punkt od którego sprawdzana jest odległość
    :type xy: tuple
    :returns: (numpy_array): tablica odległości od punktu
    '''
    return np.linalg.norm(np.array(points) - np.array(xy), axis=1)


def get_my_units_by_type(obs, unit_type):
    '''
    Funkcja zwracająca wszystkie jednostki gracza danego typu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param unit_type: typ jednostki
    :type unit_type: Class attribute
    :returns: (List): Lista wszystkich jednostek gracza danego typu
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.alliance == features.PlayerRelative.SELF]


def get_enemy_buildings(obs):
    '''
    Funkcja zwracająca wszystkie budynki przeciwnika

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns:(List): Lista wszystkich budynków przeciwnika
    '''
    enemy_buildings = [building for building in obs.observation.raw_units if
                       building.alliance == features.PlayerRelative.ENEMY and (
                               (building.unit_type == terran_buildings).any() or (
                                building.unit_type == zerg_buildings).any() or (
                                       building.unit_type == protoss_buildings).any())]
    return enemy_buildings


def get_my_buildings(obs):
    '''
    Funkcja zwracająca wszystkie budynki gracza

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns:(List): Lista wszystkich budynków gracza
    '''
    ally_buildings = [building for building in obs.observation.raw_units if
                      building.alliance == features.PlayerRelative.SELF and (
                              (building.unit_type == terran_buildings).any() or (
                                building.unit_type == zerg_buildings).any() or (
                                      building.unit_type == protoss_buildings).any())]
    return ally_buildings


def get_my_bases(obs):
    '''
    Funkcja zwracająca wszystkie istniejące budynki baz gracza.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns:(List): Lista istniejących budynków baz gracza
    '''
    ally_buildings = [building for building in obs.observation.raw_units if
                      building.alliance == features.PlayerRelative.SELF and (
                          (building.unit_type == bases).any()).any()]
    return ally_buildings


def get_enemy_workers(obs):
    '''
    Funkcja zwracająca wszystkich robotników przeciwnika

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns:(List): Lista wszystkich robotników przeciwnika
    '''
    return [worker for worker in obs.observation.raw_units if
            worker.alliance == features.PlayerRelative.ENEMY and (worker.unit_type == workers).any()]


def get_enemy_combat_units(obs):
    '''
    Funkcja zwracająca wszystkie jednostki bojowe przeciwnika

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns:(List): Lista wszystkich jednostek bojowych przeciwnika
    '''
    enemy_combat_units = [combat_unit for combat_unit in obs.observation.raw_units if
                          combat_unit.alliance == features.PlayerRelative.ENEMY and (
                                  (combat_unit.unit_type == terran_combat_units).any() or (
                                    combat_unit.unit_type == zerg_combat_units).any() or (
                                          combat_unit.unit_type == protoss_combat_units).any())]
    return enemy_combat_units


def get_my_combat_units(obs):
    '''
    Funkcja zwracająca wszystkie jednostki bojowe gracza

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns:(List): Lista wszystkich jednostek bojowych gracza
    '''
    my_combat_units = [combat_unit for combat_unit in obs.observation.raw_units if
                       combat_unit.alliance == features.PlayerRelative.SELF and (
                               (combat_unit.unit_type == terran_combat_units).any() or (
                                combat_unit.unit_type == zerg_combat_units).any() or (
                                       combat_unit.unit_type == protoss_combat_units).any())]
    return my_combat_units


def get_enemy_units_by_type(obs, unit_type):
    '''
    Funkcja zwracająca wszystkie jednostki przeciwnika danego typu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param unit_type: typ jednostki
    :type unit_type: Class attribute
    :returns:(List): Lista wszystkich jednostek przeciwnika danego typu
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.alliance == features.PlayerRelative.ENEMY]


def get_my_completed_units_by_type(obs, unit_type):
    '''
    Funkcja zwracająca wszystkie wyprodukowane jednostki gracza danego typu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param unit_type: typ jednostki
    :type unit_type: Class attribute
    :returns:(List): Lista wszystkich wyprodukowancyh jednostek gracza danego typu
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.build_progress == 100
            and unit.alliance == features.PlayerRelative.SELF]


def get_enemy_completed_units_by_type(obs, unit_type):
    '''
    Funkcja zwracająca wszystkie wyprodukowane jednostki przeciwnika danego typu

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param unit_type: typ jednostki
    :type unit_type: Class attribute
    :returns:(List): Lista wszystkich wyprodukowancyh jednostek przeciwnika danego typu
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type == unit_type
            and unit.build_progress == 100
            and unit.alliance == features.PlayerRelative.ENEMY]


def get_minerals(obs):
    '''
    Funkcja zwracająca wszystkie pola minerałów

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns:(List): Lista wszystkich pól minerałów
    '''
    return [unit for unit in obs.observation.raw_units
            if unit.unit_type in [
                units.Neutral.BattleStationMineralField,
                units.Neutral.BattleStationMineralField750,
                units.Neutral.LabMineralField,
                units.Neutral.LabMineralField750,
                units.Neutral.MineralField,
                units.Neutral.MineralField750,
                units.Neutral.PurifierMineralField,
                units.Neutral.PurifierMineralField750,
                units.Neutral.PurifierRichMineralField,
                units.Neutral.PurifierRichMineralField750,
                units.Neutral.RichMineralField,
                units.Neutral.RichMineralField750
            ]]


def cluster(units, ep=10, samples=2):
    '''
    Funkcja zwracająca pogrupowane jednostki funkcją DBSCAN

    :param units: lista jednostek
    :type units: list
    :param ep: promień przeszukiwania
    :type ep: float
    :param samples: minimalna ilość jednostek by stworzyć grupę
    :type samples: int
    :returns:(List): Lista list zgrupowanych jednostek
    '''
    units_xy = np.array([(unit.x, unit.y) for unit in units])
    sorted_units = []
    if units_xy.any():
        model = DBSCAN(eps=ep, min_samples=samples)
        cl = np.array(model.fit_predict(units_xy))
        cl_unique = np.unique(cl)

        for i in cl_unique:
            un = []
            for j in range(len(units)):
                if cl[j] == i:
                    un.append(units[j])
            sorted_units.append(un)
    return sorted_units


def mean_of_units(units):
    '''
    Funkcja zwracająca środek jednostek na mapie

    :param units: lista jednostek
    :type units: list
    :returns: Środek jednostek na mapie
    '''
    units_xy = np.array([(unit.x, unit.y) for unit in units])
    return np.mean(units_xy, axis=0)


def mean_div_of_units(units):
    '''
    Funkcja zwracająca średnią odległość od środka jednostek
    :param units: lista jednostek
    :type units: list

    :returns: Średnia odległość od środka jednostek
    '''
    units_xy = np.array([(unit.x, unit.y) for unit in units])
    return np.std(units_xy)


def get_my_upgrade(obs, upgrade_type):
    '''
    Funkcja sprawdzająca czy dane ulepszenie zostało opracowane

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param upgrade_type: typ ulepszenia
    :type upgrade_type: Class attribute
    :returns:(List): Lista z numerem podanego ulepszenia z tablicy ulepszeń (jeśli zostało opracowane. W przeciwnym wypadku
        zwraca pustą tablicę)
    '''
    return [upg for upg in obs.observation.upgrades
            if upg == upgrade_type]


def check_upgrade_in_queue(upgrade_type, buildings):
    '''
    Funkcja sprawdzająca czy dane ulepszenie może być dodane do kolejki opracowania w zależności od tego czy znajduje się lub nie w kolejce
    któregoś z podanego typu budynku
    :param upgrade_type: typ ulepszenia
    :type upgrade_type: int
    :param buildings: typ budynku
    :type buildings: list

    :returns:(List): False jeśli dane ulepszenie jest w kolejce lub True jeśli dane ulepszenie nie znajduje się w żadnej kolejce
    '''
    for i in range(len(buildings)):
        if buildings[i].order_id_0 == upgrade_type or \
                buildings[i].order_id_1 == upgrade_type or \
                buildings[i].order_id_2 == upgrade_type or \
                buildings[i].order_id_3 == upgrade_type:
            return False
    return True


def check_number_of_upgrade_in_queue(upgrade_type, buildings):
    '''
    Funkcja sprawdzająca czy dane ulepszenie może być dodane do kolejki opracowania w zależności od tego czy znajduje się lub nie w kolejce
    któregoś z podanego typu budynku
    :param upgrade_type: typ ulepszenia
    :type upgrade_type: int
    :param buildings: typ budynku
    :type buildings: list

    :returns:(List): False jeśli dane ulepszenie jest w kolejce lub True jeśli dane ulepszenie nie znajduje się w żadnej kolejce
    '''
    j = 0
    for i in range(len(buildings)):
        if buildings[i].order_id_0 == upgrade_type:
            j += 1
        if buildings[i].order_id_1 == upgrade_type:
            j += 1
        if buildings[i].order_id_2 == upgrade_type:
            j += 1
        if buildings[i].order_id_3 == upgrade_type:
            j += 1
    return j


def free_supply(obs):
    '''
    Funkcja zwracająca ilość wolnego zaopatrzenia

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns: Ilość wolnego zaopatrzenia
    '''
    return obs.observation.player.food_cap - obs.observation.player.food_used


def sort_by_dist(obs, first_base_cords, units_to_sort):
    '''
    Funkcja sortująca jednostki względem odległości od bazy gracza

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param units_to_sort: lista jednostek do posortowania
    :type units_to_sort: list
    :returns:(List): Lista posortowanych jednostek
    '''
    units_sorted = []
    tmp = get_distances(obs, units_to_sort, first_base_cords)
    if len(tmp) > 0:
        dist = list(tmp)
        for i in range(len(units_to_sort)):
            tmp = dist.index(min(dist))
            units_sorted.append(units_to_sort[tmp])
            units_to_sort.pop(tmp)
            dist.pop(tmp)
    return units_sorted


def connect_actions(old_list, actions):
    '''
    Funkcja łącząca dwie tablice akcji
    :param old_list: Pierwsza lista rozkazów
    :type old_list: list
    :param actions: Druga lista rozkazów
    :type actions: list

    :returns:(List): Lista połączonych rozkazów
    '''
    if isinstance(actions, list):
        for action in actions:
            old_list.append(action)
    else:
        old_list.append(actions)
    return old_list


def connect_actions_deeper(old_list, actions):
    '''
    Funkcja wypakowująca skomplikowane/złożone tablice akcji do jednej tablicy
    :param old_list: Pierwsza lista rozkazów
    :type old_list: list
    :param actions: Druga lista rozkazów
    :type actions: list

    :returns:(List): Lista połączonych rozkazów
    '''
    if isinstance(actions, list):
        for i in range(len(actions)):
            old_list = connect_actions_deeper(old_list, actions[i])
    else:
        old_list = connect_actions(old_list, actions)
    return old_list


workers = [units.Terran.SCV, units.Zerg.Drone, units.Protoss.Probe]
terran_buildings = [units.Terran.Armory, units.Terran.Barracks, units.Terran.BarracksFlying,
                    units.Terran.BarracksReactor, units.Terran.BarracksTechLab,
                    units.Terran.CommandCenter, units.Terran.Refinery, units.Terran.Bunker,
                    units.Terran.CommandCenterFlying, units.Terran.EngineeringBay,
                    units.Terran.Factory, units.Terran.FactoryFlying, units.Terran.FactoryReactor,
                    units.Terran.FactoryTechLab, units.Terran.FusionCore,
                    units.Terran.GhostAcademy, units.Terran.MissileTurret, units.Terran.OrbitalCommand,
                    units.Terran.OrbitalCommandFlying,
                    units.Terran.PlanetaryFortress, units.Terran.Reactor, units.Terran.Refinery,
                    units.Terran.RefineryRich, units.Terran.SensorTower, units.Terran.Starport,
                    units.Terran.StarportFlying, units.Terran.StarportReactor, units.Terran.StarportTechLab,
                    units.Terran.SupplyDepot, units.Terran.SupplyDepotLowered, units.Terran.TechLab]

zerg_buildings = [units.Zerg.Hive, units.Zerg.Hatchery, units.Zerg.HydraliskDen, units.Zerg.Lair, units.Zerg.Spire,
                  units.Zerg.SpawningPool, units.Zerg.SpineCrawler,
                  units.Zerg.SporeCrawler, units.Zerg.SpineCrawlerUprooted, units.Zerg.SporeCrawlerUprooted,
                  units.Zerg.GreaterSpire, units.Zerg.RoachWarren, units.Zerg.EvolutionChamber,
                  units.Zerg.Extractor,
                  units.Zerg.ExtractorRich, units.Zerg.BanelingNest, units.Zerg.LurkerDen,
                  units.Zerg.UltraliskCavern]

protoss_buildings = [units.Protoss.Nexus, units.Protoss.Pylon, units.Protoss.Assimilator,
                     units.Protoss.AssimilatorRich, units.Protoss.Gateway, units.Protoss.WarpGate,
                     units.Protoss.Stargate, units.Protoss.CyberneticsCore, units.Protoss.Forge,
                     units.Protoss.PhotonCannon, units.Protoss.ShieldBattery, units.Protoss.TwilightCouncil,
                     units.Protoss.RoboticsBay, units.Protoss.RoboticsFacility, units.Protoss.TemplarArchive,
                     units.Protoss.DarkShrine, units.Protoss.FleetBeacon]

terran_combat_units = [units.Terran.Marine, units.Terran.WidowMine, units.Terran.Marauder, units.Terran.Reaper,
                       units.Terran.Hellion,
                       units.Terran.Hellbat, units.Terran.SiegeTank, units.Terran.SiegeTankSieged,
                       units.Terran.Cyclone, units.Terran.Ghost, units.Terran.Thor,
                       units.Terran.AutoTurret, units.Terran.VikingAssault, units.Terran.VikingFighter,
                       units.Terran.Medivac, units.Terran.Raven, units.Terran.Liberator,
                       units.Terran.Banshee, units.Terran.Battlecruiser, units.Terran.LiberatorAG]

zerg_combat_units = [units.Zerg.Zergling, units.Zerg.Baneling, units.Zerg.BanelingCocoon,
                     units.Zerg.Mutalisk, units.Zerg.Roach,
                     units.Zerg.Ravager, units.Zerg.Infestor, units.Zerg.BroodLord, units.Zerg.SwarmHost,
                     units.Zerg.Locust,
                     units.Zerg.LocustFlying, units.Zerg.InfestedTerran, units.Zerg.Viper, units.Zerg.Ultralisk,
                     units.Zerg.Hydralisk]

protoss_combat_units = [units.Protoss.Zealot, units.Protoss.Stalker, units.Protoss.Sentry, units.Protoss.Adept,
                        units.Protoss.HighTemplar, units.Protoss.DarkTemplar, units.Protoss.Immortal,
                        units.Protoss.Colossus, units.Protoss.Disruptor, units.Protoss.DisruptorPhased,
                        units.Protoss.Archon, #units.Protoss.Observer,
                        units.Protoss.WarpPrism,
                        units.Protoss.Phoenix, units.Protoss.VoidRay, units.Protoss.Oracle, units.Protoss.Carrier,
                        units.Protoss.Tempest, units.Protoss.Mothership, units.Protoss.MothershipCore]

bases = [units.Protoss.Nexus, units.Terran.CommandCenter, units.Terran.PlanetaryFortress, units.Terran.OrbitalCommand,
         units.Zerg.Lair, units.Zerg.Hive, units.Zerg.Hatchery]
