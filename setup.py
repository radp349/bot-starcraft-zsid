# -*- coding: utf-8 -*-
import shutil, os, re
import zipfile
import sys
import threading, subprocess
import urllib.request
from PySide2.QtCore import Signal, QObject
from PySide2.QtWidgets import QMainWindow, QLabel, QFileDialog, QMessageBox, QApplication, QPushButton, QRadioButton
from PySide2.QtGui import QFont


def run_cmds(race, enemy_race):
    os.system(
        'start /wait cmd /c "cd {} & pip install pysc2 & pip install sklearn & pip install numpy & pip install joblib & pip install matplotlib"'.format(
            '\\'.join(os.path.join(os.path.abspath(os.getcwd())).split('\\')[:3])))
    dir_path = os.path.abspath(os.getcwd())
    proc = subprocess.Popen('where python', stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    tmp = re.split(r"\\r", str(proc.stdout.read()))[0]
    pysc2Path = '\\'.join(tmp[2:].split('\\')[:-1]) + '\Lib\site-packages\pysc2\maps'
    if os.path.exists(pysc2Path + '\\melee.py'):
        os.remove(pysc2Path + '\\melee.py')
    shutil.copyfile(dir_path + '\\melee.py', pysc2Path + '\\melee.py')
    os.system(
        'start cmd /k "cd {} & python -m human_vs_bot --human --map WintersGateLE --user_race {}"'.format(dir_path,
                                                                                                          race))
    if enemy_race == "terran":
        module = 'TerranBotRunner'
    elif enemy_race == "zerg":
        module = 'ZergBotRunner'
    else:
        module = 'ProtossBotRunner'
    os.system(
        'start cmd /k "cd {} & python -m human_vs_bot --agent main.{} --agent_race {} --action_space RAW --use_raw_units"'.format(
            dir_path, module, enemy_race))


class Communicate(QObject):
    signal = Signal()

    def __init__(self, parent):
        super(Communicate, self).__init__()
        self.parent = parent
        self.signal.connect(self.execute)

    def emitting(self):
        self.signal.emit()

    def execute(self):
        self.parent.choose_race()


class PathWindow(QMainWindow):
    def __init__(self):
        super(PathWindow, self).__init__()
        self.setWindowTitle("Konfigurator Gry")
        self.setFixedSize(300, 70)
        self.font = QFont()
        self.font.setBold(True)
        self.waitLabel = QLabel(self)
        self.waitLabel.setText("Pobieranie i konfigurowanie niezbędnych plików...")
        self.waitLabel.setGeometry(10, 0, 300, 60)
        self.waitLabel.setVisible(False)
        self.waitLabel.setFont(self.font)
        self.infoLabel = QLabel(self)
        self.infoLabel.setText("Wyskaż ścieżkę folderu z aplikacją StarCraft II.")
        self.infoLabel.setGeometry(20, 0, 300, 30)
        self.infoLabel.setFont(self.font)
        self.infoLabel.setVisible(True)
        self.start1Label = QLabel(self)
        self.start1Label.setText("Wybierz twoją rasę:")
        self.start1Label.setGeometry(40, 0, 300, 18)
        self.start1Label.setFont(self.font)
        self.start1Label.setVisible(False)
        self.start2Label = QLabel(self)
        self.start2Label.setText("Wybierz rasę bota:")
        self.start2Label.setGeometry(40, 0, 300, 18)
        self.start2Label.setFont(self.font)
        self.start2Label.setVisible(False)
        self.pathButton = QPushButton(self)
        self.pathButton.setGeometry(110, 30, 80, 30)
        self.pathButton.clicked.connect(self.choose_path)
        self.pathButton.setText('Przeglądaj...')

        self.okButton = QPushButton(self)
        self.okButton.setGeometry(110, 45, 80, 20)
        self.okButton.clicked.connect(self.clossing)
        self.okButton.setText('Start')
        self.okButton.setVisible(False)

        self.nextButton = QPushButton(self)
        self.nextButton.setGeometry(110, 45, 80, 20)
        self.nextButton.clicked.connect(self.next)
        self.nextButton.setText('Dalej')
        self.nextButton.setVisible(False)

        self.terranbutton = QRadioButton(self)
        self.terranbutton.setChecked(True)
        self.terranbutton.setText('Terran')
        self.terranbutton.setVisible(False)
        self.terranbutton.setFont(self.font)
        self.terranbutton.toggled.connect(self.change_race)
        self.terranbutton.move(40, 18)
        self.protosbutton = QRadioButton(self)
        self.protosbutton.setChecked(False)
        self.protosbutton.setText('Protoss')
        self.protosbutton.setVisible(False)
        self.protosbutton.setFont(self.font)
        self.protosbutton.toggled.connect(self.change_race)
        self.protosbutton.move(120, 18)
        self.zergbutton = QRadioButton(self)
        self.zergbutton.setChecked(False)
        self.zergbutton.setText('Zerg')
        self.zergbutton.setFont(self.font)
        self.zergbutton.setVisible(False)
        self.zergbutton.toggled.connect(self.change_race)
        self.zergbutton.move(200, 18)
        self.sc2Path = None
        self.user_race = 'terran'
        self.enemy_race = 'terran'
        self.user_choice = True
        self.waitSignal = Communicate(self)
        self.show()

    def change_race(self):
        if self.terranbutton.isChecked():
            if self.user_choice:
                self.user_race = 'terran'
            else:
                self.enemy_race = 'terran'
        elif self.zergbutton.isChecked():
            if self.user_choice:
                self.user_race = 'zerg'
            else:
                self.enemy_race = 'zerg'
        elif self.protosbutton.isChecked():
            if self.user_choice:
                self.user_race = 'protoss'
            else:
                self.enemy_race = 'protoss'

    def next(self):
        self.user_choice = False
        self.choose_race()

    def clossing(self):
        USER = os.getenv('PATH')
        USER = USER + self.sc2Path + "\Support" + ";" + self.sc2Path + "\Support64;"
        os.environ['PATH'] = USER
        self.close()
        run_cmds(self.user_race, self.enemy_race)

    def choose_race(self):
        self.pathButton.setVisible(False)
        if self.user_choice:
            self.nextButton.setVisible(True)
            self.okButton.setVisible(False)
            self.start1Label.setVisible(True)
            self.start2Label.setVisible(False)
        else:
            self.nextButton.setVisible(False)
            self.okButton.setVisible(True)
            self.start1Label.setVisible(False)
            self.start2Label.setVisible(True)
        self.infoLabel.setVisible(False)
        self.waitLabel.setVisible(False)
        self.terranbutton.setVisible(True)
        self.zergbutton.setVisible(True)
        self.protosbutton.setVisible(True)

    def download_map(self):
        url = 'http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2019Season3.zip'
        urllib.request.urlretrieve(url, os.path.join(self.sc2Path, 'Maps\\Melee\\maps.zip'))
        zip = zipfile.ZipFile(os.path.join(self.sc2Path, 'Maps\\Melee\\maps.zip'), 'r')
        zip.setpassword(b"iagreetotheeula")
        zip.extract('Ladder2019Season3/WintersGateLE.SC2Map', os.path.join(self.sc2Path, 'Maps\\Melee'))
        zip.close()
        shutil.copyfile(os.path.join(self.sc2Path, 'Maps\\Melee\\Ladder2019Season3\\WintersGateLE.SC2Map'),
                        os.path.join(self.sc2Path, 'Maps\\Melee\\WintersGateLE.SC2Map'))
        os.remove(os.path.join(self.sc2Path, 'Maps\\Melee\\maps.zip'))
        os.remove(os.path.join(self.sc2Path, 'Maps\\Melee\\Ladder2019Season3\\WintersGateLE.SC2Map'))
        os.rmdir(os.path.join(self.sc2Path, 'Maps\\Melee\\Ladder2019Season3'))
        self.waitSignal.emitting()

    def choose_path(self):
        self.sc2Path = QFileDialog.getExistingDirectory(self, "Choose SC2 Path", "C:\\", QFileDialog.ShowDirsOnly
                                                        | QFileDialog.DontResolveSymlinks)
        if os.path.exists(os.path.join(self.sc2Path, 'StarCraft II.exe')):
            if os.path.exists(os.path.join(self.sc2Path, 'Maps')):
                if not os.path.exists(os.path.join(self.sc2Path, 'Maps\\Melee')):
                    os.mkdir(os.path.join(self.sc2Path, 'Maps\\Melee'))
                if not os.path.exists(os.path.join(self.sc2Path, 'Maps\\Melee\\WintersGateLE.SC2Map')):
                    self.waitLabel.setVisible(True)
                    self.pathButton.setVisible(False)
                    self.infoLabel.setVisible(False)
                    threading.Thread(target=self.download_map).start()
                else:
                    self.choose_race()

            else:
                os.mkdir(os.path.join(self.sc2Path, 'Maps'))
                os.mkdir(os.path.join(self.sc2Path, 'Maps\\Melee'))
                self.waitLabel.setVisible(True)
                self.pathButton.setVisible(False)
                self.infoLabel.setVisible(False)
                threading.Thread(target=self.download_map).start()

        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("Nieprawidłowa ścieżka! Upewnij się, że plik 'StarCraft II.exe' tam się znajduje.")
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    pathWindow = PathWindow()
    sys.exit(app.exec_())
