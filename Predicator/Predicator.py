import os
import numpy as np
from joblib import load
import matplotlib.pyplot as plt
from GetValuesFromGame import GetValues


class Predicator:
    '''
    Klasa obsługująca obliczanie szans zwycięstwa i wyświetlanie wykresu predykcji.

    :param race: Rasa gracza(tylko pierwsza litera).
    :type race: string.
    :param enemy_race: Rasa przeciwnika(tylko pierwsza litera).
    :type enemy_race: string.
    :param reverse: Zamian ras graczy.
    :type reverse: bool.
    :param step_mul: Ilość pomijanych klatek gry.
    :type step_mul: int.
    :param data: Wartości predykowanych szans na wygraną.
    :type data: list.
    :param interval: Okres aktualizowania wykresu predykcji.
    :type interval: int.
    :param prev_time: Poprzednio zapisany czas gry.
    :type prev_time: int.
    :param kmeans_predicator: Obiekt wyuczonego wcześniej klasyfikatora.
    :type kmeans_predicator: KMeansClassifier.
    :param time: Aktualny czas gry.
    :type time: int.
    :param fig: Wykres.
    :type fig: figure.
    :param ax: Osie wykresu.
    :type ax: axes.
    '''

    def __init__(self, env, step_mul, reverse):
        plt.ion()
        self.step_mul = step_mul
        self.interval = 5
        self.prev_time = 0
        self.reverse = reverse
        self.data = []
        self.kmeans_predicator = self.__load_kmeans(env)
        self.time = 0
        self.fig, self.ax = plt.subplots()
        self.fig.suptitle("Win Prediction", fontsize=23)

    def __load_kmeans(self, sc2_env):
        """
        Funkcja wczytująca obiekt kmeans z pliku .joblib.

        :return: (KNeighborsClassifier): Obiekt z wcześniej wyuczonych danych.
        """
        self.race, self.enemy_race = self.__get_races(str(sc2_env.game_info[0].player_info))
        kmeans_path = "bot_vs_bot/"
        if self.reverse:
            self.race, self.enemy_race = self.enemy_race, self.race
            kmeans_path = "human_vs_bot/"

        return load(
            str(os.path.dirname(os.path.abspath("Predicator.py"))) + "/Predicator/trained_kmeans/" + kmeans_path +
            list(self.race)[0] + 'vs' + list(self.enemy_race)[
                0] + '.joblib')

    def __get_races(self, players_info):
        """
        Funkcja wczytująca rasy graczy.

        :param race_str: Rasa gracza.
        :type race_str: string.
        :param enemy_race_str: Rasa przeciwnika.
        :type enemy_race_str: string.
        :returns: (list): Rasy graczy
        """
        race_str, enemy_race_str = players_info.split('\n')[3].split(':')[1], players_info.split('\n')[6].split(':')[1]
        return "".join(race_str.split()), "".join(enemy_race_str.split())

    def __prediction_plot(self, current_predicit):
        """
        Funkcja aktualizująca wykres predykcji.
        """
        self.data.append(current_predicit)
        self.ax.clear()
        self.ax.set_ylim(0, 100)
        self.ax.set_xlim(0, (len(self.data) - 1) * self.interval)
        self.ax.set_xlabel("Game Time[s]")
        self.ax.set_ylabel("Chance to win[%]")
        self.ax.plot(np.linspace(0, (len(self.data) - 1) * self.interval, len(self.data)), self.data)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def predict(self, obs, enemy_obs=None):
        """
        Funkcja wczytująca rasy graczy.

        :param enemy_obs: Obecny stan gry przeciwnika.
        :type enemy_obs: timestep.
        :param obs: Obecny stan gry.
        :type obs: timestep.
        :param frame: Sparsowane wartości stanu gry.
        :type frame: list.
        :param enemy_frame: Sparsowane wartości stanu gry przeciwnika.
        :type enemy_frame: list.
        :param prediction: Szanse na wygraną gracza(%).
        :type prediction: float.
        """

        if self.time - self.prev_time > self.interval:
            frame = GetValues(obs, self.race)
            frame.insert(0, self.time)
            if enemy_obs is not None:
                enemy_frame = GetValues(enemy_obs, self.enemy_race)
                frame += enemy_frame
            prediction = 100 * (
                        1 - self.kmeans_predicator.predict_proba(np.array(frame).reshape((1, len(frame))))[0, 1])
            self.__prediction_plot(prediction)
            self.prev_time = self.time
        self.time += 1 / 22.4 * self.step_mul
