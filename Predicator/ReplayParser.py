import csv
import os
import pathlib
from typing import Any

import tqdm.auto as tqdm
import zephyrus_sc2_parser
from zephyrus_sc2_parser.parser import Replay

GroundUnits = [
    # Protoss Ground Units
    'Probe', 'Zealot', 'Stalker', 'Sentry', 'Adept', 'HighTemplar', 'DarkTemplar', 'Immortal', 'Colossus', 'Disruptor',
    'Archon',
    # Zerg Ground Units
    'Egg', 'Larva', 'Cocoon', 'Drone', 'Queen', 'Zergling', 'Baneling', 'Roach', 'Ravager', 'Hydralisk', 'Lurker',
    'LurkerMP', 'LurkerMPEgg', 'LurkerMPBurrowed',
    'Infestor', 'BanelingCocoon', 'OverlordCocoon', 'RavagerCocoon', 'BroodLordCocoon', 'TransportOverlordCocoon',
    'SwarmHost', 'SwarmHostMP', 'Ultralisk', 'Locust', 'LocustMP', 'LocustMPPrecursor', 'LocustMPFlying', 'Broodling',
    'BroodlingEscort', 'Changeling', 'InfestedTerran', 'NydusWorm',
    # Terran Ground Units
    'SCV', 'MULE', 'Marine',
    'Marauder', 'Reaper', 'Ghost', 'Hellion', 'Hellbat', 'HellionTank', 'SiegeTank', 'SiegeTankSieged', 'Cyclone',
    'WidowMine', 'WidowMineBurrowed',
    'Thor', 'ThorAP', 'Auto-Turret']

AirUnits = [
    # Protoss Air Units
    'Observer', 'ObserverSiegeMode', 'WarpPrism', 'WarpPrismPhasing', 'Phoenix', 'VoidRay', 'Oracle', 'Carrier',
    'Tempest', 'MothershipCore',
    'Mothership',
    'Interceptor',
    # Zerg Air Units 
    'Overlord', 'OverlordTransport', 'Overseer', 'OverseerSiegeMode', 'Mutalisk', 'Corruptor', 'BroodLord', 'Viper',
    # Terran Air Units
    'Viking', 'Medivac', 'Liberator', 'LiberatorAG', 'Raven', 'Banshee', 'Battlecruiser', 'VikingFighter',
    'VikingAssault']

REPLAY_DIRECTORY = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/replays/PvsP"
TT_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/TvsT.csv"
ZZ_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/ZvsZ.csv"
PP_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/PvsP.csv"
TP_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/TvsP.csv"
TZ_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/ZvsT.csv"
PZ_OUTPUT_CSV = os.path.dirname(os.path.abspath("ReplayParser.py")) + "/parsed_replays/PvsZ.csv"


def get_army_value(frame) -> list[int, int]:
    """
        Funkcja obliczają wartość jednostek naziemnych i powietrznych na podstawie aktualnego stanu gry.

        :param frame: Stan gry.
        :type frame: dict.
        :param ground: Wartość jednostek naziemnych.
        :type ground: int.
        :param air: Wartość jednostek powietrznych.
        :type air: int.
        :returns: (list): Wartość jednostek naziemnych i powietrznych.
    """
    ground = 0
    air = 0
    for key in frame.keys():
        if key in GroundUnits:
            ground += (frame[key]['mineral_cost'] + frame[key]['gas_cost']) * int(frame[key]['live'])
        elif key in AirUnits:
            air += (frame[key]['mineral_cost'] + frame[key]['gas_cost']) * int(frame[key]['live'])
        else:
            print("unrecognized unit: " + key)

    return ground, air


def choose_player(players) -> list[int, str]:
    """
        Funkcja wczytująca rasy graczy.

        :param players: Dane graczy.
        :type players: list.
        :param races: Rasy graczy.
        :type races: set.
        :returns: (list): Id gracza oraz ścieżka zapisu pliku wynikowego .csv.
    """
    races = {players[1].race, players[2].race}
    if races == {'Zerg', 'Protoss'}:
        return players[1].player_id if players[1].race == 'Protoss' else players[2].player_id, PZ_OUTPUT_CSV
    elif races == {'Zerg', 'Terran'}:
        return players[1].player_id if players[1].race == 'Zerg' else players[2].player_id, TZ_OUTPUT_CSV
    elif races == {'Protoss', 'Terran'}:
        return players[1].player_id if players[1].race == 'Terran' else players[2].player_id, TP_OUTPUT_CSV
    elif races == {'Protoss', 'Protoss'}:
        return players[1].player_id, PP_OUTPUT_CSV
    elif races == {'Terran', 'Terran'}:
        return players[1].player_id, TT_OUTPUT_CSV
    elif races == {'Zerg', 'Zerg'}:
        return players[1].player_id, ZZ_OUTPUT_CSV


def get_buildings_value(frame) -> int:
    """
        Funkcja obliczają wartość budnyków na podstawie aktualnego stanu gry.

        :param frame: Stan gry.
        :type frame: dict.
        :param buildings: Wartość budynków.
        :type buildings: int.
        :returns: (int): Wartość budynków.
    """
    buildings = 0
    for key in frame.keys():
        buildings += (frame[key]['mineral_cost'] + frame[key]['gas_cost']) * int(frame[key]['live'])

    return buildings


def save_to_csv(dataFrames, savePath):
    """
        Funkcja zapisująca sparsowany replay do pliku .csv we wskazanej ścieżce.

        :param dataFrames: Sparsowany replay.
        :type dataFrames: list.
        :param savePath: Ścieżka zapisu pliku .csv.
        :type savePath: string.
        :param headers: Nagłowki pliku .csv.
        :type headers: list.
        :param dict_writer: Obiekt parsujacy dict do pliku .csv.
        :type dict_writer: DictWriter.
    """
    headers = dataFrames[0].keys()
    if not os.path.isfile(savePath):
        with open(savePath, 'a+', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, fieldnames=headers, delimiter=',')
            dict_writer.writeheader()
    with open(savePath, 'a+', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, fieldnames=headers, delimiter=',')
        dict_writer.writerows(dataFrames)

def parse_replays():
    """
        Funkcja parsująca pliki .SC2Replay do plików .csv.

        :param replays_files: Ścieżki do plików .SC2Replay.
        :type replays_files: list.
        :param players: Dane na temat graczy.
        :type players: list.
        :param timeline: Stany gry.
        :type timeline: list.
        :param meta: Dane na temat gry.
        :type meta: list.
        :param time_counter: Licznik czasu gry.
        :type time_counter: int.
        :param results: Dane wczytane ze stanów gry.
        :type results: list.
        :param main_player_id: Id gracza.
        :type main_player_id: int.
        :param enemy_player_id: Id przeciwnika.
        :type enemy_player_id: int.
        :param path: Ścieżka zapisu.
        :type path: string.
        :param winner: Zwycięsca.
        :type winner: string.
        :param player: Dane gracza.
        :type player: list.
        :param enemy_player: Dane przeciwnika.
        :type enemy_player: list.
        :param playerGroundUnitsValue: Wartość jednostek naziemnych gracza.
        :type playerGroundUnitsValue: int.
        :param playerAirUnitsValue: Wartość jednostek powietrznych gracza.
        :type playerAirUnitsValue: int.
        :param enemyPlayerGroundUnitsValue: Wartość jednostek naziemnych przeciwnika.
        :type enemyPlayerGroundUnitsValue: int.
        :param enemyPlayerAirUnitsValue: Wartość jednostek powietrznych przeciwnika.
        :type enemyPlayerAirUnitsValue: int.
        :param playerBuildingsValue: Wartość budynków gracza.
        :type playerBuildingsValue: int.
        :param enemyPlayerBuildingsValue: Wartość budynków przeciwnika.
        :type enemyPlayerBuildingsValue: int.
    """
    replays_files = []
    for path, _, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(".SC2Replay")]:
            replays_files.append(os.path.join(path, filename))

    for replay_file in tqdm.tqdm(replays_files):
        try:
            players, timeline, _, _, meta = zephyrus_sc2_parser.parse_replay(replay_file, local=True)
            time_counter = 0
            results = []
            main_player_id, path = choose_player(players)
            enemy_player_id = 1 if main_player_id == 2 else 2

            winner = 'A'
            if meta['winner'] == 1:
                winner = 'A'
            elif meta['winner'] == 2:
                winner = 'B'
            else:
                print("error")

            for tick in timeline:
                player = tick[main_player_id]
                enemy_player = tick[enemy_player_id]

                playerGroundUnitsValue, playerAirUnitsValue = get_army_value(player['unit'])
                enemyPlayerGroundUnitsValue, enemyPlayerAirUnitsValue = get_army_value(enemy_player['unit'])

                playerBuildingsValue = get_buildings_value(player['building'])
                enemyPlayerBuildingsValue = get_buildings_value(enemy_player['building'])

                results.append(
                    dict(
                        GameId=list(replays_files).index(replay_file),
                        Frame=time_counter,
                        Race=players[main_player_id].race,
                        Minerals=player['unspent_resources']['minerals'],
                        Gas=player['unspent_resources']['gas'],
                        Supply=player['supply'],
                        TotalMinerals=player['resources_collected']['minerals'],
                        TotalGas=player['resources_collected']['gas'],
                        TotalSupply=player['supply_cap'],
                        GroundUnitValue=playerGroundUnitsValue,
                        AirUnitValue=playerAirUnitsValue,
                        BuildingValue=playerBuildingsValue,
                        EnemyRace=players[enemy_player_id].race,
                        EnemyMinerals=enemy_player['unspent_resources']['minerals'],
                        EnemyGas=enemy_player['unspent_resources']['gas'],
                        EnemySupply=enemy_player['supply'],
                        EnemyTotalMinerals=enemy_player['resources_collected']['minerals'],
                        EnemyTotalGas=enemy_player['resources_collected']['gas'],
                        EnemyTotalSupply=enemy_player['supply_cap'],
                        EnemyGroundUnitValue=enemyPlayerGroundUnitsValue,
                        EnemyAirUnitValue=enemyPlayerAirUnitsValue,
                        EnemyBuildingValue=enemyPlayerBuildingsValue,
                        Win=winner
                    )
                )
                time_counter += 5

            save_to_csv(results, path)

        except Exception as e:
            print(f"Failed for {replay_file}: {e}")
            continue


if __name__ == '__main__':
    parse_replays()