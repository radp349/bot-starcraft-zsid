import csv
import os
from joblib import dump
import numpy as np
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier

REPLAY_DIRECTORY = os.path.dirname(os.path.abspath("TrainPredicator.py")) + "/parsed_replays"


def train_kmeans():
    """
        Funkcja ucząca klasyfikatory kmeans i zapisująca je do plików .joblib.

        :param file_name: Nazwa pliku .csv.
        :type file_name: string.
        :param reader: Obiket parsujący plik .csv.
        :type reader: reader.
        :param csv_data: Dane wczytane z pliku .csv.
        :type csv_data: list.
        :param data: Odpowiednio przygotowane dane wczytane z pliku .csv.
        :type data: array.
        :param X: Stany gry.
        :type X: array.
        :param Y: Dane zwycięstw w poszczególnych grach.
        :type Y: array.
        :param neigh: Predicator kmeans.
        :type neigh: KNeighborsClassifier.
    """
    for file in os.listdir(REPLAY_DIRECTORY):
        file_name = file.split('.')[0]
        with open(REPLAY_DIRECTORY + "/" + file, newline='') as f:
            reader = csv.reader(f)
            csv_data = list(reader)
        data = np.delete(np.array(csv_data)[1:, :], [0, 2, 6, 7, 8, 12, 16, 17, 18],
                         1)  # wybieranie konkretnych kolumn z csv
        X, Y = data[:, :-1], data[:, -1]
        Y = np.where(Y == 'A', 0, 1)

        neigh = KNeighborsClassifier(n_neighbors=200)
        neigh.fit(X, Y)
        """ P = neigh.predict(X)
        print(file + " accuracy:", metrics.accuracy_score(Y, P)) """
        dump(neigh, "trained_kmeans/bot_vs_bot/" + file_name + '.joblib')

        if file == 'TvsP.csv' or file == 'PvsZ.csv' or file == 'ZvsT.csv':
            data = np.delete(np.array(csv_data)[1:, :], [0, 2, 6, 7, 8, 12, 16, 17, 18],
                             1)  # wybieranie konkretnych kolumn z csv
            X, Y = data[:, :-1], data[:, -1]
            X[:, 1:7], X[:, 7:] = X[:, 7:], X[:, 1:7].copy()
            Y = np.where(Y == 'B', 0, 1)

            neigh = KNeighborsClassifier(n_neighbors=200)
            neigh.fit(X, Y)
            """ P = neigh.predict(X)
            print(file + " accuracy:", metrics.accuracy_score(Y, P)) """
            dump(neigh, "trained_kmeans/bot_vs_bot/" + list(file_name)[3] + 'vs' + list(file_name)[0] + '.joblib')

        data = np.delete(np.array(csv_data)[1:, :], [0, 2, 6, 7, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
                         1)  # wybieranie konkretnych kolumn z csv
        X, Y = data[:, :-1], data[:, -1]
        Y = np.where(Y == 'A', 0, 1)

        neigh = KNeighborsClassifier(n_neighbors=200)
        neigh.fit(X, Y)
        """ P = neigh.predict(X)
        print(file + " accuracy:", metrics.accuracy_score(Y, P)) """
        dump(neigh, "trained_kmeans/human_vs_bot/" + file_name + '.joblib')

        if file == 'TvsP.csv' or file == 'PvsZ.csv' or file == 'ZvsT.csv':
            data = np.delete(np.array(csv_data)[1:, :], [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 17, 18],
                             1)  # wybieranie konkretnych kolumn z csv
            X, Y = data[:, :-1], data[:, -1]
            Y = np.where(Y == 'B', 0, 1)

            neigh = KNeighborsClassifier(n_neighbors=200)
            neigh.fit(X, Y)
            """ P = neigh.predict(X)
            print(file + " accuracy:", metrics.accuracy_score(Y, P)) """
            dump(neigh, "trained_kmeans/human_vs_bot/" + list(file_name)[3] + 'vs' + list(file_name)[0] + '.joblib')


if __name__ == '__main__':
    train_kmeans()
