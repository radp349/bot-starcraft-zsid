from pysc2.agents import base_agent
from pysc2.lib import actions, units, upgrades
import numpy as np
import math
import MineralGatheringMethods as br
import GeneralMethods as GenM
import TrainArmy as ta
import ResearchUpgrades as ru
import BuildingConstructs as bc
import UnitsSkills as us
import EnemySearchAgentMethods as esam
import AttackMethods as am
import BaseBuildingMethods as bbm

import random


class LearningAgent(base_agent.BaseAgent):

    def step(self, obs):
        super(LearningAgent, self).step(obs)
        if obs.first():
            command_center = GenM.get_my_units_by_type(
                obs, units.Terran.CommandCenter)[0]
            self.base_top_left = (command_center.x < 32)
            self.first_base_cords = (command_center.x, command_center.y)

    actions = ("harvest_all_minerals",
               "build_supply_depot",
               "train_scv",
               "train_marine",
               "attack",
               "build_barrack",
               "build_refinery",
               "build_command_center",
               "build_reactor_barrack",
               # "disengage",
               # "disengage_lowest_HP",
               # "research_infantry_weapons_level_1",
               # "research_infantry_armor_level_1",
               # "build_engineering_bay",
               "do_research"
               )

    def harvest_all_minerals(self, obs):
        return br.harvest_all_minerals(obs)

    def build_refinery(self, obs):
        return br.build_refinery(obs)

    def build_reactor_barrack(self, obs):
        return bc.build_reactor_barrack(obs)

    # def build_supply_depot(self, obs):
    #     return bc.build_supply_depot(self.base_top_left, obs)

    def build_command_center(self, obs):
        return bbm.build_command_center(self.first_base_cords, obs)

    def train_scv(self, obs):
        if len(GenM.get_my_completed_units_by_type(obs, units.Terran.CommandCenter))*26 >\
                len(GenM.get_my_units_by_type(obs, units.Terran.SCV)):
            if GenM.free_supply(obs) < 1:
                return self.build_supply_depot(obs)
            return br.train_scv(obs)
        return self.build_command_center(obs)

    def train_marine(self, obs):
        barracks = GenM.get_my_units_by_type(obs, units.Terran.Barracks)
        if len(barracks) < 1:
            return self.build_barrack(obs)
        if GenM.free_supply(obs) < 1:
            return self.build_supply_depot(obs)
        trainer = []
        for barrack in barracks:
            trainer.append(ta.train_marine(obs))
        return trainer

    # atak i micro
    def attack(self, obs):
        marines = GenM.get_my_units_by_type(obs, units.Terran.Marine)
        if len(marines) > 10:
            return am.attack(obs)
        return self.train_marine(obs)

    def disengage(self, obs):
        return am.disengage(obs)

    def disengage_lowest_HP(self, obs):
        return am.disengage_lowest_hp(obs)

    # upgrade
    def do_research(self, obs):
        if len(GenM.get_my_units_by_type(obs, units.Terran.EngineeringBay)) < 1:
            return self.build_engineering_bay(obs)
        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel1)) < 1:
            return self.research_infantry_weapons_level_1(obs)
        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel1)) < 1:
            return self.research_infantry_armor_level_1(obs)
        return self.train_marine(obs)

    def research_infantry_weapons_level_1(self, obs):
        return ru.research_infantry_weapons_level_1(obs)

    def research_infantry_armor_level_1(self, obs):
        return ru.research_infantry_armor_level_1(obs)

    # to trzeba zamienieć
    def build_barrack(self, obs):
        if len(GenM.get_my_units_by_type(obs, units.Terran.SupplyDepot)) < 1:
            return self.build_supply_depot(obs)
        barracks = GenM.get_my_units_by_type(obs, units.Terran.Barracks)
        # if len(barracks) > 3:
        #     return self.train_marine(obs)
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        barracks_xy = np.array([(unit.x, unit.y) for unit in barracks])
        if len(scvs) > 0:
            if len(barracks) == 0:
                barrack_xy = (42 + random.randint(-2, 2), 22 + random.randint(-2, 2)) \
                    if self.base_top_left else (24 + random.randint(-4, 4), 42 + random.randint(-4, 4))

                distances = GenM.get_distances(obs, scvs, barrack_xy)
                scv = scvs[np.argmin(distances)]
                return actions.RAW_FUNCTIONS.Build_Barracks_pt(
                    "now", scv.tag, barrack_xy)
            barrack_distances = GenM.get_distances(obs, barracks, self.first_base_cords)
            target_barrack = barracks_xy[0]
            cords_around = random.choice(np.array([[-2, 1], [3, 3], [-3, -3], [2, -1]]))
            barrack_xy = (target_barrack[0] + cords_around[0], target_barrack[1] + cords_around[1])
            distances = GenM.get_distances(obs, scvs, barrack_xy)
            scv = scvs[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Build_Barracks_pt(
                "now", scv.tag, barrack_xy)
        return actions.RAW_FUNCTIONS.no_op()

    def build_engineering_bay(self, obs):
        return bc.build_engineering_bay(self.base_top_left, obs)

    def build_supply_depot(self, obs):
        supply_depots = GenM.get_my_completed_units_by_type(obs, units.Terran.SupplyDepot)

        command_centers = GenM.get_my_completed_units_by_type(obs, units.Terran.CommandCenter)
        if len(command_centers) > 0:
            cc_mother = (command_centers[-1].x, command_centers[-1].y)
        else:
            cc_mother = self.first_base_cords
        scvs = GenM.get_my_completed_units_by_type(obs, units.Terran.SCV)

        r = 2.4 + 0.25*len(supply_depots)
        angel = math.radians(random.randrange(0, 360, int(60/r)))
        x = r * math.sin(angel)
        y = r * math.cos(angel)

        if (len(supply_depots) >= 0 and obs.observation.player.minerals >= 100 and
                len(scvs) > 0):
            supply_depot_xy = (cc_mother[0] + x, cc_mother[1] + y)
            distances = GenM.get_distances(obs, scvs, supply_depot_xy)
            scv = scvs[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Build_SupplyDepot_pt(
                "now", scv.tag, supply_depot_xy)
        return actions.RAW_FUNCTIONS.no_op()

