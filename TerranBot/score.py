'''

.. module:: score
   :synopsis: Zawiera zapisywanie wyniku do pliku oraz wyrysowanie go na wykresie.

'''

import numpy as np
import matplotlib.pyplot as plt
import os.path

def save(score,reward):
    f=open("score.txt","a")
    f.write(str(score)+" "+str(reward)+"\n")
    f.close()

def wykres():
    with open("score.txt") as file:
        substrings = file.read().split()
    score=[float(score) for score in substrings]
    plt.plot(score)
    plt.show()