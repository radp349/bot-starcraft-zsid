from pysc2.agents import base_agent
from pysc2.lib import actions, features, units
import TerranBot.MineralGatheringMethods as mineralGarheringMethods
import GeneralMethods as GeneralMethods
import TerranBot.BaseBuildingMethods as BaseBuildingMethods
import numpy as np


def repair_buildings(obs):
    '''
    Funkcja sprawdzająca stan życia budynków i przypisująca scvki do naprawy

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: pusta akcja lub lista akcji napraw do wykonania przez poszczególne scvki
    '''
    commnad_centers = GeneralMethods.get_my_bases(obs)
    assigned_scvs_to_repair = []
    all_scvs = GeneralMethods.get_my_units_by_type(obs, units.Terran.SCV)
    action_list = []
    if len(commnad_centers) > 0:
        for command_center in commnad_centers:
            if command_center.health < 1450:
                for i in range(15):
                    if len(all_scvs) > 0:
                        distances = GeneralMethods.get_distances(obs, all_scvs, (command_center.x, command_center.y))
                        scv = all_scvs[np.argmin(distances)]
                        action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", scv.tag,
                                                                                        command_center.tag))
                        # assigned_scvs_to_repair.append(scv.tag)
                        all_scvs.pop(np.argmin(distances))

    idle_scvs = [scv for scv in all_scvs if scv.order_length == 0]
    minerals = GeneralMethods.cluster(GeneralMethods.get_minerals(obs))
    cluster = []
    surplus = []
    commandCenter_index = 0
    for m in minerals:
        cluster.append(GeneralMethods.mean_of_units(m))
    command_centers = GeneralMethods.get_my_bases(obs)
    if len(command_centers) > 0:

        for commandCenter in command_centers:
            distances = np.linalg.norm(np.array(cluster) - np.array((commandCenter.x, commandCenter.y)), axis=1)
            nearestMinerals = minerals[np.argmin(distances)]
            surplus.append(commandCenter.assigned_harvesters - 2 * len(nearestMinerals))

        for valueWorkers in surplus:
            if valueWorkers > 0:
                all_scvs = GeneralMethods.get_my_units_by_type(obs, units.Terran.SCV)
                distances = np.linalg.norm(np.array(cluster) - np.array(
                    (command_centers[commandCenter_index].x, command_centers[commandCenter_index].y)), axis=1)
                nearestMinerals = minerals[np.argmin(distances)]
                mean = GeneralMethods.mean_of_units(nearestMinerals)
                for i in range(valueWorkers):
                    distances = GeneralMethods.get_distances(obs, all_scvs, (mean[0], mean[1]))
                    index = np.argmin(distances) + i
                    if index < len(all_scvs):
                        scv = all_scvs[index]
                        idle_scvs.append(scv)

    if len(idle_scvs) > 0:
        factories = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.Factory)
        refineries = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.Refinery)
        starports = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.Starport)
        barracks = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.Barracks)
        bunkers = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.Bunker)
        missileTurrets = GeneralMethods.get_my_completed_units_by_type(obs, units.Terran.MissileTurret)

        for idle_scv in idle_scvs:
            if len(factories) > 0:
                if factories[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    factories[0].tag))
                    factories.pop(0)
                    continue
            elif len(refineries) > 0:
                if refineries[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    refineries[0].tag))
                    refineries.pop(0)
                    continue
            elif len(starports) > 0:
                if starports[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    starports[0].tag))
                    starports.pop(0)
                    continue
            elif len(barracks) > 0:
                if barracks[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    barracks[0].tag))
                    barracks.pop(0)
                    continue
            elif len(bunkers) > 0:
                if bunkers[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    bunkers[0].tag))
                    bunkers.pop(0)
                    continue
            elif len(missileTurrets) > 0:
                if missileTurrets[0].health < 1200:
                    action_list.append(actions.RAW_FUNCTIONS.Effect_Repair_SCV_unit("now", idle_scv.tag,
                                                                                    missileTurrets[0].tag))
                    missileTurrets.pop(0)
                    continue
    if len(action_list) > 0:
        return action_list
    return actions.RAW_FUNCTIONS.no_op()
