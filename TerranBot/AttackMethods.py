from pysc2.lib import actions, units
import GeneralMethods as GenM
import numpy as np


def queue_attack(obs, enemy_units, my_units_tags, base_pos, attack_order, type="now", defensive=False):
    """
    Funkcja do kolejkowania ataku na konkretne jednostki.

    :param obs:
    :param enemy_units:
    :param my_units_tags:
    :param base_pos:
    :param attack_order:
    :param type:
    :return:
    """
    distances = GenM.get_distances(obs, enemy_units, base_pos)
    if defensive:
        cond = np.where(distances < 75)
        new_enemy_units = []
        for unit in cond[0]:
            new_enemy_units.append(enemy_units[unit])
        enemy_units = new_enemy_units
        if len(enemy_units) == 0:
            return attack_order
        distances = GenM.get_distances(obs, enemy_units, base_pos)
    attack_target = enemy_units[np.argmin(distances)]
    attack_position = (attack_target.x, attack_target.y)
    attack_order.append(actions.RAW_FUNCTIONS.Attack_pt(type, my_units_tags,
                                                        (attack_position[0], attack_position[1])))
    attack_target = enemy_units[np.argmax(distances)]
    attack_position = (attack_target.x, attack_target.y)
    attack_order.append(actions.RAW_FUNCTIONS.Attack_pt(type, my_units_tags,
                                                        (attack_position[0], attack_position[1])))
    return attack_order


def attack(obs, group_action=True, defensive=False):
    '''
    Funkcja zwracająca kolejkę rozkazów ataku. Cele ataku w kolejności:
    
    1. najbliższa i najdalsza jednostka bojowa,
    2. najbliższy i najdalszy robotnik,
    3. najbliższy i najdalszy budynek,
    4. jeżeli nie ma innych celów zaatakowane zostanie najdalsze pole minerałów.

    
    
    Jeżeli jednostki jednostki gracza są daleko od siebie to zostanie wydany rozkaz zgrupowania.
    Wersja dla Terran: dodatkowo medivaki mają wydawane rozkazy podążania za marinami i maruderami

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Lista akcji ataków punktowych
    '''
    my_units = GenM.get_my_combat_units(obs)
    if len(my_units) < 1:
        return actions.RAW_FUNCTIONS.no_op()
    my_units_tags = np.array([unit.tag for unit in my_units])
    enemy_buildings = GenM.get_enemy_buildings(obs)
    enemy_combat_units = GenM.get_enemy_combat_units(obs)
    enemy_workers = GenM.get_enemy_workers(obs)
    completed_command_centers = GenM.get_my_bases(obs)
    attack_order = []
    # marine optimization
    if group_action:
        clustered_marines = GenM.cluster(my_units, ep=0.00001, samples=5)
        for cm in clustered_marines:
            mvu = GenM.mean_div_of_units(cm)
            if mvu > 10:
                middle_of_marines = GenM.mean_of_units(cm)

                marines_tags_local = np.array([unit.tag for unit in cm])
                attack_order.append(actions.RAW_FUNCTIONS.Attack_pt("now", marines_tags_local,
                                                                    (middle_of_marines[0], middle_of_marines[1])))

    if len(completed_command_centers) > 0:
        base_pos = (completed_command_centers[0].x, completed_command_centers[0].y)
        if len(enemy_combat_units) > 0:
            attack_order = queue_attack(obs, enemy_combat_units, my_units_tags, base_pos, attack_order, defensive=defensive)

        if len(enemy_workers) > 0:
            attack_order = queue_attack(obs, enemy_workers, my_units_tags, base_pos, attack_order, type="queued", defensive=defensive)
        if len(enemy_buildings) > 0:
            attack_order = queue_attack(obs, enemy_buildings, my_units_tags, base_pos, attack_order, type="queued", defensive=defensive)
        else:
            minerals = GenM.cluster(GenM.get_minerals(obs))
            cluster = []
            for m in minerals:
                cluster.append(GenM.mean_of_units(m))

            distances = np.linalg.norm(np.array(cluster)
                                       - np.array(base_pos),
                                       axis=1)
            attack_position = cluster[np.argmax(distances)]
            attack_order.append(actions.RAW_FUNCTIONS.Attack_pt("queued", my_units_tags,
                                                                (attack_position[0], attack_position[1])))

        # marines = GenM.get_my_units_by_type(obs, units.Terran.Marine)
        # marauders = GenM.get_my_units_by_type(obs, units.Terran.Marauder)
        # medivacs = GenM.get_my_units_by_type(obs, units.Terran.Medivac)
        # if len(medivacs) > 0 and (len(marines) > 0 or len(marauders) > 0):
        #     my_medivacs_tags = [unit.tag for unit in medivacs]
        #     if len(marauders) > 0:
        #         marauder = marauders[0]
        #         attack_order.append(actions.RAW_FUNCTIONS.Attack_unit("now", my_medivacs_tags, marauder.tag))
        #     else:
        #         marine = marines[0]
        #         attack_order.append(actions.RAW_FUNCTIONS.Attack_unit("now", my_medivacs_tags, marine.tag))
        return attack_order
    return actions.RAW_FUNCTIONS.no_op()


def disengage(obs, first_base_cords):
    '''
    Funkcja zwracająca pojednynczy rozkaz powrotu do najbardziej wysuniętej bazy.
    Rozkaz jest akcją ataku punktowego wię jednostki w trakcie powrotu będą atakowały napotakane wrogie jednostki.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :returns: Akcja powrotu do bazy lub pusta akcja
    '''
    my_combat_units = GenM.get_my_combat_units(obs)
    my_units_tags = np.array([unit.tag for unit in my_combat_units])
    if len(my_combat_units) > 0:
        command_centers = GenM.get_my_bases(obs)
        if len(command_centers) == 0:
            base_pos = first_base_cords
        else:
            command_centers = list(GenM.sort_by_dist(obs, first_base_cords, command_centers))
            base_pos = (command_centers[-1].x, command_centers[-1].y)
        return actions.RAW_FUNCTIONS.Attack_pt("now", my_units_tags, base_pos)
    return actions.RAW_FUNCTIONS.no_op()


def disengage_lowest_hp(obs):
    '''
    Funkcja zwracająca rozkaz powrotu do bazy jednostki z najmniejszym poziomem zdrowia.
    Celem tej funkcji jest wycofywanie rannych jednostek.

    :param obs: Obecny stan gry.
    :type obs: timestep.

    :returns:Akcja powrotu do bazy lub pusta akcja
    '''
    marines = GenM.get_my_combat_units(obs)
    marines_healths = [unit.health for unit in marines]
    marines_tags = [unit.tag for unit in marines]
    completed_command_centers = GenM.get_my_bases(obs)
    if len(marines) > 0 & len(completed_command_centers) > 0:
        base_pos = (completed_command_centers[0].x, completed_command_centers[0].y)
        disengaged_marine = marines_tags[np.argmin(marines_healths)]
        return actions.RAW_FUNCTIONS.Move_pt("now", disengaged_marine, base_pos)
    return actions.RAW_FUNCTIONS.no_op()
