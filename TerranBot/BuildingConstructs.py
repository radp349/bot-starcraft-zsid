from pysc2.lib import units, actions, features
import GeneralMethods as gm
import numpy as np
import angle as ang


def build_refinery(obs):
    '''Funkcja zlecająca budowę rafinerii najbliższej scv.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę rafinerii.

    '''
    neutral_vespene_geysers = gm.get_units_by_type(obs, units.Neutral.VespeneGeyser)
    refineries = gm.get_my_units_by_type(obs, units.Terran.Refinery)
    indexs = []
    index = 0
    if len(refineries) > 0:
        for geyser in neutral_vespene_geysers:
            for refinery in refineries:
                if (geyser.x, geyser.y) == (refinery.x, refinery.y):
                    indexs.append(index)
            index += 1

        substract_index = 0
        for index in indexs:
            neutral_vespene_geysers.pop(index - substract_index)
            substract_index += 1
        neutral_geysers = neutral_vespene_geysers
    else:
        neutral_geysers = neutral_vespene_geysers

    command_centers = gm.get_my_units_by_type(obs, units.Terran.CommandCenter)  # tablica
    orbital_commands = gm.get_my_units_by_type(obs, units.Terran.OrbitalCommand)  # wszystkich typów
    planetary_fortresses = gm.get_my_units_by_type(obs, units.Terran.PlanetaryFortress)  # baz
    exps = np.concatenate((command_centers, orbital_commands, planetary_fortresses))
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    if len(refineries) < 3 and len(neutral_geysers) > 0 and obs.observation.player.minerals >= 75 and len(
            scvs) > 0:
        distancesGeysers = gm.get_distances(obs, neutral_geysers, (exps[0].x, exps[0].y))
        geyser = neutral_geysers[np.argmin(distancesGeysers)]
        distances = gm.get_distances(obs, scvs, (geyser.x, geyser.y))
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Refinery_pt("now", scv.tag, geyser.tag)
    return actions.RAW_FUNCTIONS.no_op()


def build_supply_depot(first_base_cords, obs):
    '''Funkcja zlecająca budowę magazynu najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę magazynu.

    '''
    supply_depots_lowered = gm.get_my_completed_units_by_type(obs, units.Terran.SupplyDepotLowered)
    supply_depots = gm.get_my_completed_units_by_type(obs, units.Terran.SupplyDepot)
    supply_depots.extend(supply_depots_lowered)  # tablica wszystkich magazynów
    scvs = gm.get_my_completed_units_by_type(obs, units.Terran.SCV)  # tablica wszystkich robotników
    r = 5.75  # promień okręgu po jakim budujemy
    angle = ang.angle_generator(0, 360, 45)  # generowanie zakresu kątów na jakich budujemy
    x = ang.sin_processing(r, angle) + 0.5  # wyznaczenie pierwszej składowej(x) punktu na okręgu, plus wyrównanie
    y = ang.cos_processing(r, angle)  # wyznaczenie drugiej składowej(y) punktu na okręgu
    exps = gm.get_my_bases(obs)  # uzyskanie listy posiadanych baz
    if len(supply_depots) >= 0 and obs.observation.player.minerals >= 100 and len(scvs) > 0 and len(
            exps) > 0:  # warunki dla budynku

        supply_depot_xy = (exps[0].x + x, exps[0].y + y)  # pozycja budowy magazynu na okręgu
        distances = gm.get_distances(obs, scvs, supply_depot_xy)  # wykrycie i zangażowanie do budowy
        scv = scvs[np.argmin(distances)]  # najbliższego robotnika
        return actions.RAW_FUNCTIONS.Build_SupplyDepot_pt("now", scv.tag, supply_depot_xy)
    return actions.RAW_FUNCTIONS.no_op()  # przywołanie akcji budowania magazynu


def build_barrack(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę koszarów najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę koszarów.

    '''
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    supply_depots = gm.get_my_completed_units_by_type(obs, units.Terran.SupplyDepot)
    supply_depots_lowered = gm.get_my_completed_units_by_type(obs, units.Terran.SupplyDepotLowered)
    scvs = gm.get_my_completed_units_by_type(obs, units.Terran.SCV)
    r = 12
    if base_top_left:
        angle = ang.angle_generator(270, 550, 35)
    else:
        angle = ang.angle_generator(90, 370, 35)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(barracks) >= 0 and len(supply_depots) + len(supply_depots_lowered) > 0 and len(scvs) > 0:
        barrack_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, barrack_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Barracks_pt("now", scv.tag, barrack_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_bunker(base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę bunkru najbliższej scv.

    :param base_cords: Współrzędne bazy.
    :type base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę bunkru.

    '''
    bunkers = gm.get_my_completed_units_by_type(obs, units.Terran.Bunker)
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 0
    if base_top_left:
        angle = ang.angle_generator(0, 0, 0)
    else:
        angle = ang.angle_generator(0, 0, 0)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(bunkers) == 0 and len(barracks) > 0 and obs.observation.player.minerals >= 100 and len(scvs) > 0:
        bunker_xy = (base_cords[0] + x, base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, bunker_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Bunker_pt("now", scv.tag, bunker_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_ghost_academy(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę akademii duchów najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę akademii duchów.

    '''
    ghost_academies = gm.get_my_completed_units_by_type(obs, units.Terran.GhostAcademy)
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 20
    if base_top_left:
        angle = ang.angle_generator(55, 75, 20)
    else:
        angle = ang.angle_generator(215, 235, 20)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(ghost_academies) == 0 and len(barracks) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 50 and len(scvs) > 0:
        ghost_academy_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, ghost_academy_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_GhostAcademy_pt("now", scv.tag, ghost_academy_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_armory(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę zbrojownii najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę zbrojownii.

    '''

    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 26
    if base_top_left:
        angle = ang.angle_generator(100, 120, 20)
    else:
        angle = ang.angle_generator(265, 285, 20)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(armories) == 0 and len(factories) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100 and len(scvs) > 0:
        armory_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, armory_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Armory_pt("now", scv.tag, armory_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_star_port(base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę protu gwiezdnego najbliższej scv.

    :param base_cords: Współrzędne bazy.
    :type base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę portu gwiezdnego.

    '''
    star_ports = gm.get_my_completed_units_by_type(obs, units.Terran.Starport)
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 26

    if len(star_ports) == 0 and len(factories) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100 and len(scvs) > 0:
        if base_top_left:
            angle = ang.angle_generator(80, 100, 20)
        else:
            angle = ang.angle_generator(250, 270, 20)
        x = ang.sin_processing(r, angle)
        y = ang.cos_processing(r, angle)
        star_port_xy = (base_cords[0] + x, base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, star_port_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Starport_pt(
            "now", scv.tag, star_port_xy)
    if len(star_ports) > 0:
        bases = gm.get_my_completed_units_by_type(obs, units.Terran.PlanetaryFortress)
        if len(bases) == 0:
            return actions.RAW_FUNCTIONS.no_op()
        r = 12
        if base_top_left:
            angle = ang.angle_generator(0, 360, 20)
        else:
            angle = ang.angle_generator(0, 360, 20)
        x = ang.sin_processing(r, angle)
        y = ang.cos_processing(r, angle)
        star_port_xy = (bases[0].x + x, bases[0].y + y)
        distances = gm.get_distances(obs, scvs, star_port_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Starport_pt(
            "now", scv.tag, star_port_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_fusion_core(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę rdzenia fuzyjnego najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę rdzenia fuzyjnego.

    '''
    fusion_cores = gm.get_my_completed_units_by_type(obs, units.Terran.FusionCore)
    star_ports = gm.get_my_completed_units_by_type(obs, units.Terran.Starport)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 24
    if base_top_left:
        angle = ang.angle_generator(65, 85, 20)
    else:
        angle = ang.angle_generator(280, 300, 20)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(fusion_cores) == 0 and len(star_ports) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 150 and len(scvs) > 0:
        fusion_core_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, fusion_core_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_FusionCore_pt(
            "now", scv.tag, fusion_core_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_factory(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę fabryki najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę fabryki.

    '''
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 19
    if base_top_left:
        angle = ang.angle_generator(90, 130, 20)
    else:
        angle = ang.angle_generator(270, 310, 20)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(factories) < 2 and len(barracks) > 0 and obs.observation.player.minerals >= 150 and \
            obs.observation.player.vespene >= 100 and len(scvs) > 0:
        factory_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, factory_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_Factory_pt(
            "now", scv.tag, factory_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_engineering_bay(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę stacji inżynieryjnej najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za stacji inżynieryjnej.

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 20
    if base_top_left:
        angle = ang.angle_generator(55, 85, 15)
    else:
        angle = ang.angle_generator(235, 265, 15)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(engineering_bays) < 2 and obs.observation.player.minerals >= 125 and len(scvs) > 0:
        engineering_bay_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, engineering_bay_xy)
        scv = scvs[np.argmin(distances)]
        return actions.RAW_FUNCTIONS.Build_EngineeringBay_pt(
            "now", scv.tag, engineering_bay_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_missile_turret(first_base_cords, base_top_left, obs):
    '''Funkcja zlecająca budowę wieżyczki rakietowej najbliższej scv.

    :param first_base_cords: Współrzędne startowej bazy.
    :type first_base_cords: tuple.
    :param base_top_left: Położenie bazy w lewym górnym rogu mapy.
    :type base_top_left: bool.
    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę wieżyczki rakietowej.

    '''
    missile_turrets = gm.get_my_completed_units_by_type(obs, units.Terran.MissileTurret)
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
    r = 13
    if base_top_left:
        angle = ang.angle_generator(180, 276, 8)
    else:
        angle = ang.angle_generator(0, 96, 8)
    x = ang.sin_processing(r, angle)
    y = ang.cos_processing(r, angle)
    if len(missile_turrets) >= 0 and len(engineering_bays) > 0 and obs.observation.player.minerals >= 100 and len(
            scvs) > 0:
        missile_turret_xy = (first_base_cords[0] + x, first_base_cords[1] + y)
        distances = gm.get_distances(obs, scvs, missile_turret_xy)
        scv = scvs[np.argmin(distances)]

        return actions.RAW_FUNCTIONS.Build_MissileTurret_pt(
            "now", scv.tag, missile_turret_xy)
    return actions.RAW_FUNCTIONS.no_op()


def build_techlab_barrack(obs):
    '''Funkcja zlecająca budowę dobudówki(techlab) do koszarów.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do koszarów.

    '''
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    if len(barracks) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(barracks):
            barrack = barracks[i]
            if barrack.addon_unit_type == units.Terran.BarracksTechLab or barrack.addon_unit_type == units.Terran.BarracksReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", barrack.tag),
                        actions.RAW_FUNCTIONS.Build_TechLab_Barracks_quick(
                            "now", barrack.tag)]

    return actions.RAW_FUNCTIONS.no_op()


def build_techlab_factory(obs):
    '''Funkcja zlecająca budowę dobudówki(techlab) do fabrykii.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do fabrykii.

    '''
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    if len(factories) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(factories):
            factory = factories[i]
            if factory.addon_unit_type == units.Terran.FactoryTechLab or factory.addon_unit_type == units.Terran.FactoryReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", factory.tag),
                        actions.RAW_FUNCTIONS.Build_TechLab_Factory_quick(
                            "now", factory.tag)]
    return actions.RAW_FUNCTIONS.no_op()


def build_techlab_starport(obs):
    '''Funkcja zlecająca budowę dobudówki(techlab) do portu gwiezdnego.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do portu gwiezdnego.

    '''
    starports = gm.get_my_completed_units_by_type(obs, units.Terran.Starport)
    if len(starports) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(starports):
            starport = starports[i]
            if starport.addon_unit_type == units.Terran.StarportTechLab or starport.addon_unit_type == units.Terran.StarportReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", starport.tag),
                        actions.RAW_FUNCTIONS.Build_TechLab_Starport_quick(
                            "now", starport.tag)]
    return actions.RAW_FUNCTIONS.no_op()


def build_reactor_barrack(obs):
    '''Funkcja zlecająca budowę dobudówki(reaktor) do koszarów.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do koszarów.

    '''
    barracks = gm.get_my_completed_units_by_type(obs, units.Terran.Barracks)
    if len(barracks) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(barracks):
            barrack = barracks[i]
            if barrack.addon_unit_type == units.Terran.BarracksTechLab or barrack.addon_unit_type == units.Terran.BarracksReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", barrack.tag),
                        actions.RAW_FUNCTIONS.Build_Reactor_Barracks_quick(
                            "now", barrack.tag)]
    return actions.RAW_FUNCTIONS.no_op()


def build_reactor_factory(obs):
    '''Funkcja zlecająca budowę dobudówki(reaktor) do fabrykii.

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do fabrykii.

    '''
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    if len(factories) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(factories):
            factory = factories[i]
            if factory.addon_unit_type == units.Terran.FactoryTechLab or factory.addon_unit_type == units.Terran.FactoryReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", factory.tag),
                        actions.RAW_FUNCTIONS.Build_Reactor_Factory_quick(
                            "now", factory.tag)]
    return actions.RAW_FUNCTIONS.no_op()


def build_reactor_starport(obs):
    '''Funkcja zlecająca budowę dobudówki(reaktor) do portu gwiezdnego.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Pusta akcja lub akcja odpowiedzialna za budowę dobudówki do portu gwiezdnego.

    '''
    starports = gm.get_my_completed_units_by_type(obs, units.Terran.Starport)
    if len(starports) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 25:
        i = 0
        while i < len(starports):
            starport = starports[i]
            if starport.addon_unit_type == units.Terran.StarportTechLab or starport.addon_unit_type == units.Terran.StarportReactor:
                i += 1
            else:
                return [actions.RAW_FUNCTIONS.Cancel_Queue5_quick("now", starport.tag),
                        actions.RAW_FUNCTIONS.Build_Reactor_Starport_quick(
                            "now", starport.tag)]
    return actions.RAW_FUNCTIONS.no_op()
