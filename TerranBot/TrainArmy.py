from pysc2.lib import units, actions, features
import GeneralMethods as gm


def train_scv(obs):
    '''Funkcja sprawdzająca czy dana jednostka (SCV) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (SCV), lub pusta akcja

    '''
    completed_command_centers = gm.get_my_completed_units_by_type(
        obs, units.Terran.CommandCenter)
    if len(completed_command_centers) > 0 and obs.observation.player.minerals >= 50 and gm.free_supply(obs) > 0:
        command_center = gm.get_my_units_by_type(obs, units.Terran.CommandCenter)[0]
        if command_center.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_SCV_quick("now", command_center.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_marine(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Marine) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Marine), lub pusta akcja

    '''
    completed_barracks = gm.get_my_completed_units_by_type(
        obs, units.Terran.Barracks)

    if len(completed_barracks) > 0 and gm.free_supply(obs) > 0 and obs.observation.player.minerals >= 50:
        completed_barracks_with_space = [unit for unit in completed_barracks if unit.order_length < 3]
        barracks = [unit.tag for unit in completed_barracks_with_space]
        if len(barracks)>0:
            return actions.RAW_FUNCTIONS.Train_Marine_quick("now", barracks)
    return actions.RAW_FUNCTIONS.no_op()


def train_marauder(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Marauder) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Marauder), lub pusta akcja

    '''
    completed_barracks = gm.get_my_units_by_type(obs, units.Terran.Barracks)
    completed_barracks_with_techLab = [unit for unit in completed_barracks if
                                       unit.addon_unit_type == units.Terran.BarracksTechLab]

    if (len(completed_barracks_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 25):
        completed_barracks_with_space = [unit for unit in completed_barracks_with_techLab if unit.order_length < 3]
        barracks = [unit.tag for unit in completed_barracks_with_space]
        if len(barracks) > 0:
            return actions.RAW_FUNCTIONS.Train_Marauder_quick("now", barracks)
    return actions.RAW_FUNCTIONS.no_op()


def train_reaper(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Reaper) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Reaper), lub pusta akcja

    '''
    completed_barracks = gm.get_my_completed_units_by_type(
        obs, units.Terran.Barracks)

    if len(completed_barracks) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 50:
        barrack = completed_barracks[0]
        order_limit = 5
        if barrack.addon_unit_type == units.Terran.BarracksReactor:
            order_limit = 10

        if barrack.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_Reaper_quick("now", barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_ghost(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Ghost) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Ghost), lub pusta akcja

    '''
    completed_barracks = gm.get_my_completed_units_by_type(
        obs, units.Terran.Barracks)

    completed_barracks_with_techLab = [unit for unit in completed_barracks if
                                       unit.addon_unit_type == units.Terran.BarracksTechLab]

    completed_ghost_academies = gm.get_my_completed_units_by_type(
        obs, units.Terran.GhostAcademy)

    if (len(completed_barracks_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 125 and completed_ghost_academies):
        barrack = completed_barracks_with_techLab[0]
        if barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Ghost_quick("now", barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_hellion(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Ghost) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Ghost), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    if (len(completed_factories) > 0 and gm.free_supply(obs) > 0 and obs.observation.player.minerals >= 100):
        factory = completed_factories[0]

        order_limit = 5
        if factory.addon_unit_type == units.Terran.FactoryReactor:
            order_limit = 10

        if factory.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_Hellion_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_widow_mine(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Widow Mine) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Widow Mine), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    if (len(completed_factories) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 75 and obs.observation.player.minerals >= 25):
        factory = completed_factories[0]
        order_limit = 5
        if factory.addon_unit_type == units.Terran.FactoryReactor:
            order_limit = 10

        if factory.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_WidowMine_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_cyclone(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Cyclone) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Cyclone)), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    completed_factories_with_techLab = [unit for unit in completed_factories if
                                        unit.addon_unit_type == units.Terran.FactoryTechLab]

    if (len(completed_factories_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.minerals >= 100):
        factory = completed_factories_with_techLab[0]
        if factory.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Cyclone_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_siege_tank(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Siege Tank) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Siege Tank), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    completed_factories_with_techLab = [unit for unit in completed_factories if
                                        unit.addon_unit_type == units.Terran.FactoryTechLab]

    if (len(completed_factories_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.minerals >= 125):
        factory = completed_factories_with_techLab[0]
        if factory.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_SiegeTank_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_hellbat(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Hellbat) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Hellbat), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    completed_armores = gm.get_my_completed_units_by_type(
        obs, units.Terran.Armory)

    if (len(completed_factories) > 0 and len(completed_armores) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 100):
        factory = completed_factories[0]
        order_limit = 5
        if factory.addon_unit_type == units.Terran.FactoryReactor:
            order_limit = 10

        if factory.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_Hellbat_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_thor(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Thor) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Thor), lub pusta akcja

    '''
    completed_factories = gm.get_my_completed_units_by_type(
        obs, units.Terran.Factory)

    completed_factories_with_techLab = [unit for unit in completed_factories if
                                        unit.addon_unit_type == units.Terran.FactoryTechLab]

    completed_armores = gm.get_my_completed_units_by_type(
        obs, units.Terran.Armory)

    if (len(completed_factories_with_techLab) > 0 and len(completed_armores) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 300 and obs.observation.player.minerals >= 200):
        factory = completed_factories_with_techLab[0]
        if factory.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Thor_quick("now", factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_viking(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Viking) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Viking), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(
        obs, units.Terran.Starport)

    if (len(completed_starports) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.minerals >= 75):
        starport = completed_starports[0]
        order_limit = 5
        if starport.addon_unit_type == units.Terran.StarportReactor:
            order_limit = 10

        if starport.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_VikingFighter_quick("now", starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_medivac(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Medivac) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Medivac), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(obs, units.Terran.Starport)
    if (len(completed_starports) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.minerals >= 100):
        starport = completed_starports[0]
        return actions.RAW_FUNCTIONS.Train_Medivac_quick("now", starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_liberator(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Liberator) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Liberator), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(
        obs, units.Terran.Starport)

    if (len(completed_starports) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.minerals >= 150):
        starport = completed_starports[0]
        order_limit = 5
        if starport.addon_unit_type == units.Terran.StarportReactor:
            order_limit = 10

        if starport.order_length < order_limit:
            return actions.RAW_FUNCTIONS.Train_Liberator_quick("now", starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_raven(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Raven) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Raven), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(
        obs, units.Terran.Starport)

    completed_starports_with_techLab = [unit for unit in completed_starports if
                                        unit.addon_unit_type == units.Terran.StarportTechLab]

    if (len(completed_starports_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.minerals >= 200):
        starport = completed_starports_with_techLab[0]
        completed_starports_with_space = [unit for unit in completed_starports_with_techLab if unit.order_length < 2]
        starports = [unit.tag for unit in completed_starports_with_space]
        if len(starports)>0:
            return actions.RAW_FUNCTIONS.Train_Raven_quick("now", starports)
    return actions.RAW_FUNCTIONS.no_op()


def train_banshee(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Banshee) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Banshee), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(
        obs, units.Terran.Starport)

    completed_starports_with_techLab = [unit for unit in completed_starports if
                                        unit.addon_unit_type == units.Terran.StarportTechLab]

    if (len(completed_starports_with_techLab) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.minerals >= 100):
        starport = completed_starports_with_techLab[0]
        if starport.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Banshee_quick("now", starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def train_battlecruiser(obs):
    '''Funkcja sprawdzająca czy dana jednostka (Battlecruiser) może zostać wytrenowana, a jeśli tak to zwraca akcję od szkolenia

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za wyszkolenie danej jednostki (Battlecruiser), lub pusta akcja

    '''
    completed_starports = gm.get_my_completed_units_by_type(
        obs, units.Terran.Starport)

    completed_fusion_cores = gm.get_my_completed_units_by_type(
        obs, units.Terran.FusionCore)

    completed_starports_with_techLab = [unit for unit in completed_starports if
                                        unit.addon_unit_type == units.Terran.StarportTechLab]

    if (len(completed_starports_with_techLab) > 0 and len(completed_fusion_cores) > 0 and gm.free_supply(
            obs) > 0 and obs.observation.player.minerals >= 400 and obs.observation.player.minerals >= 300):
        starport = completed_starports_with_techLab[0]
        if starport.order_length < 5:
            return actions.RAW_FUNCTIONS.Train_Battlecruiser_quick("now", starport.tag)
    return actions.RAW_FUNCTIONS.no_op()
