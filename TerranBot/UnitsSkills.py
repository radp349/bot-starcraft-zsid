from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm
import numpy as np


def use_stimpack_marine(obs):
    '''Funkcja zwracająca akcję użycia Stimpack Marine

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Stimpack Marine lub pusta akcja

    '''
    marines = gm.get_my_units_by_type(obs, units.Terran.Marine)
    marines_tags = [unit.tag for unit in marines]
    if (len(marines_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_Stim_Marine_quick("now", marines_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_KD8_charge(obs):
    '''Funkcja zwracająca akcję użycia KD8 Charge

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie KD8 Charge lub pusta akcja

    '''
    reapers = gm.get_my_units_by_type(obs, units.Terran.Reaper)
    if (len(reapers)) > 0:
        reaper = reapers[0]
        return actions.RAW_FUNCTIONS.Effect_KD8Charge_pt("now", reaper.tag, (35, 42))
    return actions.RAW_FUNCTIONS.no_op()


def use_stimpack_marauder(obs):
    '''Funkcja zwracająca akcję użycia Stimpack Marauder

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Stimpack Marauder lub pusta akcja

    '''
    marauders = gm.get_my_units_by_type(obs, units.Terran.Marauder)
    marauders_tags = [unit.tag for unit in marauders]
    if (len(marauders_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_Stim_Marauder_quick("now", marauders_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_EMP_round(obs):
    '''Funkcja zwracająca akcję użycia EMP Round

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie EMP Round lub pusta akcja

    '''
    ghosts = gm.get_my_units_by_type(obs, units.Terran.Ghost)
    ghosts_tags = [unit.tag for unit in ghosts]
    if (len(ghosts_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_EMP_pt("now", ghosts_tags, (20, 20))
    return actions.RAW_FUNCTIONS.no_op()


def use_steady_targeting(obs):
    '''Funkcja zwracająca akcję użycia Steady Targeting

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Steady Targeting lub pusta akcja

    '''
    ghosts = gm.get_my_units_by_type(obs, units.Terran.Ghost)
    ghosts_tags = [unit.tag for unit in ghosts]
    if (len(ghosts_tags)) > 0:
        marines = gm.get_my_units_by_type(obs, units.Terran.Marine)
        if (len(marines) > 0):
            marine = marines[0]
            return actions.RAW_FUNCTIONS.Effect_GhostSnipe_unit("now", ghosts_tags, marine.tag)
    return actions.RAW_FUNCTIONS.no_op()


def use_cloack_on_ghost(obs):
    '''Funkcja zwracająca akcję użycia Cloack On Ghost

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Cloack on Ghost lub pusta akcja

    '''
    ghosts = gm.get_my_units_by_type(obs, units.Terran.Ghost)
    ghosts_tags = [unit.tag for unit in ghosts]
    if (len(ghosts_tags)) > 0:
        return actions.RAW_FUNCTIONS.Behavior_CloakOn_Ghost_quick("now", ghosts_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_cloack_off_ghost(obs):
    '''Funkcja zwracająca akcję użycia Cloack Off Ghost

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Cloack off Ghost lub pusta akcja

    '''
    ghosts = gm.get_my_units_by_type(obs, units.Terran.Ghost)
    ghosts_tags = [unit.tag for unit in ghosts]
    if (len(ghosts_tags)) > 0:
        return actions.RAW_FUNCTIONS.Behavior_CloakOff_Ghost_quick("now", ghosts_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_burrow_down_widow_mine(obs):
    '''Funkcja zwracająca akcję użycia Burrow Down Widow Mine

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Burrow Down Widow Mine lub pusta akcja

    '''
    widow_mines = gm.get_my_units_by_type(obs, units.Terran.WidowMine)
    widow_mines_tags = [unit.tag for unit in widow_mines]
    if (len(widow_mines_tags)) > 0:
        return actions.RAW_FUNCTIONS.BurrowDown_WidowMine_quick("now", widow_mines_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_burrow_up_widow_mine(obs):
    '''Funkcja zwracająca akcję użycia Burrow Up Widow Mine

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Burrow Up Widow Mine lub pusta akcja

    '''
    widow_mines = gm.get_my_units_by_type(obs, units.Terran.WidowMineBurrowed)
    widow_mines_tags = [unit.tag for unit in widow_mines]
    if (len(widow_mines_tags)) > 0:
        return actions.RAW_FUNCTIONS.BurrowUp_WidowMine_quick("now", widow_mines_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_hellion(obs):
    '''Funkcja zwracająca akcję użycia Morph Hellion

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Hellion lub pusta akcja

    '''
    hellbats = gm.get_my_units_by_type(obs, units.Terran.Hellbat)
    hellbats_tags = [unit.tag for unit in hellbats]
    if (len(hellbats_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_Hellion_quick("now", hellbats_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_hellbat(obs):
    '''Funkcja zwracająca akcję użycia Morph Hellbat

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Hellbat lub pusta akcja

    '''
    hellions = gm.get_my_units_by_type(obs, units.Terran.Hellion)
    hellions_tags = [unit.tag for unit in hellions]
    if (len(hellions_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_Hellbat_quick("now", hellions_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_siege_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Siege Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Siege Mode lub pusta akcja

    '''
    tanks = gm.get_my_units_by_type(obs, units.Terran.SiegeTank)
    tanks_tags = [unit.tag for unit in tanks]
    if (len(tanks_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_SiegeMode_quick("now", tanks_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_unsiege(obs):
    '''Funkcja zwracająca akcję użycia Morph Unsiege

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Unsiege lub pusta akcja

    '''
    tanks = gm.get_my_units_by_type(obs, units.Terran.SiegeTankSieged)
    tanks_tags = [unit.tag for unit in tanks]
    if (len(tanks_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_Unsiege_quick("now", tanks_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_nuke(obs, base_top_left):
    '''Funkcja zwracająca akcję użycia Nuke

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Nuke lub pusta akcja

    '''
    ghosts = gm.get_my_units_by_type(obs, units.Terran.Ghost)
    ghosts_tags = [unit.tag for unit in ghosts]
    if (len(ghosts_tags)) > 0:
        xy = (39, 40) if base_top_left else (35, 16)
        return actions.RAW_FUNCTIONS.Effect_NukeCalldown_pt("now", ghosts_tags, xy)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_thor_hi_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Thor Hi Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Thor Hi Mode lub pusta akcja

    '''
    thors = gm.get_my_units_by_type(obs, units.Terran.Thor)
    thors_tags = [unit.tag for unit in thors]
    if (len(thors_tags)) > 0:
        # xy = (39, 40) if self.base_top_left else (35, 16)
        return actions.RAW_FUNCTIONS.Morph_ThorHighImpactMode_quick("now", thors_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_thor_e_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Thor E Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Thor E Mode lub pusta akcja

    '''
    thors = gm.get_my_units_by_type(obs, units.Terran.ThorHighImpactMode)
    thors_tags = [unit.tag for unit in thors]
    if (len(thors_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_ThorExplosiveMode_quick("now", thors_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_viking_a_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Viking A Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Viking A Mode lub pusta akcja

    '''
    vikings = gm.get_my_units_by_type(obs, units.Terran.VikingFighter)
    vikings_tags = [unit.tag for unit in vikings]
    if (len(vikings_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_VikingAssaultMode_quick("now", vikings_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_viking_f_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Viking F Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Viking F Mode lub pusta akcja

    '''
    vikings = gm.get_my_units_by_type(obs, units.Terran.VikingAssault)
    vikings_tags = [unit.tag for unit in vikings]
    if (len(vikings_tags)) > 0:
        return actions.RAW_FUNCTIONS.Morph_VikingFighterMode_quick("now", vikings_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_heal(obs):
    '''Funkcja zwracająca akcję użycia Morph Heal

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Heal lub pusta akcja

    '''
    medivacs = gm.get_my_units_by_type(obs, units.Terran.Medivac)
    medivacs_tags = [unit.tag for unit in medivacs]
    if (len(medivacs_tags)) > 0:
        scvs = gm.get_my_units_by_type(obs, units.Terran.SCV)
        scvs_tags = [unit.tag for unit in scvs]
        if len(scvs) > 0:
            scv_tag = scvs_tags[0]
            return actions.RAW_FUNCTIONS.Effect_Heal_unit("now", medivacs_tags, scv_tag)
    return actions.RAW_FUNCTIONS.no_op()


def use_ignite_afterburner(obs):
    '''Funkcja zwracająca akcję użycia Ignite Afterburner

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Ignite Afterburner lub pusta akcja

    '''
    medivacs = gm.get_my_units_by_type(obs, units.Terran.Medivac)
    medivacs_tags = [unit.tag for unit in medivacs]
    if (len(medivacs_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_MedivacIgniteAfterburners_quick("now", medivacs_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_defender_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Defender Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Defender Mode lub pusta akcja

    '''
    liberators = gm.get_my_units_by_type(obs, units.Terran.LiberatorAG)
    liberators_tags = [unit.tag for unit in liberators]
    if (len(liberators)) > 0:
        return actions.RAW_FUNCTIONS.Morph_LiberatorAAMode_quick("now", liberators_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_morph_fighter_mode(obs):
    '''Funkcja zwracająca akcję użycia Morph Fighter Mode

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Morph Fighter Mode lub pusta akcja

    '''
    liberators = gm.get_my_units_by_type(obs, units.Terran.Liberator)
    liberators_tags = [unit.tag for unit in liberators]
    if (len(liberators)) > 0:
        return actions.RAW_FUNCTIONS.Morph_LiberatorAGMode_pt("now", liberators_tags, (30, 30))
    return actions.RAW_FUNCTIONS.no_op()


def use_build_auto_turret(obs):
    '''Funkcja zwracająca akcję użycia Build Auto Turret

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Build Auto Turret lub pusta akcja

    '''
    ravens = gm.get_my_units_by_type(obs, units.Terran.Raven)
    ravens_tags = [unit.tag for unit in ravens]
    if (len(ravens_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_AutoTurret_pt("now", ravens_tags, (30, 30))
    return actions.RAW_FUNCTIONS.no_op()


def use_cloack_on_banshee(obs):
    '''Funkcja zwracająca akcję użycia Cloack On Banshee

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Cloack On Banshee lub pusta akcja

    '''
    banshees = gm.get_my_units_by_type(obs, units.Terran.Banshee)
    banshees_tags = [unit.tag for unit in banshees]
    if (len(banshees_tags)) > 0:
        return actions.RAW_FUNCTIONS.Behavior_CloakOn_Banshee_quick("now", banshees_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_cloack_off_banshee(obs):
    '''Funkcja zwracająca akcję użycia Cloack Off Banshee

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Cloack Off Banshee lub pusta akcja

    '''
    banshees = gm.get_my_units_by_type(obs, units.Terran.Banshee)
    banshees_tags = [unit.tag for unit in banshees]
    if (len(banshees_tags)) > 0:
        return actions.RAW_FUNCTIONS.Behavior_CloakOff_Banshee_quick("now", banshees_tags)
    return actions.RAW_FUNCTIONS.no_op()


def use_tactical_jump(obs):
    '''Funkcja zwracająca akcję użycia Tactical Jump

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Tactical Jump lub pusta akcja

    '''
    battlecruisers = gm.get_my_units_by_type(obs, units.Terran.Battlecruiser)
    battlecruisers_tags = [unit.tag for unit in battlecruisers]
    if (len(battlecruisers_tags)) > 0:
        return actions.RAW_FUNCTIONS.Effect_TacticalJump_pt("now", battlecruisers_tags, (30, 30))
    return actions.RAW_FUNCTIONS.no_op()


def calldown_MULE(obs):
    '''Funkcja zwracająca akcję użycia Calldown MULE

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za użycie Calldown MULE lub pusta akcja

    '''
    orbitalCommands = gm.get_my_units_by_type(obs, units.Terran.OrbitalCommand)
    if len(orbitalCommands) > 0:
        minerals = gm.cluster(gm.get_minerals(obs))
        cluster = []
        for m in minerals:
            cluster.append(gm.mean_of_units(m))

        nearestMinerals = []
        exps = gm.get_my_bases(obs)
        for exp in exps:
            distances = np.linalg.norm(np.array(cluster) - np.array((exp.x, exp.y)), axis=1)
            nearestMinerals.append(minerals[np.argmin(distances)])

        orbitalCommand = orbitalCommands[0]
        mineral = nearestMinerals[0][0]
        return actions.RAW_FUNCTIONS.Effect_CalldownMULE_unit("now", orbitalCommand.tag, mineral.tag)
    return actions.RAW_FUNCTIONS.no_op()
