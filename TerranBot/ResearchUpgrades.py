from pysc2.lib import units, actions, features, upgrades
import GeneralMethods as gm


def research_stimpack(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Stimpak), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Stimpak), lub pusta akcja

    '''
    techlab_barracks = gm.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)
    if len(techlab_barracks) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_barrack = techlab_barracks[0]
        if techlab_barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_Stimpack_quick(
                "now", techlab_barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_weapons_level_1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Weapons Level 1), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    if len(engineering_bays) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryWeaponsLevel1_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_weapons_level_2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Weapons Level 2), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    if len(engineering_bays) > 0 and len(
            armories) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >= 175:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryWeaponsLevel2_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_weapons_level_3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Weapons Level 3), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    if len(engineering_bays) > 0 and len(
            armories) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 250:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryWeaponsLevel3_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_armor_level_1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Armor Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Armor Level 1), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    
    if len(engineering_bays) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryArmorLevel1_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_armor_level_2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Armor Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Armor Level 2), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    if len(engineering_bays) > 0 and len(
            armories) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >= 175:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryArmorLevel2_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infantry_armor_level_3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infantry Armor Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infantry Armor Level 3), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    if len(engineering_bays) > 0 and len(
            armories) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 250:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranInfantryArmorLevel3_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_hi_sec_auto_tracking(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Hi Sec Auto Tracking), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Hi Sec Auto Tracking), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)

    if len(engineering_bays) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_HiSecAutoTracking_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_structure_armor(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Structure Armor), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Structure Armor), lub pusta akcja

    '''
    engineering_bays = gm.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)

    if len(engineering_bays) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        engineering_bay = engineering_bays[0]
        if engineering_bay.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranStructureArmorUpgrade_quick(
                "now", engineering_bay.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_combat_shield(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Combat Shield), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Combat Shield), lub pusta akcja

    '''
    techlab_barracks = gm.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)

    if len(techlab_barracks) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_barrack = techlab_barracks[0]
        if techlab_barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_CombatShield_quick(
                "now", techlab_barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_concussive_shells(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Concussive Shells), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Concussive Shells), lub pusta akcja

    '''
    techlab_barracks = gm.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)

    if len(techlab_barracks) > 0 and obs.observation.player.minerals >= 50 and obs.observation.player.vespene >= 50:
        techlab_barrack = techlab_barracks[0]
        if techlab_barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_ConcussiveShells_quick(
                "now", techlab_barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_infernal_pre_igniter(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Infernal Pre Igniter), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Infernal Pre Igniter), lub pusta akcja

    '''
    techlab_factories = gm.get_my_completed_units_by_type(obs, units.Terran.FactoryTechLab)

    if len(techlab_factories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_barrack = techlab_factories[0]
        if techlab_barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_InfernalPreigniter_quick(
                "now", techlab_barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_mag_field_accelerator(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Mag Field Accelerator), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Mag Field Accelerator), lub pusta akcja

    '''
    techlab_factories = gm.get_my_completed_units_by_type(obs, units.Terran.FactoryTechLab)

    if len(techlab_factories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_barrack = techlab_factories[0]
        if techlab_barrack.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_CycloneRapidFireLaunchers_quick(
                "now", techlab_barrack.tag)
    return actions.RAW_FUNCTIONS.no_op()


def build_nuke(obs):
    '''Funkcja sprawdzająca warunki opracowania (Build Nuke), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie (Build Nuke), lub pusta akcja

    '''
    factories = gm.get_my_completed_units_by_type(obs, units.Terran.Factory)
    ghost_academies = gm.get_my_completed_units_by_type(obs, units.Terran.GhostAcademy)
    if len(factories) > 0 and len(
            ghost_academies) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        ghost_academy = ghost_academies[0]
        if ghost_academy.order_length < 5:
            return actions.RAW_FUNCTIONS.Build_Nuke_quick(
                "now", ghost_academy.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_personal_cloaking(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Personal Cloacking), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Personal Cloacking), lub pusta akcja

    '''
    ghost_academies = gm.get_my_completed_units_by_type(obs, units.Terran.GhostAcademy)
    if len(ghost_academies) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        ghost_academy = ghost_academies[0]
        if ghost_academy.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_PersonalCloaking_quick(
                "now", ghost_academy.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_enhanced_shockwaves(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Enchanced Shockwaves), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Enchanced Shockwaves), lub pusta akcja

    '''
    ghost_academies = gm.get_my_completed_units_by_type(obs, units.Terran.GhostAcademy)
    if len(ghost_academies) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        ghost_academy = ghost_academies[0]
        if ghost_academy.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_EnhancedShockwaves_quick(
                "now", ghost_academy.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_weapons_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle Weapons Level 1), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleWeaponsLevel1_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_weapons_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle Weapons Level 2), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >= 175:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleWeaponsLevel2_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_weapons_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle Weapons Level 3), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 250:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleWeaponsLevel3_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_ship_weapons_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ship Weapons Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ship Weapons Level 1), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranShipWeaponsLevel1_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_ship_weapons_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ship Weapons Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ship Weapons Level 2), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >= 175:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranShipWeaponsLevel2_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_ship_weapons_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Ship Weapons Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Ship Weapons Level 3), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 250:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranShipWeaponsLevel3_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_and_ship_plating_level1(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle And Ship Level 1), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle And Ship Level 1), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel1_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_and_ship_plating_level2(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle And Ship Level 2), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle And Ship Level 2), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 175 and obs.observation.player.vespene >= 175:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel2_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_vehicle_and_ship_plating_level3(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Vehicle And Ship Level 3), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Vehicle And Ship Level 3), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)

    if len(armories) > 0 and obs.observation.player.minerals >= 250 and obs.observation.player.vespene >= 250:
        armory = armories[0]
        if armory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel3_quick(
                "now", armory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_drilling_claws(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Drilling Claws), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Drilling Claws), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    techlab_factories = gm.get_my_completed_units_by_type(obs, units.Terran.FactoryTechLab)
    if len(armories) > 0 and len(
            techlab_factories) > 0 and obs.observation.player.minerals >= 75 and obs.observation.player.vespene >= 75:
        techlab_factory = techlab_factories[0]
        if techlab_factory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_DrillingClaws_quick(
                "now", techlab_factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_smart_servos(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Smart Servos), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Smart Servos), lub pusta akcja

    '''
    armories = gm.get_my_completed_units_by_type(obs, units.Terran.Armory)
    techlab_factories = gm.get_my_completed_units_by_type(obs, units.Terran.FactoryTechLab)
    if len(armories) > 0 and len(
            techlab_factories) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_factory = techlab_factories[0]
        if techlab_factory.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_SmartServos_quick(
                "now", techlab_factory.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_corvid_reactor(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Corvid Reactor), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Corvid Reactor), lub pusta akcja

    '''
    techlab_starports = gm.get_my_completed_units_by_type(obs, units.Terran.StarportTechLab)
    if len(techlab_starports) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        techlab_starport = techlab_starports[0]
        if techlab_starport.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_RavenCorvidReactor_quick(
                "now", techlab_starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_banshee_cloaking_field(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Banshee Cloacking Field), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Banshee Cloacking Field), lub pusta akcja

    '''
    techlab_starports = gm.get_my_completed_units_by_type(obs, units.Terran.StarportTechLab)
    if len(techlab_starports) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        techlab_starport = techlab_starports[0]
        if techlab_starport.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_BansheeCloakingField_quick(
                "now", techlab_starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_hyperflight_rotors(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Hyperflight Rotors), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Hyperflight Rotors), lub pusta akcja

    '''
    techlab_starports = gm.get_my_completed_units_by_type(obs, units.Terran.StarportTechLab)
    if len(techlab_starports) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        techlab_starport = techlab_starports[0]
        if techlab_starport.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_BansheeHyperflightRotors_quick(
                "now", techlab_starport.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_weapon_refit(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Weapon Refit), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Weapon Refit), lub pusta akcja

    '''
    fusion_cores = gm.get_my_completed_units_by_type(obs, units.Terran.FusionCore)
    if len(fusion_cores) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        fusion_core = fusion_cores[0]
        if fusion_core.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_BattlecruiserWeaponRefit_quick(
                "now", fusion_core.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_high_capacity_fuel_tanks(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research High Capacity Fuel Tanks), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research High Capacity Fuel Tanks), lub pusta akcja

    '''
    fusion_cores = gm.get_my_completed_units_by_type(obs, units.Terran.FusionCore)
    if len(fusion_cores) > 0 and obs.observation.player.minerals >= 100 and obs.observation.player.vespene >= 100:
        fusion_core = fusion_cores[0]
        if fusion_core.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_HighCapacityFuelTanks_quick(
                "now", fusion_core.tag)
    return actions.RAW_FUNCTIONS.no_op()


def research_advanced_ballistics(obs):
    '''Funkcja sprawdzająca warunki opracowania ulepszenia (Research Advanced Ballistics), jeśli tak to zwracająca akcję opracowania

    :param obs: Obecny stan gry.
    :type obs: timestep.
    :returns: Akcja odpowiadająca za opracowanie ulepszenia (Research Advanced Ballistics), lub pusta akcja

    '''
    fusion_cores = gm.get_my_completed_units_by_type(obs, units.Terran.FusionCore)
    if len(fusion_cores) > 0 and obs.observation.player.minerals >= 150 and obs.observation.player.vespene >= 150:
        fusion_core = fusion_cores[0]
        if fusion_core.order_length < 5:
            return actions.RAW_FUNCTIONS.Research_AdvancedBallistics_quick(
                "now", fusion_core.tag)
    return actions.RAW_FUNCTIONS.no_op()
