import numpy as np
import GeneralMethods as gm
from pysc2.lib import actions, units, upgrades, features

def GetValues(obs, race: str) -> np.array:
    '''
    Pobieranie wartości z gry do predykcji zwycięstwa

    :param minerals: Ilość minerałów.
    :type minerals: int.
    :param gas: Ilość vespanu.
    :type gas: int.
    :param supply: Ilość supply.
    :type supply: int.
    :param groundUnitValue: Wartość jednostek naziemnych.
    :type groundUnitValue: int.
    :param airUnitValue: Wartość jednostek powietrznych.
    :type airUnitValue: int.
    :param buildingValue: Wartość budynków.
    :type buildingValue: int.

    :returns: (list): Wszystkie potrzebne wartości do zastosowania k-means.
    '''

    minerals = obs.observation.player.minerals
    gas = obs.observation.player.vespene
    supply = obs.observation.player.food_used
    groundUnitValue = CalculateGroundValue(obs, race)
    airUnitValue=CalculateAirValue(obs, race)
    buildingValue=CalculateBuildingValue(obs, race)

    # print(
    #     f"{minerals}, {gas}, {supply}, {groundUnitValue}, {totalMinerals}, {totalGas}, {totalSupply}, {AirUnitValue}, {BuildingValue}, '\n'")

    return [minerals, gas, supply, groundUnitValue, airUnitValue,
                     buildingValue]

def CalculateGroundValue(obs, race: str) -> int:
    '''
    Policzenie całkowitej wartości jednostek naziemnych, prosty koszt w minerałach i gazie
    
    :param all_ground_units: Słownik wszystkich jednostek naziemnych wraz z ich kosztem minerałowym.
    :type all_ground_units: dictionary.
    '''
    totalCost = 0
    for unit in obs.observation.raw_units:
        if unit.build_progress == 100 and unit.alliance == features.PlayerRelative.SELF:
            if unit.unit_type in all_ground_units[race]:
                 totalCost += all_ground_units[race][unit.unit_type][0] + all_ground_units[race][unit.unit_type][1]
    return totalCost

def CalculateAirValue(obs, race: str) -> int:
    '''
    Policzenie całkowitej wartości jednostek powietrznych, prosty koszt w minerałach i gazie
    
    :param all_air_units: Słownik wszystkich jednostek naziemnych wraz z ich kosztem vespanowym.
    :type all_air_units: dictionary.
    '''
    totalCost = 0
    for unit in obs.observation.raw_units:
        if unit.build_progress == 100 and unit.alliance == features.PlayerRelative.SELF:
            if unit.unit_type in all_air_units[race]:
                 totalCost += all_air_units[race][unit.unit_type][0] + all_air_units[race][unit.unit_type][1]
    return totalCost

def CalculateBuildingValue(obs, race: str) -> int:
    '''
    Policzenie całkowitej wartości budynków, prosty koszt w minerałach i gazie
    
    :param all_building: Słownik wszystkich budynków wraz z ich kosztem vespanowym.
    :type all_buildings: dictionary.
    '''
    totalCost = 0
    for unit in obs.observation.raw_units:
        if unit.build_progress == 100 and unit.alliance == features.PlayerRelative.SELF:
            if unit.unit_type in all_buildings[race]:
                 totalCost += all_buildings[race][unit.unit_type][0] + all_buildings[race][unit.unit_type][1]
    return totalCost

#########################################################
all_buildings = {"Terran": {units.Terran.Armory: [150,100], units.Terran.Barracks: [150,0], units.Terran.BarracksFlying: [150,0],
                        units.Terran.BarracksReactor: [50,50], units.Terran.BarracksTechLab: [50,25],
                        units.Terran.CommandCenter: [400,0], units.Terran.Refinery: [75,0], units.Terran.Bunker: [100,0],
                        units.Terran.CommandCenterFlying: [400,0], units.Terran.EngineeringBay: [125,0],
                        units.Terran.Factory: [150,100], units.Terran.FactoryFlying: [150,100], units.Terran.FactoryReactor: [50,50],
                        units.Terran.FactoryTechLab: [50,25], units.Terran.FusionCore: [150,450],
                        units.Terran.GhostAcademy: [150,50], units.Terran.MissileTurret: [100,0], units.Terran.OrbitalCommand: [150,0],
                        units.Terran.OrbitalCommandFlying: [150,0],
                        units.Terran.PlanetaryFortress: [150,150], units.Terran.Reactor: [50,50], units.Terran.Refinery: [75,0],
                        units.Terran.RefineryRich: [75,0], units.Terran.SensorTower: [125,100], units.Terran.Starport: [150,100],
                        units.Terran.StarportFlying: [150,100], units.Terran.StarportReactor: [50,50], units.Terran.StarportTechLab: [50,25],
                        units.Terran.SupplyDepot: [100,0], units.Terran.SupplyDepotLowered: [100, 0], units.Terran.TechLab: [50,25]}, 
                    "Zerg": {units.Zerg.Hive: [200, 150], units.Zerg.Hatchery: [300, 0], units.Zerg.HydraliskDen: [100, 100], units.Zerg.Lair: [150, 100], 
                        units.Zerg.SpawningPool: [200, 0], units.Zerg.SpineCrawler: [100, 0], units.Zerg.Spire: [200, 200],
                        units.Zerg.SporeCrawler: [75, 0], units.Zerg.SpineCrawlerUprooted: [100, 0], units.Zerg.SporeCrawlerUprooted: [75, 0],
                        units.Zerg.GreaterSpire: [300, 350], units.Zerg.RoachWarren: [150, 0], units.Zerg.EvolutionChamber: [75, 0],
                        units.Zerg.Extractor: [25, 0], units.Zerg.ExtractorRich: [25, 0], units.Zerg.BanelingNest: [100, 50], units.Zerg.LurkerDen: [100, 150],
                        units.Zerg.UltraliskCavern: [150, 200], units.Zerg.InfestationPit: [100,100], units.Zerg.NydusNetwork: [150,150] },
                    "Protoss": {units.Protoss.Nexus: [400, 0], units.Protoss.Pylon: [100, 0], units.Protoss.Assimilator: [75, 0],
                        units.Protoss.AssimilatorRich: [75, 0], units.Protoss.Gateway: [150, 0], units.Protoss.WarpGate: [0, 0],
                        units.Protoss.Stargate: [150, 150], units.Protoss.CyberneticsCore: [150, 0], units.Protoss.Forge: [150, 0],
                        units.Protoss.PhotonCannon: [150, 0], units.Protoss.ShieldBattery: [100, 0], units.Protoss.TwilightCouncil: [150, 100],
                        units.Protoss.RoboticsBay: [150, 150], units.Protoss.RoboticsFacility: [150, 100], units.Protoss.TemplarArchive: [150, 200],
                        units.Protoss.DarkShrine: [150, 150], units.Protoss.FleetBeacon: [300, 200]} }
############################################################
all_ground_units = {"Terran": {units.Terran.SCV: [50,0], units.Terran.Marine: [50,0], units.Terran.WidowMine: [75,25], units.Terran.Marauder: [100,30], 
                            units.Terran.Reaper: [50,50], units.Terran.Hellion: [100,0], units.Terran.Hellbat: [100,0], units.Terran.SiegeTank: [150, 125], units.Terran.SiegeTankSieged: [150,125],
                            units.Terran.Cyclone: [50,0], units.Terran.Ghost: [200,100], units.Terran.Thor: [300,200], units.Terran.AutoTurret: [50,0]},
                    "Zerg": {units.Zerg.Drone: [50,0], units.Zerg.Queen: [150, 0], units.Zerg.Zergling: [25,0], units.Zerg.Baneling: [50, 25],
                        units.Zerg.BanelingCocoon: [50,25], units.Zerg.Roach: [75,25], units.Zerg.Ravager: [100,100],
                        units.Zerg.Infestor: [100,150], units.Zerg.SwarmHost: [100, 75], units.Zerg.Locust: [50,50],  
                        units.Zerg.InfestedTerran: [100,0], units.Zerg.Ultralisk: [300,200]},
                    "Protoss": {units.Protoss.Probe: [50, 0], units.Protoss.Zealot: [100, 0], units.Protoss.Stalker: [125, 50], units.Protoss.Sentry: [50, 100], units.Protoss.Adept: [100, 25],
                        units.Protoss.HighTemplar: [50, 150], units.Protoss.DarkTemplar: [125, 125],units.Protoss.Immortal: [275, 100],
                        units.Protoss.Colossus: [300, 200], units.Protoss.Disruptor: [150, 150], units.Protoss.DisruptorPhased: [150, 150],
                        units.Protoss.Archon: [175, 275]}}

all_air_units = {"Terran": {units.Terran.VikingAssault: [150,75], units.Terran.VikingFighter: [150,75],
                        units.Terran.Medivac: [100,100], units.Terran.Raven: [100,200], units.Terran.Liberator: [100,200],
                        units.Terran.Banshee: [150,100], units.Terran.Battlecruiser: [400,300], units.Terran.LiberatorAG: [100,200]},
                "Zerg": {units.Zerg.LocustFlying: [100, 50], units.Zerg.Overseer: [150,50], units.Zerg.Overlord: [100,0], units.Zerg.Mutalisk: [100,100],
                        units.Zerg.BroodLord: [300,250],  units.Zerg.Viper: [100,200], units.Zerg.Corruptor: [150,100]},
                "Protoss": {units.Protoss.Observer: [25, 75], units.Protoss.WarpPrism: [250, 0],
                        units.Protoss.Phoenix: [150, 100], units.Protoss.VoidRay: [200, 150], units.Protoss.Oracle: [150, 150], units.Protoss.Carrier: [350, 250],
                        units.Protoss.Tempest: [250, 175], units.Protoss.Mothership: [400, 400], units.Protoss.MothershipCore: [100, 100]}}

#---------------------------------------------------------------------------------------------


