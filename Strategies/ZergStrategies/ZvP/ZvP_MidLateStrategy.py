import Strategies.ZergStrategies.MidLateZergStrategy as MidLateZergStrategy
import numpy as np
import GeneralMethods as GenM
from pysc2.lib import actions, units
import GeneralMethods as gm
from Strategies.ZergStrategies.ZergBaseStrategy import ZergBaseStrategy
import ZergBot.AttackMethods as am
from ZergBot import TrainArmy as ta
import ZergBot.BaseBuildingMethods as bbm


class ZvPMidLateStrategy(MidLateZergStrategy.MidLateZergStrategy):
    def __init__(self, first_base, scout_units, scouting_target):
        """
        Strategia mid-late game przeciw protosom.

        Strategia zawiera strategię gry na protosów.
        Pozostaje ta strategia do końca gry.

        :param first_base: Koordynaty pierwszej bazy
        :param scout_units: Lista jednostek wybranych do scoutu
        :param scouting_target: Numer wybranego miejsca scoutingu
        """
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_base = first_base
        self.base_top_left = (first_base.x < 128)
        self.first_base_cords = (first_base.x, first_base.y)
        self.scout_unit_chosen = scout_units
        self.scouting_target = scouting_target

    def strategy_specific_actions(self, obs):
        larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
        table = [self.train_queen(obs), self.plant_larva(obs), self.build_spore_crawler(obs),
                 self.build_infestation_pit(obs), self.build_hive(obs), self.build_ultralisk_cavern(obs),
                 self.build_overlord(obs), self.train_army(obs), self.train_overseer(obs)]
        if len(larvas) > 8:
            table.extend([self.train_army(obs), self.train_army(obs), self.train_army(obs), self.train_army(obs)])
        return table

    def train_army(self, obs):
        army = gm.get_my_combat_units(obs)
        if obs.observation.player.minerals <= 250:
            return self.do_nothing(obs)

        spire = gm.get_my_units_by_type(obs, units.Zerg.Spire)
        cocons = gm.get_my_units_by_type(obs, units.Zerg.Cocoon)
        zerglings = gm.get_my_units_by_type(obs, units.Zerg.Zergling)
        zerglings.extend([unit for unit in cocons
                          if not gm.check_upgrade_in_queue(504, cocons)])
        roaches = gm.get_my_units_by_type(obs, units.Zerg.Roach)
        roaches.extend([unit for unit in cocons
                        if not gm.check_upgrade_in_queue(489, cocons)])
        ravagers = gm.get_my_units_by_type(obs, units.Zerg.Ravager)
        ravagers.extend([unit for unit in cocons
                         if not gm.check_upgrade_in_queue(400, cocons)])
        hydralisks = gm.get_my_units_by_type(obs, units.Zerg.Hydralisk)
        hydralisks.extend([unit for unit in cocons
                           if not gm.check_upgrade_in_queue(507, cocons)])
        ultralisks = gm.get_my_units_by_type(obs, units.Zerg.Ultralisk)
        ultralisks.extend([unit for unit in cocons
                           if not gm.check_upgrade_in_queue(524, cocons)])
        mutualisk = gm.get_my_units_by_type(obs, units.Zerg.Mutalisk)
        mutualisk.extend([unit for unit in cocons
                          if not gm.check_upgrade_in_queue(507, cocons)])
        if obs.observation.player.minerals >= 500 and obs.observation.player.vespene < 100:
            return self.train_zergling(obs)

        if len(mutualisk) < 6 and len(spire) > 0:
            return self.train_mutalisk(obs)
        if len(zerglings) < 8:
            return self.train_zergling(obs)
        if len(roaches) < 6:
            return self.train_roach(obs)
        if len(ravagers) < 2:
            return self.train_ravager(obs)
        if len(hydralisks) < 3:
            return self.train_hydralisk(obs)
        if len(mutualisk) < 10:
            return self.train_mutalisk(obs)
        if len(roaches) + len(ravagers) < 12:
            return self.train_roach(obs)
        if len(ravagers) < 5:
            return self.train_ravager(obs)
        if len(mutualisk) < 12:
            return self.train_mutalisk(obs)
        if len(hydralisks) < 10:
            return self.train_hydralisk(obs)
        if len(ultralisks) < 2:
            return self.train_ultralisk(obs)

        return self.do_nothing(obs)

    def get_strategy(self, obs):
        # army = gm.get_my_combat_units(obs)
        # if len(army) < 7:
        #     return EarlyZergStrategy.EarlyZergStrategy(self.first_base, self.scout_unit_chosen, self.scouting_target)
        return self


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)
        self.army_size = 40

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] >= self.army_size or (self.targets[0] > 5 and self.my_units[0] > 0):
            if -1 in div_sign:  # or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0

    def change_army_size(self, new_size):
        self.army_size = new_size