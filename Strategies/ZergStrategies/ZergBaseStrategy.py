import random
import numpy as np
import GeneralMethods as GenM
from GeneralMethods import connect_actions_deeper
from pysc2.lib import actions, units, upgrades
from ZergBot import TrainArmy as ta
from ZergBot import BuildingConstructs as bc
import GeneralMethods as gm
import ZergBot.BaseBuildingMethods as bbm
import ZergBot.EnemySearchAgentMethods as essam
import ZergBot.AttackMethods as am
import ZergBot.MineralGatheringMethods as mgm
import ZergBot.UnitsSkills as us
import ZergBot.ResearchUpgrades as ru


class ZergBaseStrategy:
    def __init__(self):
        """Bazowa strategia Zergów. Zawiera bazowe metody budowania, ataku i trenowania jednostek."""
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.time = 0

    def update_time(self, time):
        """Uaktualnia czas gry, na bazie którego może być podejmowana inna strategia.

        :param time: Aktualny czas gry
        :type time: float
        """
        self.time = time

    def step(self, obs):
        if obs.first():
            hatchery = gm.get_my_units_by_type(
                obs, units.Zerg.Hatchery)[0]
            self.base_top_left = (hatchery.x < 128)
            self.first_base_cords = (hatchery.x, hatchery.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               "build_extractor",
               "build_overlord",
               "build_spawning_pool",
               "build_baneling_nest",
               "build_roach_warren",
               "build_lair",
               "build_hydralisk_den",
               "build_hive",
               "train_drone",
               "train_drone",
               "train_drone",
               "train_zergling",
               "train_queen",
               "plant_larva",
               "train_roach",
               "train_hydralisk",
               "train_baneling",
               "build_hatchery",
               "scout",
               "attack",
               "do_research",  # uncomment when method is ready
               "harvest_all_minerals")

    def get_strategy(self, obs):
        return self

    def get_action(self, obs):
        action = random.choice(self.actions)
        all_actions = []
        all_actions = connect_actions_deeper(all_actions, getattr(self, action)(obs))
        return all_actions

    # metoda "nic nie rób"
    def do_nothing(self, obs=0):  # added as optional to not break existing code and to not have to write obs every time
        return actions.RAW_FUNCTIONS.no_op()

    def strategy_specific_actions(self, obs):
        return actions.RAW_FUNCTIONS.no_op()

    def do_research(self, obs):
        evol_chamber = gm.get_my_units_by_type(obs, units.Zerg.EvolutionChamber)
        hydralisk_den = gm.get_my_units_by_type(obs, units.Zerg.HydraliskDen)
        spire = gm.get_my_units_by_type(obs, units.Zerg.Spire)
        bases = gm.get_my_bases(obs)

        # hatchery
        # if gm.check_upgrade_in_queue(450, bases) and \
        #         len(gm.get_my_upgrade(obs, upgrades.Upgrades.MetabolicBoost)) < 1 and \
        #         len(gm.get_my_units_by_type(obs, units.Zerg.Zergling)) > 0:
        #     return ru.research_metabolic_boost(obs)

        if len(evol_chamber) < 1:
            return self.build_evolution_chamber(obs)

        if len(gm.get_my_bases(obs)) < 2:
            return actions.RAW_FUNCTIONS.no_op()

        if gm.check_upgrade_in_queue(483, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergGroundArmorsLevel1)) < 1:
            return ru.research_ground_armor_level1(obs)

        if gm.check_upgrade_in_queue(442, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMeleeWeaponsLevel1)) < 1 and \
                len(gm.get_my_units_by_type(obs, units.Zerg.Zergling)) > 0:
            return ru.research_melee_weapons_level1(obs)

        if gm.check_upgrade_in_queue(446, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMissileWeaponsLevel1)) < 1:
            return ru.research_missile_weapons_level1(obs)

        # lair
        if gm.check_upgrade_in_queue(443, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMeleeWeaponsLevel2)) < 1 and \
                len(gm.get_my_units_by_type(obs, units.Zerg.Zergling)) > 0:
            return ru.research_melee_weapons_level2(obs)

        if gm.check_upgrade_in_queue(447, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMissileWeaponsLevel2)) < 1:
            return ru.research_missile_weapons_level2(obs)

        if gm.check_upgrade_in_queue(484, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergGroundArmorsLevel2)) < 1:
            return ru.research_ground_armor_level2(obs)

        # hydralisks
        if len(hydralisk_den) < 1:
            return self.build_hydralisk_den(obs)
        if gm.check_upgrade_in_queue(368, hydralisk_den) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.GroovedSpines)) < 1:
            return ru.research_grooved_spines(obs)

        if len(spire) < 1:  # #### spire
            return self.build_spire(obs)
        if gm.check_upgrade_in_queue(434, spire) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergFlyerWeaponsLevel1)) < 1:
            return ru.research_flyer_attack_level1(obs)
        if gm.check_upgrade_in_queue(430, spire) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergFlyerArmorsLevel1)) < 1:
            return ru.research_flyer_carapace_level1(obs)
        if gm.check_upgrade_in_queue(435, spire) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergFlyerWeaponsLevel2)) < 1:
            return ru.research_flyer_attack_level2(obs)
        if gm.check_upgrade_in_queue(431, spire) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergFlyerArmorsLevel2)) < 1:
            return ru.research_flyer_carapace_level2(obs)

        # hive
        if gm.check_upgrade_in_queue(485, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergGroundArmorsLevel3)) < 1:
            return ru.research_ground_armor_level3(obs)

        if gm.check_upgrade_in_queue(444, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMeleeWeaponsLevel3)) < 1 and \
                len(gm.get_my_units_by_type(obs, units.Zerg.Zergling)) > 0:
            return ru.research_melee_weapons_level3(obs)

        if gm.check_upgrade_in_queue(448, evol_chamber) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ZergMissileWeaponsLevel3)) < 1:
            return ru.research_missile_weapons_level3(obs)

        return actions.RAW_FUNCTIONS.no_op()

    def plant_larva(self, obs):
        return us.plant_larva(obs, self.first_base_cords)

    def harvest_all_minerals(self, obs):
        return mgm.harvest_all_minerals(obs)

    def build_hatchery(self, obs):
        bases = gm.get_my_bases(obs)
        if len(bases) >= 3:
            return self.do_nothing(obs)
        return bbm.build_hatchery(self.first_base_cords, obs)

    def scout(self, obs):
        return essam.scout(self, obs)

    def attack(self, obs):
        if self.aggression == 1:
            return am.attack(obs, self.first_base_cords)
        if self.aggression == 2:
            return am.attack(obs, self.first_base_cords, False)
        return am.disengage(obs, self.first_base_cords)

    def use_corrosive_bile(self, obs):
        return us.use_corrosive_bile(obs)

    def move_overseers(self, obs):
        overseers = gm.get_my_units_by_type(obs, units.Zerg.Overseer)
        my_units_tags = np.array([unit.tag for unit in overseers])
        if len(overseers) < 1:
            return self.do_nothing(obs)
        army = gm.get_my_combat_units(obs)
        if len(army) < 1:
            return self.do_nothing(obs)
        army = gm.sort_by_dist(obs, self.first_base_cords, army)
        base_pos = (army[-1].x, army[-1].y)
        return actions.RAW_FUNCTIONS.Attack_pt("now", my_units_tags, base_pos)

    def move_mutalisks(self, obs):
        mutalisks = gm.get_my_units_by_type(obs, units.Zerg.Mutalisk)
        my_units_tags = np.array([unit.tag for unit in mutalisks])
        if len(mutalisks) < 1:
            return self.do_nothing(obs)
        army = gm.get_my_combat_units(obs)
        if len(army) < 1:
            return self.do_nothing(obs)
        army = gm.sort_by_dist(obs, self.first_base_cords, army)
        if len(army) < 5:
            return self.do_nothing(obs)
        base_pos = (army[-4].x, army[-4].y)
        return actions.RAW_FUNCTIONS.Attack_pt("now", my_units_tags, base_pos)

    # metoda służąca do wydobywania minerałów
    def harvest_minerals(self, obs):
        drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
        idle_drones = [drone for drone in drones if drone.order_length == 0]
        if len(idle_drones) > 0:
            mineral_patches = [unit for unit in obs.observation.raw_units
                               if unit.unit_type in [
                                   units.Neutral.BattleStationMineralField,
                                   units.Neutral.BattleStationMineralField750,
                                   units.Neutral.LabMineralField,
                                   units.Neutral.LabMineralField750,
                                   units.Neutral.MineralField,
                                   units.Neutral.MineralField750,
                                   units.Neutral.PurifierMineralField,
                                   units.Neutral.PurifierMineralField750,
                                   units.Neutral.PurifierRichMineralField,
                                   units.Neutral.PurifierRichMineralField750,
                                   units.Neutral.RichMineralField,
                                   units.Neutral.RichMineralField750
                               ]]
            drone = random.choice(idle_drones)
            distances = gm.get_distances(obs, mineral_patches, (drone.x, drone.y))
            mineral_patch = mineral_patches[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Harvest_Gather_unit(
                "now", drone.tag, mineral_patch.tag)
        return actions.RAW_FUNCTIONS.no_op()

    def build_extractor(self, obs):
        extractors = gm.get_my_units_by_type(obs, units.Zerg.Extractor)
        bases = gm.get_my_bases(obs)
        if len(extractors) >= 2 * len(bases):
            return self.do_nothing(obs)
        return mgm.build_refinery(obs, self.first_base_cords)

    def build_overlord(self, obs):
        overlord_cocoons = gm.get_my_units_by_type(obs, units.Zerg.Cocoon)
        overlord_cocoons = [cocon for cocon in overlord_cocoons if not gm.check_upgrade_in_queue(483, [cocon])]
        if gm.free_supply(obs) >= 7 or len(overlord_cocoons) >= 2:
            return actions.RAW_FUNCTIONS.no_op()
        return bc.build_overlord(obs)

    def build_spawning_pool(self, obs):
        return bc.build_spawning_pool(obs, self.first_base_cords, self.base_top_left)

    def build_evolution_chamber(self, obs):
        return bc.build_evolution_chamber(obs, self.first_base_cords, self.base_top_left)

    def build_roach_warren(self, obs):
        return bc.build_roach_warren(obs, self.first_base_cords, self.base_top_left)

    def build_baneling_nest(self, obs):
        return bc.build_baneling_nest(obs, self.first_base_cords, self.base_top_left)

    def build_spine_crawler(self, obs):
        return bc.build_spine_crawler(obs, self.first_base_cords, self.base_top_left)

    def build_spore_crawler(self, obs):
        bases1 = gm.get_my_completed_units_by_type(obs, units.Zerg.Hatchery)
        bases2 = gm.get_my_completed_units_by_type(obs, units.Zerg.Lair)
        bases = gm.get_my_completed_units_by_type(obs, units.Zerg.Hive)
        bases.extend(bases1)
        bases.extend(bases2)
        if len(bases) < 1:
            return actions.RAW_FUNCTIONS.no_op()

        spore_crawlers = gm.get_my_units_by_type(obs, units.Zerg.SporeCrawler)
        if len(spore_crawlers) >= len(bases):
            return actions.RAW_FUNCTIONS.no_op()

        bases = gm.sort_by_dist(obs, self.first_base_cords, bases)
        bases_without_spore_crawler = []
        for base in bases:
            if len(spore_crawlers) > 0:
                dist = gm.get_distances(obs, spore_crawlers, (base.x, base.y))
                min_dist = min(dist)
            else:
                min_dist = 1000
            if min_dist > 20:
                bases_without_spore_crawler.append(base)

        if len(bases_without_spore_crawler) < 1:
            return actions.RAW_FUNCTIONS.no_op()

        return bc.build_spore_crawler(obs, (bases_without_spore_crawler[0].x, bases_without_spore_crawler[0].y),
                                      self.base_top_left)

    def build_lair(self, obs):
        return bc.build_lair(obs, self.first_base_cords)

    def build_hydralisk_den(self, obs):
        return bc.build_hydralisk_den(obs, self.first_base_cords, self.base_top_left)

    def build_infestation_pit(self, obs):
        return bc.build_infestation_pit(obs, self.first_base_cords, self.base_top_left)

    def build_spire(self, obs):
        return bc.build_spire(obs, self.first_base_cords, self.base_top_left)

    def build_nydus_network(self, obs):
        return bc.build_nydus_network(obs, self.first_base_cords, self.base_top_left)

    def build_lurker_den(self, obs):
        return bc.build_lurker_den(obs, self.first_base_cords, self.base_top_left)

    def build_hive(self, obs):
        return bc.build_hive(obs)

    def build_ultralisk_cavern(self, obs):
        return bc.build_ultralisk_cavern(obs, self.first_base_cords, self.base_top_left)

    def build_greater_spire(self, obs):
        return bc.build_greater_spire(obs)

    def train_drone(self, obs):
        return ta.train_drone(obs)

    def train_overseer(self, obs):
        oversers = gm.get_my_units_by_type(obs, units.Zerg.Overseer)
        oversers.extend(gm.get_my_units_by_type(obs, units.Zerg.OverseerCocoon))
        if len(oversers) > 1:
            return self.do_nothing(obs)
        return ta.train_overseer(obs)

    def train_zergling(self, obs):
        return ta.train_zergling(obs)

    def train_queen(self, obs):
        return ta.train_queen(obs, self.first_base_cords)

    def train_roach(self, obs):
        return ta.train_roach(obs)

    def train_ravager(self, obs):
        return ta.train_ravager(obs)

    def train_baneling(self, obs):
        return ta.train_baneling(obs)

    def train_infestor(self, obs):
        return ta.train_infestor(obs)

    def train_swarm_host(self, obs):
        return ta.train_swarm_host(obs)

    def train_mutalisk(self, obs):
        return ta.train_mutalisk(obs)

    def train_corruptor(self, obs):
        return ta.train_corruptor(obs)

    def train_hydralisk(self, obs):
        return ta.train_hydralisk(obs)

    def train_viper(self, obs):
        return ta.train_viper(obs)

    def train_ultralisk(self, obs):
        return ta.train_ultralisk(obs)

    def train_broodlord(self, obs):
        return ta.train_broodlord(obs)


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets)*(self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0
