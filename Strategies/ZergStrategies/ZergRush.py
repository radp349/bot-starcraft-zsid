import random
import numpy as np
import GeneralMethods as GenM
from pysc2.lib import actions, units
import GeneralMethods as gm
import Strategies.ZergStrategies.ZvZ.ZvZ_Early as ZvZ_Early
import Strategies.ZergStrategies.ZvT.ZvT_Early as ZvT_Early
import Strategies.ZergStrategies.ZvP.ZvP_Early as ZvP_Early
from Strategies.ZergStrategies.ZergBaseStrategy import ZergBaseStrategy
import ZergBot.AttackMethods as am
import ZergBot.BaseBuildingMethods as bbm
import ZergBot.MineralGatheringMethods as mgm
import ZergBot.TrainArmy as ta
import ZergBot.EnemySearchAgentMethods as essam
import ZergBot.BuildingConstructs as bc
import ZergBot.ResearchUpgrades as ru


class ZergRush(ZergBaseStrategy):    # #############
    def __init__(self, first_base, scout_unit_chosen, scouting_target):
        """
        Strategia zerg rush.

        Strategia zawiera strategię zerg rushu. W momencie nie udania się - straty jednostek i nie wygrania gry zmienia
        strategię na inną, w zależności od rasy przeciwnika.

        :param first_base: Koordynaty pierwszej bazy
        :param scout_unit_chosen: Lista jednostek wybranych do scoutu
        :param scouting_target: Numer wybranego miejsca scoutingu
        """
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_base = first_base
        self.base_top_left = (first_base.x < 128)
        self.first_base_cords = (first_base.x, first_base.y)
        self.scout_unit_chosen = scout_unit_chosen
        self.scouting_target = scouting_target
        self.state = 0  # 0-recruiting army, 1-attack, 2-we lost and coming back to standard strategy based on enemy

    def step(self, obs):
        if obs.first():
            self.first_base = gm.get_my_units_by_type(
                obs, units.Zerg.Hatchery)[0]
            self.base_top_left = (self.first_base.x < 128)
            self.first_base_cords = (self.first_base.x, self.first_base.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        if self.state < 2:
            self.aggression = self.aggression_handler.get_aggresion()
        else:
            self.aggression = 1
        if self.state == 0 and len(gm.get_my_combat_units(obs)) >= 8:
            self.state = 1
        if self.state == 3 and len(gm.get_my_combat_units(obs)) < 4:
            self.state = 2

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               # "build_extractor",
               "build_overlord",
               "build_spawning_pool",
               # "build_roach_warren",
               "build_lair",
               "build_hive",
               # "train_drone",
               "train_queen",
               "plant_larva",
               # "train_roach",
               "build_hatchery",
               "scout",
               "attack",
               "harvest_all_minerals")

    def strategy_specific_actions(self, obs):
        if self.state == 0:
            return [self.build_spawning_pool(obs),
                    self.train_army(obs), self.train_army(obs), self.train_army(obs),
                    self.train_queen(obs)]
        return [self.build_spawning_pool(obs), self.build_extractor(obs),
                self.train_army(obs), self.train_drone(obs),
                self.train_queen(obs), self.plant_larva(obs), self.research_metabolic_boost(obs), self.do_research(obs)]

    def train_army(self, obs):
        if self.state == 1 and len(gm.get_my_combat_units(obs)) >= 14:
            self.state = 3
            return self.train_drone(obs)
        if self.state == 3:
            return self.build_roach_warren(obs)
        return self.train_zergling(obs)

    def get_strategy(self, obs):
        if self.state == 2:
            enemy_building = gm.get_enemy_buildings(obs)
            if len(enemy_building) > 0:
                for building in enemy_building:
                    if (building.unit_type == gm.terran_buildings).any():
                        return ZvT_Early.EarlyZvTStrategy(self.first_base, self.scout_unit_chosen, self.scouting_target)
                    elif (building.unit_type == gm.zerg_buildings).any():
                        return ZvZ_Early.EarlyZvZStrategy(self.first_base, self.scout_unit_chosen, self.scouting_target)
                    elif (building.unit_type == gm.protoss_buildings).any():
                        return ZvP_Early.EarlyZvPStrategy(self.first_base, self.scout_unit_chosen, self.scouting_target)
        return self

    def research_metabolic_boost(self, obs):
        return ru.research_metabolic_boost(obs)

    def build_lair(self, obs):
        queens = gm.get_my_units_by_type(obs, units.Zerg.Queen)
        if len(queens) < 2:
            return self.do_nothing(obs)
        return bc.build_lair(obs, self.first_base_cords)

    def build_hatchery(self, obs):
        bases = gm.get_my_bases(obs)
        spawning_pools = gm.get_my_units_by_type(obs, units.Zerg.SpawningPool)
        if len(spawning_pools) < 1:
            return actions.RAW_FUNCTIONS.no_op()
        if len(bases) >= 2:
            return actions.RAW_FUNCTIONS.no_op()
        return bbm.build_hatchery(self.first_base_cords, obs)

    def build_extractor(self, obs):
        extractors = gm.get_my_units_by_type(obs, units.Zerg.Extractor)
        # bases = gm.get_my_bases(obs)
        if len(extractors) > 0:
            return actions.RAW_FUNCTIONS.no_op()
        return mgm.build_refinery(obs, self.first_base_cords)

    def scout(self, obs):
        drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
        if len(drones) < 14:
            return self.do_nothing(obs)
        return essam.scout(self, obs)


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] >= 8 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if -1 in div_sign or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0




