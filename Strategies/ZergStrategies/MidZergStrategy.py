import numpy as np
import GeneralMethods as GenM
from pysc2.lib import actions, units
import GeneralMethods as gm
import Strategies.ZergStrategies.MidLateZergStrategy as MidLateZergStrategy
from Strategies.ZergStrategies.ZergBaseStrategy import ZergBaseStrategy
import ZergBot.AttackMethods as am
import ZergBot.TrainArmy as ta
import ZergBot.BuildingConstructs as bc
import ZergBot.BaseBuildingMethods as bbm
import ZergBot.MineralGatheringMethods as mgm


class MidZergStrategy(ZergBaseStrategy):
    def __init__(self, first_base, scout_units, scouting_target):
        """
        Strategia zergów na mid game.

        Strategia zawiera ogólne metody ekonomiczne dla innych metod mid game'owych.

        :param first_base: Koordynaty pierwszej bazy
        :param scout_unit_chosen: Lista jednostek wybranych do scoutu
        :param scouting_target: Numer wybranego miejsca scoutingu
        """
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_base = first_base
        self.base_top_left = (first_base.x < 128)
        self.first_base_cords = (first_base.x, first_base.y)
        self.scout_unit_chosen = scout_units
        self.scouting_target = scouting_target

    def step(self, obs):
        if obs.first():
            hatchery = gm.get_my_units_by_type(
                obs, units.Zerg.Hatchery)[0]
            self.base_top_left = (hatchery.x < 128)
            self.first_base_cords = (hatchery.x, hatchery.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               "build_extractor",
               "build_overlord",
               "build_spawning_pool",
               # "build_baneling_nest",
               "build_evolution_chamber",
               "build_roach_warren",
               "build_lair",
               "build_hydralisk_den",
               # "build_hive",
               "train_drone",
               "train_drone",
               "train_drone",
               # "train_zergling",
               "train_queen",
               "train_queen",
               "train_overseer",
               "build_spore_crawler",
               "plant_larva",
               "train_roach",
               "train_ravager",
               "train_hydralisk",
               # "train_baneling",
               "build_hatchery",
               "scout",
               "attack",
               "do_research",  # uncomment when method is ready
               "harvest_all_minerals")

    def strategy_specific_actions(self, obs):
        larvas = gm.get_my_units_by_type(obs, units.Zerg.Larva)
        table = [self.build_lair(obs), self.build_hydralisk_den(obs), self.build_spore_crawler(obs),
                 self.build_hatchery(obs), self.build_overlord(obs), self.train_army(obs),
                 self.train_queen(obs), self.plant_larva(obs),
                 self.train_overseer(obs), self.move_overseers(obs)]
        if len(larvas) > 4:
            table.extend([self.train_army(obs), self.train_army(obs), self.train_army(obs), self.train_army(obs)])
        return table

    def train_army(self, obs):
        if obs.observation.player.minerals <= 150:
            return self.do_nothing(obs)
        cocons = gm.get_my_units_by_type(obs, units.Zerg.Cocoon)
        zerglings = gm.get_my_units_by_type(obs, units.Zerg.Zergling)
        zerglings.extend([unit for unit in cocons
                          if not gm.check_upgrade_in_queue(504, cocons)])
        roaches = gm.get_my_units_by_type(obs, units.Zerg.Roach)
        roaches.extend([unit for unit in cocons
                        if not gm.check_upgrade_in_queue(489, cocons)])
        ravagers = gm.get_my_units_by_type(obs, units.Zerg.Ravager)
        ravagers.extend([unit for unit in cocons
                         if not gm.check_upgrade_in_queue(400, cocons)])
        hydralisks = gm.get_my_units_by_type(obs, units.Zerg.Hydralisk)
        hydralisks.extend([unit for unit in cocons
                           if not gm.check_upgrade_in_queue(507, cocons)])
        # if len(zerglings) < 6:
        #     return self.train_zergling(obs)
        if obs.observation.player.minerals >= 500 and obs.observation.player.vespene < 50:
            return self.train_zergling(obs)
        if len(roaches) < 10:
            return self.train_roach(obs)
        if len(ravagers) < 5:
            return self.train_ravager(obs)
        if len(hydralisks) < 10:
            return self.train_hydralisk(obs)

        return self.train_roach(obs)

    def build_hydralisk_den(self, obs):
        spire = gm.get_my_units_by_type(obs, units.Zerg.Spire)
        if len(spire) < 1:
            return self.do_nothing()
        return bc.build_hydralisk_den(obs, self.first_base_cords, self.base_top_left)


    def get_strategy(self, obs):
        beses = gm.get_my_bases(obs)
        army = gm.get_my_combat_units(obs)
        if len(beses) > 4 and len(army) > 20:
            return MidLateZergStrategy.MidLateZergStrategy(self.first_base,
                                                           self.scout_unit_chosen, self.scouting_target)
        return self

    def train_ravager(self, obs):
        ravagers = gm.get_my_completed_units_by_type(obs, units.Zerg.Ravager)
        if len(ravagers) > 5:
            return actions.RAW_FUNCTIONS.no_op()
        return ta.train_ravager(obs)

    def build_hatchery(self, obs):
        bases = gm.get_my_bases(obs)
        lair = gm.get_my_units_by_type(obs, units.Zerg.Lair)
        lair.extend(gm.get_my_units_by_type(obs, units.Zerg.Hive))
        if len(lair) < 1:
            return self.do_nothing(obs)
        drones = gm.get_my_units_by_type(obs, units.Zerg.Drone)
        if len(drones) < len(bases) * 14:
            return self.do_nothing(obs)
        if len(bases) >= 5:
            return self.do_nothing()
        return bbm.build_hatchery(self.first_base_cords, obs)

    def build_extractor(self, obs):
        extractors = gm.get_my_units_by_type(obs, units.Zerg.Extractor)
        bases = gm.get_my_bases(obs)
        if len(extractors) >= len(bases) * 1.5:
            return self.do_nothing(obs)
        return mgm.build_refinery(obs, self.first_base_cords)


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)
        self.army_size = 20

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] >= self.army_size or (self.targets[0] > 5 and self.my_units[0] > 0):
            if -1 in div_sign:  # or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0

    def change_army_size(self, new_size):
        self.army_size = new_size
