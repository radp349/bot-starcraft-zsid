import numpy as np
from pysc2.lib import units
import ProtossBot.AttackMethods as am
import GeneralMethods as gm
from Strategies.ProtossStrategies.ProtossBaseStrategy import ProtossBaseStrategy


class MidProtossStrategy(ProtossBaseStrategy):
    def __init__(self, command_center, scout_units, scouting_target):
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_command_center = command_center
        self.base_top_left = (command_center.x < 128)
        self.first_base_cords = (command_center.x, command_center.y)
        self.scout_unit_chosen = scout_units
        self.scouting_target = scouting_target

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, opisuje dostepne akcje.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        if obs.first():
            self.first_command_center = gm.get_my_units_by_type(
                obs, units.Protoss.Nexus)[0]
            self.base_top_left = (self.first_command_center.x < 128)
            self.first_base_cords = (self.first_command_center.x, self.first_command_center.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    actions = ("do_nothing",
               "build_assimilator",
               "build_pylon",
               "build_robotics_bay",
               #"build_gateway",
               #"build_cybernetics_core",
               #"build_dark_shrine",
               "scout",
               "train_zealot",
               "train_stalker",
               "train_dark_templar",
               "train_colossus",
               "train_observer",
               "morph_archon",
               #"attack",
               "train_probe",
               "build_nexus",
               "do_research",
               #"build_forge",
               "train_sentry",
               #"build_twilight_council",
               #"build_robotics_facility",
               "harvest_all_minerals",
               "build_constructs"
               )

    def get_strategy(self, obs):
        return self

class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(gm.get_enemy_buildings(obs))
        self.my_units[0] = len(gm.get_my_combat_units(obs))
        self.targets[0] = len(gm.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        # print(div_sign)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0
