import random
import numpy as np
from pysc2.lib import actions, features, units, upgrades

from Predicator.Predicator import Predicator
from ProtossBot import TrainArmy as ta
from ProtossBot import ResearchUpgrades as ru
from ProtossBot import BuildingConstructs as bc
from ProtossBot import UnitsSkills as us
import GeneralMethods as GenM
import GeneralMethods as gm
from GeneralMethods import connect_actions_deeper
import ProtossBot.BaseBuildingMethods as bbm
import ProtossBot.EnemySearchAgentMethods as essam
import ProtossBot.AttackMethods as am
import ProtossBot.MineralGatheringMethods as mgm


class ProtossBaseStrategy():
    def __init__(self):
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_command_center = 0

    def step(self, obs):
        if obs.first():
            self.first_command_center = gm.get_my_units_by_type(
                obs, units.Protoss.Nexus)[0]
            self.base_top_left = (self.first_command_center.x < 128)
            self.first_base_cords = (self.first_command_center.x, self.first_command_center.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0

    # krotka z wszystkimi możliwymi akcjami, które wykonuje agent
    actions = ("do_nothing",
               "build_assimilator",
               "build_pylon",
               #"build_gateway",
               #"build_cybernetics_core",
               #"build_dark_shrine",
               "build_robotics_bay",
               "scout",
               "train_zealot",
               "train_stalker",
               "train_dark_templar",
               "train_observer",
               "train_colossus",
               "morph_archon",
               "attack",
               "train_probe",
               "build_nexus",
               "do_research",
               #"build_forge",
               "train_sentry",
               "use_guardian_shield",
               #"build_twilight_council",
               #"build_robotics_facility",
               "harvest_all_minerals",
               "build_constructs"
               )

    def build_constructs(self, obs):
        gateways = GenM.get_my_units_by_type(obs, units.Protoss.Gateway)
        if len(gateways)<1:
            return self.build_gateway(obs)

        cybernetics_cores = GenM.get_my_units_by_type(obs, units.Protoss.CyberneticsCore)        
        if len(cybernetics_cores)<1:

            return self.build_cybernetics_core(obs)


        gateways_finished = GenM.get_my_completed_units_by_type(obs, units.Protoss.Gateway)

        if len(gateways_finished)<2:
            return self.build_gateway(obs)

        cybernetics_cores_finished = GenM.get_my_completed_units_by_type(obs, units.Protoss.CyberneticsCore)     

        if len(cybernetics_cores_finished)>0:
            robotic_facilities = GenM.get_my_units_by_type(obs, units.Protoss.RoboticsFacility)
            if len(robotic_facilities)<1:
                return self.build_robotics_facility(obs)

            robotics_bays = GenM.get_my_units_by_type(obs, units.Protoss.RoboticsBay)
            if len(robotics_bays)<1:
                return self.build_robotics_bay(obs)

            twilight_councils = GenM.get_my_units_by_type(obs, units.Protoss.TwilightCouncil)
            if len(twilight_councils)<1:
                return self.build_twilight_council(obs)

            dark_shrines = GenM.get_my_units_by_type(obs, units.Protoss.DarkShrine)
            if len(dark_shrines)<1:
                return self.build_dark_shrine(obs)



            

    def use_guardian_shield(self, obs):
        '''
        Metoda do użycia umiejętności (Guardian Shield).

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return us.use_guardian_shield(obs)

    def use_blink_stalker(self, obs):
        '''
        Metoda do użycia umiejętności (Blink Stalker).

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return us.use_blink_stalker(obs, self.first_base_cords)


    def get_strategy(self, obs):
        return self

    def get_action(self, obs):
        action = random.choice(self.actions)
        all_actions = []
        all_actions = connect_actions_deeper(all_actions, getattr(self, action)(obs))
        return all_actions

    def build_nexus(self, obs):
        '''
        Metoda do budowy Nexusa.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_units_by_type(obs, units.Protoss.Gateway))<2:
            return self.build_constructs(obs)

        elif len(GenM.get_my_units_by_type(obs, units.Protoss.Gateway))>= 2*len(GenM.get_my_units_by_type(obs, units.Protoss.Nexus)) \
            or len(GenM.get_my_units_by_type(obs, units.Protoss.Gateway))>=5:
            return bbm.build_nexus(self.first_base_cords, obs)
        else:
            return bc.build_gateway(obs, self.first_base_cords, self.base_top_left)


    def build_forge(self, obs):
        '''
        Metoda do budowy Kuźni.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_forge(obs, self.first_base_cords, self.base_top_left)

    def build_robotics_bay(self, obs):
        '''
        Metoda do budowy Stacji Robotyki.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_robotics_bay(obs, self.first_base_cords, self.base_top_left)


    # metoda "nic nie rób"
    def do_nothing(self, obs):
        '''
        Metoda do zwrócenia pustej akcji.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return actions.RAW_FUNCTIONS.no_op()

    def harvest_all_minerals(self, obs):
        '''
        Metoda do zbierania kryształów i vespanu.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return mgm.harvest_all_minerals(obs)


    def scout(self, obs):
        '''
        Metoda do zwiadu.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return essam.scout(self, obs)

    def attack(self, obs):
        '''
        Metoda do atakowania.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if self.aggression == 1:
            return am.attack(obs)
        if self.aggression == 2:
            return am.attack(obs, False)
        return am.disengage(obs, self.first_base_cords)

    '''
    # metoda służąca do wydobywania minerałów
    def harvest_minerals(self, obs):
        probes = gm.get_my_units_by_type(obs, units.Protoss.Probe)
        idle_probes = [probe for probe in probes if probes.order_length == 0]
        if len(idle_probes) > 0:
            mineral_patches = [unit for unit in obs.observation.raw_units
                               if unit.unit_type in [
                                   units.Neutral.BattleStationMineralField,
                                   units.Neutral.BattleStationMineralField750,
                                   units.Neutral.LabMineralField,
                                   units.Neutral.LabMineralField750,
                                   units.Neutral.MineralField,
                                   units.Neutral.MineralField750,
                                   units.Neutral.PurifierMineralField,
                                   units.Neutral.PurifierMineralField750,
                                   units.Neutral.PurifierRichMineralField,
                                   units.Neutral.PurifierRichMineralField750,
                                   units.Neutral.RichMineralField,
                                   units.Neutral.RichMineralField750
                               ]]
            probe = random.choice(idle_probes)
            distances = gm.get_distances(obs, mineral_patches, (probe.x, probe.y))
            mineral_patch = mineral_patches[np.argmin(distances)]
            return actions.RAW_FUNCTIONS.Harvest_Gather_unit(
                "now", probe.tag, mineral_patch.tag)
        return actions.RAW_FUNCTIONS.no_op()
    '''

    def build_assimilator(self, obs):
        '''
        Metoda do budowy Asymilatora.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return mgm.build_assimilator(obs, self.first_base_cords)

    def build_pylon(self, obs):
        '''
        Metoda do budowy Pylonu.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if gm.free_supply(obs) >= 8:
            return actions.RAW_FUNCTIONS.no_op()
        return bc.build_pylon(obs, self.first_base_cords)

    def build_cybernetics_core(self, obs):
        '''
        Metoda do Rdzenia Cybernetycznego.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_cybernetics_core(obs, self.first_base_cords, self.base_top_left)

    def build_gateway(self, obs):
        '''
        Metoda do budowy Bramy.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(gm.get_my_completed_units_by_type(obs, units.Protoss.Nexus)) * 2 > len(
                gm.get_my_units_by_type(obs, units.Protoss.Gateway)):
            return bc.build_gateway(obs, self.first_base_cords, self.base_top_left)
        return actions.RAW_FUNCTIONS.no_op()

    def build_twilight_council(self, obs):
        '''
        Metoda do budowy Rady Zmroku.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_twilight_council(obs, self.first_base_cords, self.base_top_left)

    def build_robotics_facility(self, obs):
        '''
        Metoda do budowy Zakładu Robotyki.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_robotics_facility(obs, self.first_base_cords, self.base_top_left)

    def build_dark_shrine(self, obs):
        '''
        Metoda do budowy Mrocznej Świątyni.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_dark_shrine(obs, self.first_base_cords, self.base_top_left)


    def train_zealot(self, obs):
        '''
        Metoda do trenowania Zealot.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return ta.train_zealot(obs)

    def train_colossus(self, obs):
        '''
        Metoda do trenowania Colossus.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return ta.train_colossus(obs)

    def train_sentry(self, obs):
        '''
        Metoda do trenowania Sentry.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return ta.train_sentry(obs)

    def train_dark_templar(self, obs):
        '''
        Metoda do trenowania Dark Templar.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return ta.train_dark_templar(obs)

    def train_stalker(self, obs):
        '''
        Metoda do trenowania Stalker.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return ta.train_stalker(obs)

    def train_probe(self, obs):
        '''
        Metoda do trenowania Probe.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(gm.get_my_completed_units_by_type(obs, units.Protoss.Nexus)) * 23 > len(
                gm.get_my_units_by_type(obs, units.Protoss.Probe)):
            return ta.train_probe(obs)
        return actions.RAW_FUNCTIONS.no_op()

    def train_observer(self, obs):
        '''
        Metoda do trenowania Observer.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(gm.get_my_completed_units_by_type(obs, units.Protoss.Observer)) < 1:
            return ta.train_observer(obs)
        return actions.RAW_FUNCTIONS.no_op()

    def morph_archon(self, obs):
        '''
        Metoda do trenowania Archon.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        templars = gm.get_my_units_by_type(obs, units.Protoss.DarkTemplar)
        templars.extend(gm.get_my_units_by_type(obs, units.Protoss.HighTemplar))
        if len(templars) > 5:
            return ta.morph_archon(obs)
        return actions.RAW_FUNCTIONS.no_op()

    def do_research(self, obs):
        '''
        Metoda opracowująca kolejne ulepszenia z drzewka ulepszeń.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Akcja odpowiadająca za opracowanie ulepszenia , lub akcja budująca budynek potrzebny do opracowania kolejnych ulepszeń w drzewku

        '''
        forges = gm.get_my_completed_units_by_type(obs, units.Protoss.Forge)
        twilight_councils = gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)
        
        if len(gm.get_my_units_by_type(obs, units.Protoss.Gateway)) < 1:
            return self.build_gateway(obs)
        if len(forges) < 1:
            return self.build_forge(obs)
        if len(twilight_councils)>0:
            if gm.check_upgrade_in_queue(365, twilight_councils) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.Blink)) < 1:
                return ru.research_blink(obs)

        if gm.check_upgrade_in_queue(394, forges) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundWeaponsLevel1)) < 1:
            return ru.research_ground_weapons_level1(obs)

        if gm.check_upgrade_in_queue(390, forges) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundArmorsLevel1)) < 1:
            return ru.research_ground_armor_level1(obs)

        if gm.check_upgrade_in_queue(398, forges) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossShieldsLevel1)) < 1:
            return ru.research_ground_shields_level1(obs)

        if len(gm.get_my_completed_units_by_type(obs, units.Protoss.TwilightCouncil)) < 1:
            return self.build_twilight_council(obs)


        if gm.check_upgrade_in_queue(365, twilight_councils) and \
                len(gm.get_my_upgrade(obs, upgrades.Upgrades.Blink)) < 1:
            return ru.research_blink(obs)


        if len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundWeaponsLevel1)) == 1 and len(
                gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundArmorsLevel1)) == 1 and len(
            gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossShieldsLevel1)) == 1:



            if gm.check_upgrade_in_queue(86, twilight_councils) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.Charge)) < 1:
                return ru.research_charge(obs)



            if gm.check_upgrade_in_queue(395, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundWeaponsLevel2)) < 1:
                return ru.research_ground_weapons_level2(obs)

            if gm.check_upgrade_in_queue(391, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundArmorsLevel2)) < 1:
                return ru.research_ground_armor_level2(obs)

            if gm.check_upgrade_in_queue(399, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossShieldsLevel2)) < 1:
                return ru.research_ground_shields_level2(obs)

            if gm.check_upgrade_in_queue(396, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundWeaponsLevel3)) < 1:
                return ru.research_ground_weapons_level3(obs)

            if gm.check_upgrade_in_queue(392, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossGroundArmorsLevel3)) < 1:
                return ru.research_ground_armor_level3(obs)

            if gm.check_upgrade_in_queue(400, forges) and \
                    len(gm.get_my_upgrade(obs, upgrades.Upgrades.ProtossShieldsLevel3)) < 1:
                return ru.research_ground_shields_level3(obs)

        return self.do_nothing(obs)


'''
        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel1)) == 1 and len(
                GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel1)) == 1:
            barracks = GenM.get_my_completed_units_by_type(obs, units.Terran.Barracks)
            if len(barracks) < 1:
                return self.build_barrack(obs)

            techlab_barracks = GenM.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)
            if len(techlab_barracks) < 1:
                return bc.build_techlab_barrack(obs)

            if GenM.check_upgrade_in_queue(433, techlab_barracks) and \
                    len(GenM.get_my_upgrade(obs, upgrades.Upgrades.CombatShield)) < 1:
                return ru.research_combat_shield(obs)

            if GenM.check_upgrade_in_queue(362, techlab_barracks) and \
                    len(GenM.get_my_upgrade(obs, upgrades.Upgrades.ConcussiveShells)) < 1:
                return ru.research_concussive_shells(obs)

            if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.CombatShield)) == 1 and len(
                    GenM.get_my_upgrade(obs, upgrades.Upgrades.ConcussiveShells)) == 1:
                factories = GenM.get_my_completed_units_by_type(obs, units.Terran.Factory)
                if len(factories) < 1:
                    return bc.build_factory(self.first_base_cords, self.base_top_left, obs)
                armories = GenM.get_my_completed_units_by_type(obs, units.Terran.Armory)
                if len(armories) < 1:
                    return bc.build_armory(self.first_base_cords, self.base_top_left, obs)

                if GenM.check_upgrade_in_queue(458, engineering_bays) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel2)) < 1:
                    return ru.research_infantry_weapons_level_2(obs)

                if GenM.check_upgrade_in_queue(454, engineering_bays) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel2)) < 1:
                    return ru.research_infantry_armor_level_2(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel2)) == 1:
                    if GenM.check_upgrade_in_queue(459, engineering_bays) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel3)) < 1:
                        return ru.research_infantry_weapons_level_3(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel2)) == 1:
                    if GenM.check_upgrade_in_queue(455, engineering_bays) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel3)) < 1:
                        return ru.research_infantry_armor_level_3(obs)

                if GenM.check_upgrade_in_queue(427, armories) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel1)) < 1:
                    return ru.research_vehicle_weapons_level1(obs)

                if GenM.check_upgrade_in_queue(420, armories) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel1)) < 1:
                    return ru.research_vehicle_and_ship_plating_level1(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel1)) == 1 and len(
                        GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel1)) == 1:

                    if GenM.check_upgrade_in_queue(428, armories) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel2)) < 1:
                        return ru.research_vehicle_weapons_level2(obs)

                    if GenM.check_upgrade_in_queue(421, armories) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel2)) < 1:
                        return ru.research_vehicle_and_ship_plating_level2(obs)

                    if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel2)) == 1 and len(
                            GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel2)) == 1:

                        if GenM.check_upgrade_in_queue(429, armories) and \
                                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel3)) < 1:
                            return ru.research_vehicle_weapons_level3(obs)

                        if GenM.check_upgrade_in_queue(422, armories) and \
                                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel3)) < 1:
                            return ru.research_vehicle_and_ship_plating_level3(obs)
'''


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        # print(div_sign)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0