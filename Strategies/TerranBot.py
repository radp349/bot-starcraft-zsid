from pysc2.agents import base_agent
from pysc2.lib import actions

from Predicator.Predicator import Predicator
from Strategies.TerranStrategies.EarlyTerranStrategy import EarlyTerranStrategy
from Strategies.TerranStrategies.TerranEarlyPush import TerranEarlyPush
from Strategies.TerranStrategies.MidTerranStrategy import MidTerranStrategy
from GeneralMethods import connect_actions_deeper


class TerranBot(base_agent.BaseAgent):
    def __init__(self):
        super(TerranBot, self).__init__()
        self.actions_counter = 0
        self.strategy = EarlyTerranStrategy()
        self.predicator = None
        self.enemy = None
        self.obs = None

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, wywołująca częściowo losowe akcje

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        super(TerranBot, self).step(obs)
        self.obs = obs
        if obs.first():
            self.strategy = TerranEarlyPush([EarlyTerranStrategy])
        if obs.last():
            f = open("score.txt", "a")
            f.write(str(obs.reward) + "\n")
            f.close()
        self.strategy.step(obs)
        self.strategy = self.strategy.get_strategy(obs)
        all_actions = self.strategy.get_action(obs)
        attack_action = getattr(self.strategy, "attack")(obs)
        harvest_action = getattr(self.strategy, "harvest_all_minerals")(obs)
        repair_action = getattr(self.strategy, "repair_buildings")(obs)
        self.actions_counter += 1
        if self.actions_counter % 4 == 0:
            all_actions = connect_actions_deeper(all_actions, harvest_action)
            all_actions = connect_actions_deeper(all_actions, repair_action)
        if self.actions_counter > 8:
            self.actions_counter = 0
            all_actions = connect_actions_deeper(all_actions, attack_action)

        all_actions = [act for act in all_actions if act is not None]
        if len(all_actions) == 0:
            all_actions.append(actions.RAW_FUNCTIONS.no_op())
        if self.predicator is not None:
            if self.enemy is not None:
                self.predicator.predict(self.obs, self.enemy.obs)
            else:
                self.predicator.predict(self.obs)
        return all_actions

    def create_predicator(self, env, step_mul, reverse=False):
        self.predicator = Predicator(env, step_mul, reverse)
