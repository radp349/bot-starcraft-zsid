import numpy as np
from pysc2.lib import units
import TerranBot.AttackMethods as am
import GeneralMethods as GenM
from Strategies.TerranStrategies.TerranBaseStrategy import TerranBaseStrategy
from Strategies.TerranStrategies.LateTerranStrategy import LateTerranStrategy


class MidTerranStrategy(TerranBaseStrategy):
    def __init__(self, command_center, scout_units, scouting_target):
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_command_center = command_center
        self.base_top_left = (command_center.x < 128)
        self.first_base_cords = (command_center.x, command_center.y)
        self.scout_unit_chosen = scout_units
        self.scouting_target = scouting_target

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, opisuje dostepne akcje.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    actions = ("build_supply_depot",
               "train_scv",
               "train_army",
               "build_barrack",
               "build_refinery",
               "build_command_center",
               "build_reactor_or_techlab",
               "scout",
               "do_research",
               "build_tech"
               )

    def get_strategy(self, obs):
        """
        Metoda zwracająca strategię,siebie jeżeli strategia się nie zmienia lub inną

        :param obs: Obecny stan gry
        :return: strategia
        """
        if len(GenM.get_my_completed_units_by_type(obs, units.Terran.Marine)) < 10:
            return LateTerranStrategy(self.first_command_center, self.scout_unit_chosen, self.scouting_target)
        return self


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0
