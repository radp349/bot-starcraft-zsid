from pysc2.agents import base_agent
from pysc2.lib import actions, units, upgrades
import numpy as np
import TerranBot.MineralGatheringMethods as br
import GeneralMethods as GenM
from GeneralMethods import connect_actions_deeper
import TerranBot.TrainArmy as ta
import TerranBot.ResearchUpgrades as ru
import TerranBot.BuildingConstructs as bc
import TerranBot.UnitsSkills as us
import TerranBot.EnemySearchAgentMethods as esam
import TerranBot.AttackMethods as am
import TerranBot.BaseBuildingMethods as bbm
import TerranBot.RepairBuldings as rb
import random


class TerranBaseStrategy:
    '''
    Klasa podstawowej strategii teran obejmująca akcje mogące być wykonywane w każdym momencie gry.
    '''

    def __init__(self):
        self.aggression = 0
        self.aggression_handler = AggressionHandler()

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, opisuje dostepne akcje.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        if obs.first():
            command_center = GenM.get_my_units_by_type(
                obs, units.Terran.CommandCenter)[0]
            self.base_top_left = (command_center.x < 128)
            self.first_base_cords = (command_center.x, command_center.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    actions = ("build_supply_depot",
               "train_scv",
               "train_army",
               "build_barrack",
               "build_refinery",
               "build_command_center",
               "build_reactor_or_techlab",
               "scout",
               "do_research",
               "build_tech"
               )

    def get_strategy(self, obs):
        """
        Metoda zwracająca strategię,siebie jeżeli strategia się nie zmienia lub inną

        :param obs: Obecny stan gry
        :return: strategia
        """
        return self

    def get_action(self, obs):
        """
        Metoda do otrzymywania akcji przy danej strategii

        :param obs: Obecny stan gry
        :return: Lista akcji
        """
        action = random.choice(self.actions)
        all_actions = []
        all_actions = connect_actions_deeper(all_actions, getattr(self, action)(obs))
        return all_actions

    def repair_buildings(self, obs):
        '''
        Metoda do naprawiania budynków.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return rb.repair_buildings(obs)

    def scout(self, obs):
        '''
        Metoda do scoutowania.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return esam.scout(self, obs)

    def harvest_all_minerals(self, obs):
        '''
        Metoda do zbierania kryształów i vespanu.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return br.harvest_all_minerals(obs)

    def build_refinery(self, obs):
        '''
        Metoda do budowy rafinerii.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 1:
            return self.build_barrack(obs)
        return br.build_refinery(obs, self.first_base_cords)

    def build_reactor_or_techlab(self, obs):
        '''
        Metoda do budowy reaktora lub techlaba.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  pusta akcja lub lista akcji.
        '''
        all_acts = []
        r = random.randint(0, 1)
        if len(GenM.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)) < 1 or r > 0:
            acts = bc.build_techlab_barrack(obs)
        else:
            acts = bc.build_reactor_barrack(obs)
        GenM.connect_actions(all_acts, acts)

        acts = bc.build_techlab_factory(obs)
        GenM.connect_actions(all_acts, acts)
        acts = bc.build_techlab_starport(obs)
        GenM.connect_actions(all_acts, acts)
        '''
        supply_depots = GenM.get_my_completed_units_by_type(obs, units.Terran.SupplyDepot)
        if len(supply_depots)>0:
            supply_depots_tags = [unit.tag for unit in supply_depots]
            for supply_depot_tag in supply_depots_tags:
                all_acts.append(actions.RAW_FUNCTIONS.Morph_SupplyDepot_Lower_quick("now", supply_depot_tag))
        '''
        if len(all_acts) > 0:
            return all_acts
        return actions.RAW_FUNCTIONS.no_op()

    def build_command_center(self, obs):
        '''
        Metoda do budowy centrum dowodzenia.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 2:
            return self.build_barrack(obs)
        return bbm.build_command_center(self.first_base_cords, obs)

    def train_scv(self, obs):
        '''
        Metoda do wytrenowania scv.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  pusta akcja lub lista akcji.
        '''
        acts = []
        if len(GenM.get_my_units_by_type(obs, units.Terran.OrbitalCommand)) > 0:
            acts.append(us.calldown_MULE(obs))
        if len(GenM.get_my_bases(obs)) * 24 > len(GenM.get_my_units_by_type(obs, units.Terran.SCV)):
            if GenM.free_supply(obs) < 1:
                acts.append(self.build_supply_depot(obs))
                return acts
            acts.append(br.train_scv(obs))
            return acts
        acts.append(self.build_supply_depot(obs))
        return acts

    def train_army(self, obs):
        '''
        Metoda do wyttrenowania różnych jednostek bojowych.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        marines = GenM.get_my_units_by_type(obs, units.Terran.Marine)
        maruders = GenM.get_my_units_by_type(obs, units.Terran.Marauder)
        thors = GenM.get_my_units_by_type(obs, units.Terran.Thor)
        bases = GenM.get_my_bases(obs)
        calc = (len(bases)) * 2
        # if len(thors) < (len(bases) - 1) * 2:
        #     return self.train_thor(obs)
        if len(marines) <= (len(maruders) * calc):
            return self.train_marine(obs)
        else:
            return self.train_marauder(obs)

    def train_thor(self, obs):
        '''
        Metoda do wytrenowania thora.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        armory = GenM.get_my_units_by_type(obs, units.Terran.Armory)

        if len(armory) < 1:
            return bc.build_armory(self.first_base_cords, self.base_top_left, obs)
        return ta.train_thor(obs)

    def train_viking(self, obs):
        '''
        Metoda do wytrenowania wikinga.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  lista akcji lub akcja.
        '''
        starport = GenM.get_my_units_by_type(obs, units.Terran.Starport)

        if len(starport) < 1:
            return self.build_star_port(obs)
        return ta.train_viking(obs)

    def train_battle_cruiser(self, obs):
        '''
        Metoda do wytrenowania krążownika.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  lista akcji lub akcja.
        '''
        starport = GenM.get_my_units_by_type(obs, units.Terran.Starport)
        fusion_core = GenM.get_my_units_by_type(obs, units.Terran.FusionCore)

        if len(starport) < 1:
            return self.build_star_port(obs)
        if len(fusion_core) < 1:
            return self.build_fusion_core(obs)
        return ta.train_battlecruiser(obs)

    def train_raven(self, obs):
        '''
        Metoda do wytrenowania kruka.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  lista akcji lub akcja.
        '''
        starport = GenM.get_my_units_by_type(obs, units.Terran.Starport)
        if len(starport) < 1:
            return self.build_star_port(obs)
        return ta.train_raven(obs)

    def train_marine(self, obs):
        '''
        Metoda do wytrenowania marine.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  lista akcji lub akcja.
        '''
        all_acts = []
        barracks = GenM.get_my_units_by_type(obs, units.Terran.Barracks)

        if len(barracks) < 1:
            return self.build_barrack(obs)
        if GenM.free_supply(obs) < 1:
            return self.build_supply_depot(obs)

        for barrack in barracks:
            all_acts.append(ta.train_marine(obs))

        return all_acts

    def train_marauder(self, obs):
        '''
        Metoda do wytrenowania marudera.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  lista akcji.
        '''
        all_acts = []
        barracks = GenM.get_my_units_by_type(obs, units.Terran.Barracks)

        completed_barracks_with_techLab = [unit for unit in barracks if
                                           unit.addon_unit_type == units.Terran.BarracksTechLab]
        if len(completed_barracks_with_techLab) < 1:
            act = bc.build_techlab_barrack(obs)
            if isinstance(act, list):
                for raw_action in act:
                    all_acts.append(raw_action)
        else:
            for barrack in completed_barracks_with_techLab:
                all_acts.append(ta.train_marauder(obs))

        if len(all_acts) == 0:
            all_acts.append(actions.RAW_FUNCTIONS.no_op())

        return all_acts

    def attack(self, obs):
        '''
        Metoda do atakowania.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''

        # thors = GenM.get_my_units_by_type(obs, units.Terran.Thor)
        if self.aggression == -1:
            return am.attack(obs, defensive=True)
        if self.aggression == 1:
            return am.attack(obs)
        if self.aggression == 2:
            return am.attack(obs, False)
        return am.disengage(obs, self.first_base_cords)

    def disengage(self, obs):
        '''
        Metoda służąca do odwrotu jednostek.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return am.disengage(obs, self.first_base_cords)

    def disengage_lowest_HP(self, obs):
        '''
        Metoda do odrotu jednostek o najmniejszym hp.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return am.disengage_lowest_hp(obs)

    def do_research(self, obs):
        '''
        Metoda opracowująca kolejne ulepszenia z drzewka ulepszeń.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Akcja odpowiadająca za opracowanie ulepszenia , lub akcja budująca budynek potrzebny do opracowania kolejnych ulepszeń w drzewku

        '''
        engineering_bays = GenM.get_my_completed_units_by_type(obs, units.Terran.EngineeringBay)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 1:
            return self.build_barrack(obs)
        if len(engineering_bays) < 1:
            return self.build_engineering_bay(obs)

        if GenM.check_upgrade_in_queue(457, engineering_bays) and \
                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel1)) < 1:
            return ru.research_infantry_weapons_level_1(obs)

        if GenM.check_upgrade_in_queue(453, engineering_bays) and \
                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel1)) < 1:
            return ru.research_infantry_armor_level_1(obs)

        if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel1)) == 1 and len(
                GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel1)) == 1:
            barracks = GenM.get_my_completed_units_by_type(obs, units.Terran.Barracks)
            if len(barracks) < 1:
                return self.build_barrack(obs)

            techlab_barracks = GenM.get_my_completed_units_by_type(obs, units.Terran.BarracksTechLab)
            if len(techlab_barracks) < 1:
                return bc.build_techlab_barrack(obs)

            if GenM.check_upgrade_in_queue(433, techlab_barracks) and \
                    len(GenM.get_my_upgrade(obs, upgrades.Upgrades.CombatShield)) < 1:
                return ru.research_combat_shield(obs)

            if GenM.check_upgrade_in_queue(362, techlab_barracks) and \
                    len(GenM.get_my_upgrade(obs, upgrades.Upgrades.ConcussiveShells)) < 1:
                return ru.research_concussive_shells(obs)

            if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.CombatShield)) == 1 and len(
                    GenM.get_my_upgrade(obs, upgrades.Upgrades.ConcussiveShells)) == 1:
                factories = GenM.get_my_completed_units_by_type(obs, units.Terran.Factory)
                if len(factories) < 1:
                    return bc.build_factory(self.first_base_cords, self.base_top_left, obs)
                armories = GenM.get_my_completed_units_by_type(obs, units.Terran.Armory)
                if len(armories) < 1:
                    return bc.build_armory(self.first_base_cords, self.base_top_left, obs)

                if GenM.check_upgrade_in_queue(458, engineering_bays) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel2)) < 1:
                    return ru.research_infantry_weapons_level_2(obs)

                if GenM.check_upgrade_in_queue(454, engineering_bays) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel2)) < 1:
                    return ru.research_infantry_armor_level_2(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel2)) == 1:
                    if GenM.check_upgrade_in_queue(459, engineering_bays) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryWeaponsLevel3)) < 1:
                        return ru.research_infantry_weapons_level_3(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel2)) == 1:
                    if GenM.check_upgrade_in_queue(455, engineering_bays) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranInfantryArmorsLevel3)) < 1:
                        return ru.research_infantry_armor_level_3(obs)

                if GenM.check_upgrade_in_queue(427, armories) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel1)) < 1:
                    return ru.research_vehicle_weapons_level1(obs)

                if GenM.check_upgrade_in_queue(420, armories) and \
                        len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel1)) < 1:
                    return ru.research_vehicle_and_ship_plating_level1(obs)

                if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel1)) == 1 and len(
                        GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel1)) == 1:

                    if GenM.check_upgrade_in_queue(428, armories) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel2)) < 1:
                        return ru.research_vehicle_weapons_level2(obs)

                    if GenM.check_upgrade_in_queue(421, armories) and \
                            len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel2)) < 1:
                        return ru.research_vehicle_and_ship_plating_level2(obs)

                    if len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel2)) == 1 and len(
                            GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel2)) == 1:

                        if GenM.check_upgrade_in_queue(429, armories) and \
                                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleWeaponsLevel3)) < 1:
                            return ru.research_vehicle_weapons_level3(obs)

                        if GenM.check_upgrade_in_queue(422, armories) and \
                                len(GenM.get_my_upgrade(obs, upgrades.Upgrades.TerranVehicleAndShipArmorsLevel3)) < 1:
                            return ru.research_vehicle_and_ship_plating_level3(obs)

        return self.train_marine(obs)

    def build_barrack(self, obs):
        '''
        Metoda do budowy baraków.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja lub pusta akcja.
        '''
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 2 * len(GenM.get_my_bases(obs)):
            return bc.build_barrack(self.first_base_cords, self.base_top_left, obs)
        else:
            return actions.RAW_FUNCTIONS.no_op()

    def build_engineering_bay(self, obs):
        '''
        Metoda do budowy engineering bay.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_engineering_bay(self.first_base_cords, self.base_top_left, obs)

    def build_factory(self, obs):
        '''
        Metoda do budowy fabryki.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_factory(self.first_base_cords, self.base_top_left, obs)

    def build_turret(self, obs):
        '''
        Metoda do budowy turretów.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_missile_turret(self.first_base_cords, self.base_top_left, obs)

    def build_supply_depot(self, obs):
        '''
        Metoda do budowy magazynów.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  pusta akcja lub lista akcji.
        '''
        all_acts = []
        supply_depots = GenM.get_my_completed_units_by_type(obs, units.Terran.SupplyDepot)
        if len(supply_depots) > 0:
            supply_depots_tags = [unit.tag for unit in supply_depots]
            for supply_depot_tag in supply_depots_tags:
                all_acts.append(actions.RAW_FUNCTIONS.Morph_SupplyDepot_Lower_quick("queued", supply_depot_tag))
        if GenM.free_supply(obs) >= 7:
            if len(all_acts) > 0:
                if len(all_acts) == 1:
                    return all_acts[0]
                return all_acts
            return actions.RAW_FUNCTIONS.no_op()
        all_acts.append(bc.build_supply_depot(self.first_base_cords, obs))
        if len(all_acts) > 0:
            if len(all_acts) == 1:
                return all_acts[0]
            return all_acts
        return actions.RAW_FUNCTIONS.no_op()

    def build_star_port(self, obs):
        '''
        Metoda do budowy start portu.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return bc.build_star_port(self.first_base_cords, self.base_top_left, obs)

    def build_fusion_core(self, obs):
        return bc.build_fusion_core(self.first_base_cords, self.base_top_left, obs)

    def build_tech(self, obs):
        '''
        Metoda do uzupełniania braków w budynkach.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_bases(obs)) < 2:
            return self.build_command_center(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 1:
            return self.build_barrack(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Factory)) < 1:
            return self.build_factory(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Armory)) < 1:
            return bc.build_armory(self.first_base_cords, self.base_top_left, obs)
        return self.build_barrack(obs)


class AggressionHandler:
    """
    Klasa do sterowania agresją bota
    """
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        """
        Metoda do zapamiętania parametrów rozgrywki do późniejszego obliczenia agresji

        :param obs: Obecny stan gry.
        :return: None
        """
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        """
        Metoda do wyliczania agresji

        0. tylko obrona
        1. atak z grupowaniem
        2. atak bez innych akcji

        :return: flaga agresji
        """
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets)*(self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0



