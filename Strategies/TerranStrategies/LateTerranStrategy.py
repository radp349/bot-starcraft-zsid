import numpy as np
from pysc2.lib import units
import TerranBot.AttackMethods as am
import GeneralMethods as GenM
from Strategies.TerranStrategies.TerranBaseStrategy import TerranBaseStrategy
from GeneralMethods import connect_actions_deeper
import TerranBot.BuildingConstructs as bc


class LateTerranStrategy(TerranBaseStrategy):
    def __init__(self, command_center, scout_units, scouting_target):
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.base_top_left = (command_center.x < 128)
        self.first_base_cords = (command_center.x, command_center.y)
        self.scout_unit_chosen = scout_units
        self.scouting_target = scouting_target
        self.behaviour_flags = {"anti_air": False, "detection": False, "full_army_build": False}

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, opisuje dostepne akcje.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        enemy_buildings = GenM.get_enemy_buildings(obs)
        if not self.behaviour_flags["anti_air"]:
            for bulding in enemy_buildings:
                if bulding.unit_type in [units.Zerg.Spire, units.Zerg.GreaterSpire, units.Protoss.Stargate,
                                         units.Terran.Starport]:
                    self.behaviour_flags["anti_air"] = True
        if not self.behaviour_flags["detection"]:
            for bulding in enemy_buildings:
                if bulding.unit_type in [units.Terran.Ghost, units.Terran.Starport, units.Protoss.DarkShrine,
                                         units.Zerg.LurkerDen]:
                    self.behaviour_flags["detection"] = True
        if not self.behaviour_flags["full_army_build"]:
            if obs.observation.player.minerals > 1000:
                self.behaviour_flags["full_army_build"] = True
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    actions = ("build_supply_depot",
               "train_scv",
               "train_army",
               "build_refinery",
               "build_command_center",
               "build_reactor_or_techlab",
               "scout",
               "do_research",
               "build_tech",
               "build_turret"
               )

    def get_strategy(self, obs):
        """
        Metoda zwracająca strategię,siebie jeżeli strategia się nie zmienia lub inną

        :param obs: Obecny stan gry
        :return: strategia
        """
        return self

    def train_army(self, obs):
        '''
        Metoda do wyttrenowania różnych jednostek bojowych.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        marines = GenM.get_my_units_by_type(obs, units.Terran.Marine)
        maruders = GenM.get_my_units_by_type(obs, units.Terran.Marauder)
        thors = GenM.get_my_units_by_type(obs, units.Terran.Thor)
        ravens = GenM.get_my_units_by_type(obs, units.Terran.Raven)
        vikings = GenM.get_my_units_by_type(obs, units.Terran.VikingFighter)
        bases = GenM.get_my_bases(obs)
        calc = (len(bases)) * 2

        actions = []
        if len(ravens) < 2 and self.behaviour_flags["detection"]:
            if self.behaviour_flags["full_army_build"]:
                actions = connect_actions_deeper(actions, self.train_raven(obs))
            else:
                return self.train_raven(obs)
        if len(vikings) < len(thors)*3 and self.behaviour_flags["anti_air"]:
            if self.behaviour_flags["full_army_build"]:
                actions = connect_actions_deeper(actions, self.train_viking(obs))
            else:
                return self.train_viking(obs)
        if len(thors) < (len(bases) - 1) * 2:
            if self.behaviour_flags["full_army_build"]:
                actions = connect_actions_deeper(actions, self.train_thor(obs))
            else:
                return self.train_thor(obs)
        return connect_actions_deeper(actions, self.train_marine(obs))


    def build_tech(self, obs):
        '''
        Metoda do uzupełniania braków w budynkach.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_bases(obs)) < 2:
            return self.build_command_center(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Factory)) < 2:
            return self.build_factory(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Starport)) < 1:
            return self.build_star_port(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Armory)) < 1:
            return bc.build_armory(self.first_base_cords, self.base_top_left, obs)
        return self.build_barrack(obs)


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)
        self.my_thors = 0
        self.food_used = 0
        self.latch = False

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.my_thors = len(GenM.get_my_units_by_type(obs, units.Terran.Thor))
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))
        self.food_used = obs.observation.player.food_used

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.food_used > 190 or (self.targets[0] > 5 and self.my_units[0] > 0) or self.latch:
            self.latch = True
            if self.food_used < 100:
                self.latch = False
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0
