import numpy as np
import random
from pysc2.lib import units, actions
import TerranBot.AttackMethods as am
import GeneralMethods as GenM
from Strategies.TerranStrategies.MidTerranStrategy import MidTerranStrategy
from Strategies.TerranStrategies.TerranBaseStrategy import TerranBaseStrategy
import TerranBot.AttackMethods as am
import TerranBot.BaseBuildingMethods as bbm
import TerranBot.MineralGatheringMethods as br
import TerranBot.BuildingConstructs as bc
from GeneralMethods import connect_actions_deeper


class TerranEarlyPush(TerranBaseStrategy):
    """
    Strategia ściśle określonej rozbudowy na początku rozgrywki
    """
    def __init__(self, next_strategy_list):
        self.build_order_strategy = {0: self.build_scv_until,
                                     1: self.build_supply_depot_until,
                                     2: self.build_refinery_with_flag,
                                     3: self.build_barrack_until,
                                     4: self.build_command_center_flag,
                                     5: self.get_none_strategy}
        self.aggression = 0
        self.aggression_handler = AggressionHandler()
        self.first_command_center = 0
        self.build_order_flag = 0
        self.next_strategy_list = next_strategy_list

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, opisuje dostepne akcje.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        if obs.first():
            self.first_command_center = GenM.get_my_units_by_type(
                obs, units.Terran.CommandCenter)[0]
            self.base_top_left = (self.first_command_center.x < 128)
            self.first_base_cords = (self.first_command_center.x, self.first_command_center.y)
            self.scout_unit_chosen = []
            self.scouting_target = 0
        self.aggression_handler.remember_state(obs)
        self.aggression = self.aggression_handler.get_aggresion()

    actions = ("build_supply_depot",
               "train_scv",
               "train_army",
               "build_refinery",
               "build_reactor_or_techlab",
               "scout",
               "do_research",
               "build_tech"
               )

    def get_action(self, obs):
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        minerals = GenM.get_minerals(obs)
        action = self.build_order_strategy[self.build_order_flag](obs, self.build_order_flag)
        if isinstance(action, list) and len(action) == 0:
            return [actions.RAW_FUNCTIONS.no_op()]
        return self.build_order_strategy[self.build_order_flag](obs, self.build_order_flag)

    def build_scv_until(self, obs, flag):
        """
        Trenowanie scv do konkretnej ich ilości

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        scv_number = {0: 14}
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        minerals = GenM.get_minerals(obs)
        if scv_count >= scv_number[flag]:
            self.build_order_flag += 1
        return self.train_scv(obs)

    def build_supply_depot_until(self, obs, flag):
        """
        Budowanie magazynów aż do spełnienia konkretnych warunków

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        scv_number = {0: 14,
                      1: 15}
        supply_count = {0: -1,
                        1: 1}
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        minerals = GenM.get_minerals(obs)
        actions = []
        if scv_count < scv_number[flag]:
            actions = connect_actions_deeper(actions, self.train_scv(obs))
        if len(GenM.get_my_units_by_type(obs, units.Terran.SupplyDepot)) < supply_count[flag]:
            actions = connect_actions_deeper(actions, self.build_supply_depot(obs))
        elif scv_count >= scv_number[flag]:
            self.build_order_flag += 1
        return actions

    def build_refinery_with_flag(self, obs, flag):
        """
        Budowanie rafinerii aż do spełnienia konkretnych warunków

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        scv_number = {0: 14,
                      1: 15,
                      2: 15}
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        actions = []
        if scv_count < scv_number[flag]:
            actions = connect_actions_deeper(actions, self.train_scv(obs))
        if len(GenM.get_my_units_by_type(obs, units.Terran.Refinery)) < 1:
            actions = connect_actions_deeper(actions, self.build_refinery(obs))
        elif scv_count >= scv_number[flag]:
            self.build_order_flag += 1
        return actions

    def build_barrack_until(self, obs, flag):
        """
        Budowanie baraków aż do spełnienia konkretnych warunków

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        scv_number = {0: 14,
                      1: 15,
                      2: 15,
                      3: 19}
        barrack_number = {3: 1}
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        actions = []
        if scv_count < scv_number[flag]:
            actions = connect_actions_deeper(actions, self.train_scv(obs))
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < barrack_number[flag]:
            actions = connect_actions_deeper(actions, self.build_barrack(obs))
        elif scv_count >= scv_number[flag]:
            self.build_order_flag += 1
        return actions

    def build_command_center_flag(self, obs, flag):
        """
        Budowanie baz aż do spełnienia konkretnych warunków

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        scv_number = {0: 14,
                      1: 15,
                      2: 15,
                      3: 19,
                      4: 19,}
        barrack_number = {3: 1}
        scvs = GenM.get_my_units_by_type(obs, units.Terran.SCV)
        scv_count = len(scvs)
        actions = []
        if scv_count < scv_number[flag]:
            actions = connect_actions_deeper(actions, self.train_scv(obs))
        if len(GenM.get_my_bases(obs)) < 2:
            actions = connect_actions_deeper(actions, bbm.build_command_center(self.first_base_cords, obs))
        elif scv_count >= scv_number[flag]:
            self.build_order_flag += 1
        return actions

    def get_none_strategy(self, obs, flag):
        """
        pusta akcja

        :param obs: Obecny stan gry.
        :param flag: flaga stanu strategii
        :return: Lista akcji
        """
        return []

    def get_strategy(self, obs):
        """
        Metoda zwracająca strategię,siebie jeżeli strategia się nie zmienia lub inną

        :param obs: Obecny stan gry
        :return: strategia
        """
        if self.build_order_flag == 5:
            return random.choice(self.next_strategy_list)(self.first_command_center, self.scout_unit_chosen, self.scouting_target)
        return self

    def attack(self, obs):
        '''
        Metoda do aktakowania.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''

        return am.disengage(obs, self.first_base_cords)

    def build_tech(self, obs):
        '''
        Metoda do uzupełniania braków w budynkach.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        if len(GenM.get_my_bases(obs)) < 2:
            return self.build_command_center(obs)
        if len(GenM.get_my_units_by_type(obs, units.Terran.Barracks)) < 2:
            return self.build_barrack(obs)
        return self.build_barrack(obs)

    def build_refinery(self, obs):
        '''
        Metoda do budowy rafinerii.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja.
        '''
        return br.build_refinery(obs, self.first_base_cords)

    def build_barrack(self, obs):
        '''
        Metoda do budowy baraków.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  akcja lub pusta akcja.
        '''
        return bc.build_barrack(self.first_base_cords, self.base_top_left, obs)

    def train_scv(self, obs):
        '''
        Metoda do wytrenowania scv.

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns:  pusta akcja lub lista akcji.
        '''
        return [br.train_scv(obs)]


class AggressionHandler:
    def __init__(self):
        self.my_units = np.zeros(100)
        self.targets = np.zeros(100)
        self.enemy_buildings = np.zeros(100)

    def remember_state(self, obs):
        self.enemy_buildings = np.roll(self.enemy_buildings, shift=1)
        self.my_units = np.roll(self.my_units, shift=1)
        self.targets = np.roll(self.targets, shift=1)
        self.enemy_buildings[0] = len(GenM.get_enemy_buildings(obs))
        self.my_units[0] = len(GenM.get_my_combat_units(obs))
        self.targets[0] = len(GenM.get_enemy_combat_units(obs))

    def get_aggresion(self):
        div = self.enemy_buildings[:-1] - self.enemy_buildings[1:]
        div_sign = np.sign(div)
        if self.my_units[0] > 10 or (self.targets[0] > 5 and self.my_units[0] > 0):
            if (-1 in div_sign) or (-1 not in np.sign(np.sign(self.targets) * (self.my_units - self.targets))):
                return 2
            return 1
        else:
            return 0


