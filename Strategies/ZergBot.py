from pysc2.agents import base_agent
from pysc2.lib import actions, units
from Predicator.Predicator import Predicator
from Strategies.ZergStrategies.EarlyZergStrategy import EarlyZergStrategy
from Strategies.ZergStrategies.ZergBaseStrategy import ZergBaseStrategy
from GeneralMethods import connect_actions_deeper
import random
import GeneralMethods as gm


class ZergBot(base_agent.BaseAgent):
    '''
    Klasa agenta wykonującego częściowo losowe akcje zergów.
    Co 4 krok gry wykonywana jest losowa akcja, akcja optymalizacji robotników i akcja naprawy budynków.
    Co 32 krok wykonywana jest akcja ataku.
    W ostatnim kroku wynik gry jest zapisywany do pliku score.txt
    '''

    def __init__(self):
        super(ZergBot, self).__init__()
        self.actions_counter = 0
        self.predicator = None
        self.enemy = None
        self.obs = None

    def step(self, obs):
        '''
        Metoda wykonująca się co krok gry, wywołująca częściowo losowe akcje

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania
        '''
        super(ZergBot, self).step(obs)
        self.obs = obs
        if obs.first():
            hatchery = gm.get_my_units_by_type(
                obs, units.Zerg.Hatchery)[0]
            self.strategy = EarlyZergStrategy(hatchery, [], 0)
        if obs.last():
            f = open("score.txt", "a")
            f.write(str(obs.reward) + "\n")
            f.close()
        self.strategy.step(obs)
        self.strategy = self.strategy.get_strategy(obs)
        all_actions = self.strategy.get_action(obs)
        attack_action = getattr(self.strategy, "attack")(obs)
        move_overseers_action = getattr(self.strategy, "move_overseers")(obs)
        move_mutalisks_action = getattr(self.strategy, "move_mutalisks")(obs)
        move_use_corrosive_bile = getattr(self.strategy, "use_corrosive_bile")(obs)
        harvest_action = getattr(self.strategy, "harvest_all_minerals")(obs)
        strategy_specific_actions = getattr(self.strategy, "strategy_specific_actions")(obs)
        self.actions_counter += 1
        if self.actions_counter % 4 == 0:
            all_actions = connect_actions_deeper(all_actions, harvest_action)
        if self.actions_counter % 4 == 0:
            all_actions = connect_actions_deeper(all_actions, strategy_specific_actions)
        if self.actions_counter >= 10:
            self.actions_counter = 0
            all_actions = connect_actions_deeper(all_actions, attack_action)
            all_actions = connect_actions_deeper(all_actions, move_overseers_action)
            all_actions = connect_actions_deeper(all_actions, move_mutalisks_action)
            all_actions = connect_actions_deeper(all_actions, move_use_corrosive_bile)

        all_actions = [act for act in all_actions if act is not None]
        if len(all_actions) == 0:
            all_actions.append(actions.RAW_FUNCTIONS.no_op())
        if self.predicator is not None:
            if self.enemy is not None:
                self.predicator.predict(self.obs, self.enemy.obs)
            else:
                self.predicator.predict(self.obs)
        return all_actions

    def create_predicator(self, env, step_mul, reverse=False):
        self.predicator = Predicator(env, step_mul, reverse)
