Na początku między trzema apostrofami (''' ''') należy napisać w miarę krótki i treściwy opis działania funkcji.
Następnie używamy słowa kluczowego Args, pod którym opisujemy zmienne zawarte w funkcji(te które funkcja przyjmuje i najważniejsze w środku).
Po tym trzeba użyć słowa kluczowego Returns i opisać zwracaną zmienną.
Sposób opisowania argumentów jak podano niżej,tj. najpierw nazwa zmiennej i w nawiasie jej rodzaj, a po : opis.
Kierować się przykładami niżej.

Edited:

    '''
        Metoda wykonująca się co krok gry, wywołująca częściowo losowe akcje

        :param obs: Obecny stan gry.
        :type obs: timestep.
        :returns: Lista akcji do wykonania

    '''
 

EDITED 2:
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

'''

        Główna metoda ucząca w klasie, zbiera minibatch zebranych stanów z self.expirience_replay,
        rozpakowuje je na poszczególne części i predykuje wybór sieci
        Na koniec fit'uje q_network

        
        :param minibatch: lista zawierająca ostatnie 32 stany gry
        :type minibatch: list
        :param state: tablica stanów 32 x state_size
        :type state: array
        :param target: akcja przewidziana przez sieć
        :type target: String
        :param t: akcja przewidziana prze drugą sieć
        :type t: String

        :returns: None  

'''

///////////////////////////////////////////////////////////////////////////////////////////////

'''

        Strukturta używanego modelu, zastosowanie ConvLSTM w celu poprawy działania,
        przyjmuje argumentu rozmiaru batch_size, standardowo 32 poprzednie state'y gry

        :param model: warstwy modelu
        :type model: class
        
        :returns: (network): struktura sieci


'''

//////////////////////////////////////////////////////////////////////////////////////////

'''
    
    Klasa odpowiedzialna za naukę naszego agenta,
    znajdują się w niej wszystkie elementy potrzebne do nauki agenta posiadającego
    dowolne akcje.
    Używamy DeepQLearningu do zoptymalizowania nauki

        
    :param state_size: rozmiar zwracany prze metodę get_state, zwraca ona wszystkie istotne informacje liczbowe o stanie gry
    :type state_size: int
    :param actions: możliwe akcje do wykonania przez bota
    :type actions: tuple
    :param optimizer: funkcja optymalizująca wykorzystywana w sieciach, Adam
    :type optimizer: optimizer

    .. note::

        Główna klasa ucząca programu.

'''
